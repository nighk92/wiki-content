## Definition of Ready (DOR)
All tasks must meet the definition of ready before they can be brought into a sprint to be worked upon. This helps ensure that we implement the correct things in the correct way, and we don't get slowed down during sprints by having to gather further requirements. The following points should be met:

* Tasks broken down into chunks small enough to complete in a sprint
* Tasks have a clear description of what needs to be done
* Any major design or exploration work has been done to de-risk the solution
* Tasks have been estimated
* Ticket status set to "Ready to Start"
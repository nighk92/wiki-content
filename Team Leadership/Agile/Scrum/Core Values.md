## Overview
Scrum is underpinned by 5 core values, which aid in the implementation of a successful scrum process. The values are:

**Courage** - Team members have courage to do the right thing and work on tough problems.

**Commitment** - People personally commit to achieving the goals of the scrum team.

**Focus** - Everyone focuses on the work of the sprint and the goals of the scrum team.

**Openness** - The team and stakeholders agree to be open about all the work and the challenges involved.

**Respect** - Team members respect each other to be capable and independent people.

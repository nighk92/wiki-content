## Overview
A **demo** session at the end of each sprint to show the work completed to the product owner and other clients, all team members should be present.
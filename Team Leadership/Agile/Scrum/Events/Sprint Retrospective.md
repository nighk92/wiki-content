## Overview
A meeting involving the whole team that happens at the end of each sprint. The main aims of this meeting are to look back at the last sprint deciding on the:

* Things that went well that we want to continue doing
* Things that didn't work so well and need improving
* Actions we can take to improve in the next sprint
* Happiness of each of the team members

### Techniques
* What went well / even better if
* [Journey lines](https://www.barryovereem.com/the-journey-line/)
* Make a list of improvement actions for next sprint
* Maintain a retrospective / process improvement backlog
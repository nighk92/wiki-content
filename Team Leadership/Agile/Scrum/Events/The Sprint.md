## Overview
Sprints are using within the Scrum project management methodology. Sprints can be any length but 2 or 3 week sprints tend to be standard. Work should be split into small enough chunks to be completed within a sprint, and it should be possible to release the application with some new features or bug fixes at the end of every sprint.

### An example 3 week sprint process

1. Wednesday lunchtime - Overall sprint planning with the Client
2. Wednesday afternoon - Team sprint planning, including an overview of the work for this sprint, and splitting up into sub-tasks as needed
3. Wednesday afternoon - Team pickup work starting with the highest priority tasks
4. Following Wednesday - Refinement meeting with the client to pick up discovery and design work to be completed within this sprint, so the refined tasks can be worked on in subsequent sprints
5. Midway stage - Review of progress and re-assignment of tasks as needed, i.e. to surge on high priority tasks to ensure they are completed before the end of the sprint
6. Last Monday of sprint - Team retrospective
7. Last Wednesday morning - Demo of work completed to client
8. Last Wednesday morning - Overall retrospective with the client or other project teams
9. Sprint ends before the planning session for the next sprint (back to step 1)

### Additional Sprint Time

We should always set aside some time in the sprint for innovation & training time, up to 2 days can be allowed in a 3 week sprint

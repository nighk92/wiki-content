## Overview
The Scrum framework specifies 5 events that take place. Each event is time boxed, the time limits are specified below. The sprint acts as a container for the other 4 events.

### The Events
* [The Sprint](The Sprint.md) (between 1 - 4 weeks long)
    * [Sprint Planning](Sprint Planning.md) (maximum of 2 hours per week of sprint)
    * [Daily Scrum](Daily Scrum.md) (maximum 15 mins)
    * [Sprint Review](Sprint Review.md) (maximum of 1 hour per week of sprint)
    * [Sprint Retrospective](Sprint Retrospective.md) (maximum 45 minutes per week of sprint)
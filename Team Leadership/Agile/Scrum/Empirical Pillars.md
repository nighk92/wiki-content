## Overview
Scrum process is founded on the empirical process control theory. Empiricism says that knowledge comes from experience, and that decisions should be made based on what is already known. The following three pillars uphold every implementation of empirical process control:

### Transparency
All aspects of a process must be visible to those responsible for the outcome. In particular the product owner and development team must share a common definition of done.

### Inspection
Scrum masters must frequently inspect the overall process and progress towards the goal. These inspections should not get in the way of everyday work but should help to highlight blockers and potential areas for improvement. Scrum uses regular timeboxed events to facilitate this.

### Adaptation
The process must be adjusted to address any issues raised by an inspection. Changes should be made as soon as possible to minimise further problems.
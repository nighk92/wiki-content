## Overview
Scrum specifies that a team should consist of a Product Owner, Scrum Master and Developers. Scrum teams should be self-organising and cross-functional with a good mix of skills enabling them to work on any product. There should be 3 - 9 developers per team.

### Product Owner
The Product Owner is responsible for maximising the value of the product resulting from work the development team does. They are the owners of the backlog, and order items appropriately to match the needs of the business.

### Scrum Master
The Scrum Master is responsible for promoting and supporting the scrum framework within the team and wider organisation. They are a servant-leader for the scrum team, meaning that they guide and facilitate rather than directly manage. Additionally they help ensure the Development Team is working to their full potential and aid in removing any impediments the Development Team may encounter.

### Development Team
The Development Team do the work prioritised by the product owner. Scrum doesn't specify what type of people should be included in this team, just that the team as a whole should be cross-functional. An ideal development team will have a mixture of skills including programmers, testers, a tech lead, a BA/UX expert. The team should be empowered by the organisation to organise and manage their own work within the boundaries of a sprint.
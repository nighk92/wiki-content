## Overview
Scrum artifacts represent the work and value that is being delivered. The following artifacts are specifically designed to maximise the transparency of key information:

### Product Backlog
The product backlog is an ordered list of everything that is known to be needed in the product. It is the single source of requirements for any changes to be made to the product. The Product Owner is responsible of management of the backlog.

Every item on the backlog should have the following:

* Description
* Order
* Estimate
* Business value

### Sprint Backlog
The sprint backlog is the set of product backlog items selected for the sprint, plus a plan for delivering the product increment and realising the sprint goal. It is a real-time picture of the work the Development Team is doing within the sprint. It should include enough detail so that changes in progress can be understood in the daily scrum. The development team should modify the sprint backlog throughout the sprint, splitting up large tasks so several developers can work on them, and updating it as tasks are completed.

### Increment
This is the potentially releasable increment to the product which should be created at the end of each sprint. This should be the sum of all the backlog items completed and the value of all the increments from previous sprints. It doesn't necessarily need to be released into a production environment, but needs to be available to demo to the Product Owner, and be released when necessary.
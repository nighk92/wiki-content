## Scrum
Scrum is a framework for Agile software development. It is based on the empirical management approach. The definitive Scrum guide can be found here: https://www.scrumguides.org/index.html

The below diagram outlines the overall Scrum process:

![Scrum Framework](.images/Scrum_Framework.png)
## Agile
In the Agile methodology the **Analysis** stage is done in **parallel** to the **Design**, **Implementation** and **Testing** stages. This means the requirements evolve throughout the project and the application evolves with them. Functionality is tested as soon as it has been coded. 

The Scrum process is normally used for Agile development. This involves splitting work into 2-3 week blocks called sprints, and after each block is complete a new version of the application is released.

The following diagram outlines the overall process:

![Agile Process](.images/Agile_Process.jpg)

## Manifesto
The agile manifesto underpins the foundations of the entire agile process and is as follows:

We are uncovering better ways of developing software by doing it and helping others do it. Through this work we have come to value:

* **Individuals and interactions** over processes and tools
* **Working software** over comprehensive documentation
* **Customer collaboration** over contract negotiation
* **Responding to change** over following a plan

That is, while there is value in the items on the right, we value the items on the left more.

## Twelve Principals
The Twelve principals of agile specify more detail on how the manifesto should be implemented:

1. Our highest priority is to satisfy the customer through early and continuous delivery of valuable software.
2. Welcome changing requirements, even late in development. Agile processes harness change for the customer's competitive advantage.
3. Deliver working software frequently, from a couple of weeks to a couple of months, with a preference to the shorter timescale.
4. Business people and developers must work together daily throughout the project.
5. Build projects around motivated individuals. Give them the environment and support they need, and trust them to get the job done.
6. The most efficient and effective method of conveying information to and within a development team is face-to-face conversation.
7. Working software is the primary measure of progress.
8. Agile processes promote sustainable development. The sponsors, developers, and users should be able to maintain a constant pace indefinitely.
9. Continuous attention to technical excellence and good design enhances agility.
10. Simplicity--the art of maximising the amount of work not done--is essential.
11. The best architectures, requirements, and designs emerge from self-organising teams.
12. At regular intervals, the team reflects on how to become more effective, then tunes and adjusts its behaviour accordingly.

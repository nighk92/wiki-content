## Team Leadership
Leading a development team involves knowledge in many areas, not just technical but also logistical, organisational and business knowledge.

There are lots of different lifecycles and structures that can be applied when running a team. Often several of them are used together to create a well balanced team. The more popular methodologies are:

* [Waterfall](Waterfall.md)
* [Agile](Agile/ReadMe.md)
* [Scrum](Agile/Scrum/ReadMe.md)
* LeSS
* DevOps
* Kanban
* Extreme Programming (XP)
* Lean
## Waterfall
The Waterfall methodology consists of 5 stages, which take place in order with minimal overlap:

* **Analysis** – Specification & Requirements
* **Design** – Class diagrams & Pseudocode
* **Implementation** – Code & Installation
* **Testing** – Unit & System tests
* **Operation & Maintenance** – Fixing bugs & Support

![Waterfall Process](.images/Waterfall_Process.jpg)
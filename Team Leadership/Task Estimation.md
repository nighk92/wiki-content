## Task Estimation
The standard way to estimate the size of our tasks is by using story points. This will help decide how much work we should take into a sprint, to ensure we don't take on too much but don't get bored either!

#### Story Points should be based on:
* Size
* Complexity
* Uncertainty

#### When pointing consider:
* Any setup/research required
* Technical design
* UI Design
* How changes will impact the wider system
* Unit/integration testing

#### Pointing Scale (Modified Fibonacci Sequence):

**1** - Small & straight forward 1-2 hours

**2** - Fairly small with no uncertainties - up to 1 day

**3** - Small but with a little uncertainty or complexity - 2-3 days

**5** - Moderate changes or complexity - 1 person for up to a week

**8** - Larger changes with high complexity - 1 person for up to 2 weeks

**13** - Large changes or complexity - 1 person for up to a sprint

**20** - Very large task - requiring 2 people for a sprint

**40** - Need to consider breaking down further

**100** - Something has gone wrong!
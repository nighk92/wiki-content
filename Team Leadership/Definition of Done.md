## Definition of Done (DOD)
Before a task can be marked as completed, the following definition of done must be met. This helps ensure our work is of a high standard, and that we don't take credit for partly completed work.

* Work completed satisfies all requirements from the associated task
* Unit / integrations tests written and pass
* Continuous integration build passes
* Pull request has been approved by 2 people and merged into the Master/Develop branch
* Sign off from senior user / task owner as necessary
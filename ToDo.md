[TOC]

## Improve
### React Notes
* Review & formulate into proper pages

### Web Servers
* Add more detail to Wildfly page
* Add more detail to Apache page

### Maven
* Add mvn:release commands

### Openshift
* Useful commands
* Services it provides
* How it works
  * Internal architecture
  * DNS routing

### Kafka
* Setup & usage
* Advantages / disadvantages compared to other queues

---

## Add
### Javascript
* Brief description of node, npm, webpack, express and maybe a nice diagram to show what runs on server side and client side
* NodeJS version info
* NPM version info
* Angular version info
* React version info

### Linux
* Creating services & enabling them at startup
* Security best practices, how to secure a server
* Anacron, as well as cron.daily and cron.weekly jobs

### Design
* Gateway design and webtokens (JWT)
* Microservices design wiki page woth pros/cons
* Queues and alternatives

### Java
* Convert word document to wiki pages
* JavaSE > Inheritence page add:
    * Constructor Chaining
    * Abstract Classes
    * Interfaces
    * Implements keyword
* JavaSE add sections on:
    * Enums
    * Lambdas
    * Java Server Pages
    * Saving Data
        * To a File
        * To a Database (SQL)
        * To a Database (Javax Persistence)
    * Common Annotations (@)
* Convert course notes to wiki pages
* Add a page on the Java Collections Framework, under JavaSE
* Add pages on JavaEE
* Useful libraries (with maven dependency examples)
* Testing with JUnit, Selenium, Cucumber & Gherkin

### React & Redux
* General syntax and design
* Redux: https://redux.js.org/docs/basics/UsageWithReact.html
* Controlled forms: https://reactjs.org/docs/forms.html
* Uncontrolled forms: https://reactjs.org/docs/uncontrolled-components.html
* Controlled vs. Uncontrolled forms: https://goshakkk.name/controlled-vs-uncontrolled-inputs-react/

### NodeJS
* General setup info
* Installing on Windows
* npm commands
* express web server

### Javascript
* Syntax & usage
* AJAX

### EJS
* General syntax & usage

### Spring
* Dependency injection & beans: @Autowired, @Configuration java file, @Component class annotations and XML methods.
* @PostConstruct and @PreDetroy annotations
* Advice annotations @Before, @Around, @After and @AfterThrowing
* Database Persistence and Exceptions
* @Cacheable, @CacheEvict and @EnableCaching
* Environment variables using @Value or ${value_name}
* JdbcTemplate for executing sql queries
* Transaction management with @Transactional
* Class vs method annotations, class annotation apply to every method in the class but can be overidden with method specific annotations
* Extending Repository class to add automatic implementations of database calls
* Spring boot
* Spring Security
* Rest interfaces
* Spring boot and microservices
* Spring boot and swagger: https://springframework.guru/spring-boot-restful-api-documentation-with-swagger-2/
* Other spring features like Eureka (discovery service), Zuul (gateway)

### Android parental controls
* Turn on parental controls in the play store
* Instal Norton Family
* Setup a non-admin account
* Edit the hosts file - which is persistent even if the device is reset

### MongoDB:
* Selecting Users/Collections and running Common queries
* Using mongo command line client
* Writing js scripts to run queries

### SQLPlus
* How to instal and use
* Need to put \ at the end of SQL scripts and after PL/SQL blocks

### Angular v1
* General syntax & usage

### Angular JS (new version)
* General syntax & usage

### Maven
* Add additional pages on pom.xml files and how libraries are stored in a central maven repository
* Configuring through the settings.xml file
* Cobertura coverage, checkstyle & findbugs plugins
* Configuring MAVEN_OPTS environment variable, e.g. pointing to a java truststore for https maven repositories

### Linux
* Write info about the command: expect
* Bash scripting
* User and group access controls

### Apache Tomcat
* Document from example here https://bitbucket.org/ybot12/jsps/src
* JSP's
* Servlet's
* Apache Tomcat config, web-xml & database connection details (context.xml)

### Jboss Server
* Setup & deployment of .war files

### Haskell
* Convert word document to wiki pages
* General syntax & commands

### Git
* Branching strategies

### Python
* General syntax & commands
* PIP
* Django web applications

### Arduino
* General syntax & usage

### Rock64
* Setup instructions - as secure page?

### RaspberryPI
* Setup instructions
* Using the camera

### HTML
* Specifying legacy document modes in IE
* General syntax & usage

### CSS
* General syntax & usage

### Docker
* Installing & Linux setup to secure docker & enable namespace mapping
* Commands
* Docker-compose files
* Building your own docker container

### Other automated build software
* Gulp

### JMeter
* Setup & usage

### Groovy
* General syntax & usage
* Used in Jenkins pipeline jobs & JMeter

### Jenkins
* Pipeline builds using a Jenkinsfile
* General job configuration
* Setting up hooks with bitbucket
* Bitbucket pull request builder plugin
* Setting up slave nodes

### Atlassian Suite
* Bitbucket - Readme.md syntax
* Confluence - useful macros/modules

### Eclipse
* Installing & setup
* Various plugins
* Usage with maven
* Deploying code to local/remote locations

### Team Leadership
* Add pages for DevOps, Kanban and Multi-Track

### AWS
* Different services available like EC2, S3, Developer Desktops, Kubernetes support
* Cheapest way to do things e.g. using "t" EC2 instances with CPU credits, link to cost calculator
* Designing a secure cloud architenture
    * VPC
    * Security groups / firewalls
    * Internet gateways

### SonarQube
* What it is
* How to install
* Linking with Jenkins

### Webservers
* Add a page about express (for NodeJS)

### Security
* Certificates / HTTPS authentication
    * How it works inc. server, client, CA and truststore certificates
    * How to create certificates
* How VPN's work
* How a VPC works
* How Encryption works, and the different types

### Cypress
* Description of testing framework
* Pros/Cons vs Selenium
* How to use it

### Kubernetwes
* Kubctl
* Helm

### Infrastrucutre as code
* Terraform
* Packer
* Ansible

### Nexus
* How to setup a server

### Elasticsearch / Kibana / Logstash
* Setup resilient Elasticsearch cluster
* Setup Kibana
* Setup logstash

### Typescript
* Pros/Cons
* Syntax
## Repairing a HDD

If Windows won't book the HDD could be corrupted. THis can be fixed by removing it, plugging it into another computer and running:
```bat
:: chkdsk /r <drive-letter>
chkdsk /r D:
```
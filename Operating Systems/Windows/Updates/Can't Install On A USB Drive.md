## Overview
On a fresh install of Windows it may not install updates due to an error with the registry making it think it is installed on a USB drive not a HDD. 

### Problem and Fix
If the windows update fails with the error message "You can't install Windows on a USB flash drive using Setup" then do the following:

**1.** Open "Regedit"

**2.** Go to: HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control

**3.** Change the value of "PortableOperatingSystem" from 1 to 0

**4.** Restart the computer and retry the update
## Create Boot and Windows Partitions
Open an Administrator level Command Prompt and run the following commands:

```powershell
diskpart
list disk
select <disk> # where <disk> is the HDD you want to format
clean
convert gpt
list partition
select <partition> # where <partition> is the MSR partition that was automatically generated
delete partition override
create partition efi size=300
format quick fs=fat32
create partition primary
format quick fs=ntfs
```

## Download and Install Windows
The Microsoft website only lets non-windws machines download windows images directly, so you have to change your browsers user-agent before being able to download:

**1.** Change user agent in Opera:

* Open Developer tools
* Open the right hand menu (three dots) > More Tools > Network Conditions

**2.** Scroll to the bottom and change the user agent to "Opera - Mac"

**3.** Download the Windows 10 image from [here](https://www.microsoft.com/en-gb/software-download/windows10)

**4.** Install onto the HDD using [WinToUSB](https://www.easyuefi.com/wintousb/)

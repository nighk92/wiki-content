## Screen Too Dim
To fix:

**1.** Open Settings

**2.** Click on System > Display

**3.** Untick "Change brightness automatically when lighting changes"

## Can't Adjust Brightness
When running Windows on a Mac through Boot Camp, the brightness of the screen can't be adjusted.

To Fix:

**1.** Open the "Device Manager"

**2.** Under monitors, disable the "Generic PnP Monitor" by right clicking and selecting "Disable device"

**3.** Restart the computer

**4.** Re-enable the "Generic PnP Monitor" from the Device Manager

**5.** Restart the computer

**6.** Test the brightness controls, which should now be working normally
## Invert Two Finger Scrolling

**1.** Open Regedit

**2.** Navigate to the following location: **HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Enum\HID or HKEY_LOCAL_MACHINE\SYSTEM\ControlSetXXX\Enum\HID**

**3.** Expand each of the **VID...** folders and each of the **7&...** sub-folders

**4.** Select **Device Parameters**

**5.** Find the **Device Parameters** folder that contains the **FlipFlopWheel** value

**6.** Double click the **FlipFlopWheel** value and set it to **1**

**7.** Restart the computer for the change to take affect

**8.** If the change hasn't had any affect then you may have changed the **FlipFlopWheel** value of the wrong mouse, so try changing the **FlipFlopWheel** value of a different **VID...** folder
## Commands
Recursively list the number of words in all files of the specific type in a directory
```ps1
# Replace <file-type> with the suffix you wish to search for
(Get-ChildItem -Filter "*.<file-type>" -Recurse | Select-String -pattern ".*" -AllMatches).matches.count 
```
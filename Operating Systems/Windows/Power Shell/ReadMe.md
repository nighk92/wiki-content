## Overview

Power Shell is a task automation and configuration management framework from Microsoft, consisting of a command-line shell and associated scripting language. It is built upon the .NET framework.
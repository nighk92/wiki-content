## Steps
Sometimes the capacity of a USB stick can appear reduced because of an error with the disk partitioning. This can happen when using it as a bootable USB or formatting it in a non-standard way. To fix the partitions and restore the capacity do the following:

**1.** Open the Start Menu

**2.** Search for “Run” and open it

**3.** Enter “DiskPart” and click run

**4.** Enter the following CMD's:
```powershell
list disk
select disk 1 # or whichever disk is the memory stick - not Disk 0!
list partition

# Select and delete all partitions e.g.
select partition 1
delete partition

clean
create partition primary
```

**5.** Then format the USB as normal
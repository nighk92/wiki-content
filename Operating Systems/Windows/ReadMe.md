## Overview
Windows is an operating system built by Microsoft. It is the preferred operating system for many due to the well known user interface. It was based on MS-DOS and the first version came out in 1985.

There are many variants of Windows including: Desktop, Mobile and Server versions.
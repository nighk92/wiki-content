## Installation Steps
* Download the Kubuntu ISO from [here](https://kubuntu.org/getkubuntu)
* Use the ISO to create a bootable USB stick using [Win32 Disk Imager](https://sourceforge.net/projects/win32diskimager/)
* Boot the computer you wish to install Kubuntu on from the USB stick
    * You may need to adjust the BIOS settings so it will boot from the USB
* Follow the installation menu
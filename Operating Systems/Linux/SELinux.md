## Overview

Security-Enhanced Linux (SELinux) is a mandatory access control (MAC) security mechanism implemented in the Centos kernel. SELinux was first introduced in CentOS 4 and significantly enhanced in later CentOS releases. It essentially adds additional controls over the standard linux file permissions. SELinux rules are more fine grained and more aligned to the model of least-privilege then the standard r/w/x file permissions. That being said SELinux is used addition to the standard controls, it does not replace them.

For more information see: https://wiki.centos.org/HowTos/SELinux

## Fixing SELinux issues

SELinux permissions normally affect/block running scripts and programs as a linux service, as it treats the service daemon separatly to standard users. Therefore if you can run the script as a user, but it fails with permission errors when running as a service, SELinux is more than likely blocking it. To resolve this issue try the following:
```bash
setenforce permissive
systemctl start NAME_OF_SERVICE
```

If the service now runs then SELinux is the issue, however the initial `setenforce permissive` command just temporarily turns it off. To permanently fix the issue the SElinux rules can be updated by running the below commands. *Note:* the module name (postgreylocal) must be unique otherwise you will override your existing rules for that module.
```bash
# Create new rules, NOTE: you may need to replace init_t with a different tearm, you can search the audit.log for "error" to find a unique term for the errors you wish to fix
# If audit2allow isn't found then install it using: yum install policycoreutils-python
grep init_t /var/log/audit/audit.log | audit2allow -M RULE_NAME # the RULE_NAME should be unique e.g. it could be the name of the service 
semodule -i RULE_NAME.pp # apply new rules (this is persistent)
setenforce enforcing # turn on SELinux
systemctl start NAME_OF_SERVICE
```

If the service now works then the issue has been fixed! However you may need to repeat the above steps several times to catch/fix all the security errors.
## VNC Through The Browser
The standard way to VNC into a remote server is to use a VNC client like RealVNC. However it is also possible to serve a static webpage which allows a VNC connection using websockets. This can be setup by running the following commands:
```bash
sudo yum groupinstall -y "GNOME Desktop" 
sudo yum install -y tigervnc-server git
sudo reboot

vncserver
git clone https://github.com/novnc/noVNC.git
cd noVnc
sudo ./utils/launch.sh --vnc localhost:5901 --listen 80
```

If you can't connect try stopping the servers firewall by running:
```bash
sudo systemctl stop firewalld.service

#or
sudo systemctl stop iptables
```

To run with a secure https/wss connection follow the commands above but instead of the launch command run the following:
```bash
sudo yum install mod_ssl
sudo mkdir /etc/ssl/keys
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/keys/selfsigned.key -out /etc/ssl/certs/selfsigned.crt
sudo ./utils/launch.sh --vnc localhost:5901 --listen 443 --cert /etc/ssl/certs/selfsigned.crt --key /etc/ssl/keys/selfsigned.key --ssl-only
```
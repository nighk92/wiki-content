## Overview

Linux is a Unix like computer operating system first released in 1991. One of it's main strengths is having extensive command line operations using a language called Bash. Many operating systems are based on Linux/Unix including MacOS, iOS, Ubuntu, Debian, Centos, RedHat, Android, and ChromeOS.

Linux in it's varying forms is widely used on servers, due to it having a powerful command line interface and being free to use.
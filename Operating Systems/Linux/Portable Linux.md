[TOC]

## Portable Linux
It is possible to install a portable version of Linux on a USB stick, this is particularly useful for debugging machines that won't boot, or recovering files.

## What You’ll Need
**1.** A verified copy of the appropriate ISO image of the latest Kali build image for the system you’ll be running it on. This can be:

* [Kali Linux](https://www.kali.org/downloads/)
* [Ubuntu](https://ubuntu.com/download/desktop)
* [Lubuntu](https://lubuntu.net/downloads/)

**2.** If you’re running under Windows, you’ll also need to download the [Win32 Disk Imager utility](https://sourceforge.net/projects/win32diskimager/). On Linux and OS X, you can use the dd command, which is pre-installed on those platforms.

**3.** A USB thumb drive, 4GB or larger. (Systems with a direct SD card slot can use an SD card with similar capacity. The procedure is identical.)

## Creating a Bootable USB Drive on Windows
**1.** Plug your USB drive into an available USB port on your Windows PC, note which drive designator (e.g. “F:\”) it uses once it mounts, and launch the **Win32 Disk Imager** software you downloaded.

**2.** Choose the Linux ISO file to be imaged and verify that the USB drive to be overwritten is the correct one. 

**3.** Click the **Write** button.
 
**3.** Once the imaging is complete, safely eject the USB drive from the Windows machine. You can now use the USB device to boot into Linux ;-)
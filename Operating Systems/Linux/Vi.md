[TOC]

## Cursor Movement
| Shortcut | Action |
| --- | --- |
| **h** or **left arrow** | Move cursor left |
| **j** or **down arrow** | Move cursor down |
| **k** or **up arrow** | Move cursor up |
| **l** or **right arrow** | Move cursor right |
| **w** | Jump to the start of a word |
| **W** | Jump forwards to the start of a word, containing punctuation |
| **e** | Jump forwards to the end of a word |
| **E** | Jump forwards to the end of a word, containing punctuation |
| **b** | Jump backwards to the start of a word |
| **B** | Jump backwards to the end of a word, containing punctuation |
| **0** (Zero) | Jump to the start of a line |
| **^** | Jump to the first non-blank character of the line |
| **$** | Jump to the end of a line |
| **g** | Go to the first line of the document |
| **G** / **:$ | Go to the last line of the document |
| **5G** | Go to line 5 |
| **fx** | Jump to the next occurrence of character "x" |
| **}** | Jump to the next paragraph, or code function/block |
| **{** | Jump to the previous paragraph, or code function/block |
| **Pg Up** | Scroll up the document |
| **Pg Down** | Scroll down the document |

## Insert Mode
| Shortcut | Action |
| --- | --- |
| **i** | Insert text before the cursor |
| **I** (capital i) | Insert at the beginning of the line |
| **a** | Insert (append) text after the cursor |
| **A** | Insert (append) text at the end of the line |
| **o** | Append (open) a new line below the current line |
| **O** | Append (open) a new line above the current line |
| **ea** | Insert (append) text at the end of the word |
| **Esc** | Exit insert mode |

## Editing
| Shortcut | Action |
| --- | --- |
| **r** | Replace a single character |
| **J** | Join line below to the current one |
| **cc** | Change (replace) the entire line |
| **cw** | Change (replace) to the end of the word |
| **c$** | Change (replace) to the end of the line |
| **s** | Delete character and substitute text |
| **S** | Delete line and substitute text (same as cc) |
| **xp** | Transpose two letters (delete and paste) |
| **u** | Undo |
| **Ctrl + r** | Redo |
| **.** | Repeat last command |

## Marking Text (Visual Mode)
| Shortcut | Action |
| --- | --- |
| **v** | Start visual mode, mark lines then do a command |
| **V** | Start line wise visual mode |
| **o** | Move to other end of marked area |
| **Ctrl + V** | Start visual block mode |
| **O** | Move to other corner of block |
| **aw** | Mark a word |
| **ab** | A block with () |
| **aB** | A block with {} |
| **ib** | Inner block with () |
| **iB** | Inner block with {} |
| **Esc** | Exit visual mode |

## Visual Commands
| Shortcut | Action |
| --- | --- |
| **>** | Shift text right |
| **<** | Shift text left |
| **y** | Yank (copy) marked text |
| **d** | Delete marked text |
| **~** | Switch case |

## Cut and Paste
| Shortcut | Action |
| --- | --- |
| **yy** | Yank (copy) a line |
| **2yy** | Yank (copy) 2 lines |
| **yw** | Yank (copy) a word |
| **y$** | Yank (copy) to end of line |
| **p** | Paste the clipboard after the cursor |
| **P** | Paste before the cursor |
| **dd** | Delete (cut) a line |
| **2dd** | Delete (cut) 2 lines |
| **dw** | Delete (cut) a word |
| **D** or **d$** | Delete (cut) to the end of the line |
| **x** | Delete (cut) a single character |

## Exiting
| Shortcut | Action |
| --- | --- |
| **:w** | Write (save) the file |
| **:wq** or **:x** or **ZZ** | Write (save) the file and quit |
| **:q** | Quit, fails if there are unsaved changes |
| **:q!** or **ZQ** | Quit discarding any changes |

## Search and Replace
| Shortcut | Action |
| --- | --- |
| **/pattern** | Search for a pattern |
| **?pattern** | Search backwards for a pattern |
| **\vpattern** | Search for a pattern with regex |
| **n** | Repeat search in the same direction |
| **N** | Repeat search in the opposite direction |
| **:%s/old/new/g** | Replace all old with new throughout the file |
| **:noh** | Turn off search highlighting for previous search | 

## Working with multiple files
| Shortcut | Action |
| --- | --- |
| **:e filename** | Edit a file in a new buffer |
| **:bnext** or **:bn** | Go to the next buffer |
| **:bprev** or **:bp** | Yank (copy) a word |
| **:bd** | Close the current file |
| **:sp filename** | Open a file in a new buffer, split window |
| **:vsp filename** | Open a file in a new buffer, vertically split window |
| **Ctrl + ws** | Split window |
| **Ctrl + ww** | Switch windows |
| **Ctrl + wq** | Quit a window |
| **Ctrl + wv** | Split window vertically |
| **Ctrl + wh** | Move cursor to the left window |
| **Ctrl + wl** | Move the cursor to the right window |
| **Ctrl + wj** | Move the cursor to the window below |
| **Ctrl + wk** | Move the cursor to the window above |

## Tabs
| Shortcut | Action |
| --- | --- |
| **:tabnew filename** or **:tabn filename** | Open a file in a new tab |
| **Ctrl + wT** | Move the current split window to its own tab |
| **gt** or **:tabnext** or **:tabn** | Move to the next tab |
| **gT** or **:tabprev** or **:tabp** | Move to the previous tab |
| **#gt** | Move to the tab number # |
| **:tabmove #** | Move the current tab to the #th position (indexed from 0) |
| **:tabclose** or **:tabc** | Close the current tab |
| **:tabonly** or **:tabo** | Close all tabs except the current one |

## Cheat Sheet
[Vi Cheat Sheet.pdf](.files/Vi Cheat Sheet.pdf)
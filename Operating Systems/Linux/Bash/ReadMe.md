## Overview

One of the most popular command line interfaces for Linux is called the Bash Shell. It is used as the default login shell for most Linux distributions.

It can read and excute textual commands that a user types, and also execute commands from a file called a Shell Script.
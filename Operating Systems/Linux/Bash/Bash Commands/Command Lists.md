## Command Lists
Run cmd1 then cmd2
```bash
cmd1; cmd2
```

Run cmd2 if cmd1 is successful
```bash
cmd1 && cmd2
```

Run cmd2 if cmd1 is unsuccessful
```bash
cmd1 || cmd2
```

Run a command in a sub-shell, which doesn't lock the current shell
```bash
cmd &
```

Write a command that spans several lines by concatenating lines with /
```bash
echo "This command goes onto a /
      second line"
```
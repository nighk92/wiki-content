## Bash Variables
The **.bashrc** file is located in each users home directory. It is run when the cmd window is first opened and can be used to set environment variables and aliases. There is also a **.bash_profile** file as well, which is run when logging in as a particular user. Aliases and variables can be set like this:
```bash
alias NAME="COMMAND" # create an alias which is a shortcut to run a different command
export VARIABLE=VALUE # set the value of an environment variable
```

Reload the bashrc file manually, without restarting the command prompt
```bash
source ~/.bashrc
```

Show all environment variables
```bash
env
```

Print the value of a variable
```bash
echo $NAME
```

Create/overwrite the value of a variable
```bash
export VARIABLE=VALUE
```

Special environment variables
```bash
$PATH # stores the path that linux will automatically search for executable files/commands
$HOME # stores the current users home directory location
```
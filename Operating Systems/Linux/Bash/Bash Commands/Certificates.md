[TOC]

## Certificates
OpenSSL is a free command line tool for Linux which can perform operations on certificates and ssh keys

### Read a Certificate
Check a certificate (.crt or .cert)
```bash
openssl x509 -in certificate.crt -text
```

Check PKCS#12 certificate (.pfx or .p12)
```bash
openssl pkcs12 -info -in keyStore.p12
```

Check a private key
```bash
openssl rsa -in privateKey.key -check
```

Check a certificate signing request (CSR)
```bash
openssl req -text -noout -verify -in CSR.csr
```

Find expiry date for a PKCS#12 certificate (.pfx or .p12)
```bash
openssl pkcs12 -in certificate.p12 -out certificate.pem -nodes
openssl x509 -in certificate.pem -noout -enddate
```

### Generating New Certificates
Generate a new certificate authority (CA)
```bash
openssl req -new -newkey rsa:2048 -days 12000 -x509 -subj "/C=UK/O=<organisation>/OU=<Organisation Unit>/CN=ca" -keyout ca.pem -out ca.crt -passout "pass:<password>"
```

Generate a new private key and Certificate Signing Request
```bash
openssl req -out CSR.csr -new -newkey rsa:2048 -nodes -keyout privateKey.key
```

Generate a self-signed certificate
```bash
openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -keyout privateKey.key -out certificate.crt
```

Generate a certificate signing request (CSR) for an existing private key
```bash
openssl req -out CSR.csr -key privateKey.key -new
```

Generate a certificate signing request based on an existing certificate
```bash
openssl x509 -x509toreq -in certificate.crt -out CSR.csr -signkey privateKey.key
```

### Modifying & Extracting Certificates
Remove a passphrase from a private key
```bash
openssl rsa -in privateKey.key -out newPrivateKey.key
```

Extract a standard certificate file from a PEM
```bash
openssl x509 -in certificate.pem -out certificate.crt
```

Extract a DER certificate file from a PEM
```bash
openssl x509 -outform der -in certificate.pem -out certificate.der
```

Extract a Private key file from a PEM
```bash
openssl pkey -in certificate.pem -out key.key
```

Extract a Certificate file from a PKCS12 (.pfx or .p12)
```bash
openssl pkcs12 -in yourP12File.pfx -clcerts -nokeys -out publicCert.cert
```

Extract a Private key file from a PKCS12 (.pfx or .p12)
```bash
openssl pkcs12 -in yourP12File.pfx -nocerts -out privateKey.key
```

Convert a PKCS#12 file (.pfx .p12) containing a private key and certificates to PEM
```bash
openssl pkcs12 -in keyStore.pfx -out keyStore.pem -nodes
```

Convert a PEM certificate file and a private key to PKCS#12 (.pfx .p12)
```bash
openssl pkcs12 -export -out certificate.pfx -inkey privateKey.key -in certificate.crt -certfile CACert.crt
```
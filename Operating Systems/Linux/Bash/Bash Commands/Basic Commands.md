## Basic Commands
Show System and Terminal information
```bash
uname -a
```

Show this machines Linux distribution
```bash
head -nt /ect/issue
# or
cat /etc/*-release
```

Show the system date
```bash
date
```

Show how long the system has been on for (since last boot)
```bash
uptime
```

Show your username
```bash
whoami
```

Show your user & group id information
```bash
id
```

Show where the executable file is for a command
```bash
whereis COMMAND
```

Show the manual for a command
```bash
man COMMAND
```

Show the help text for a command
```bash
COMMAND --help
```

Login as the specified user
```bash
su - user

# Or add sudo to switch to a user with higher privileges
sudo su - user
```

Close down the command window, or logout if you have logged in as another user
```bash
exit
```

Shutdown the system
```bash
halt
```

Shutdown and restart the system
```bash
reboot
```

Edit the sudoers file, to update sudo access for different users
```bash
sudo visudo

# Or to programatically edit e.g. in a script
echo '<LINE TO ADD>' | sudo EDITOR='tee -a' visudo
```

Get the mac address of all network interfaces
```bash
cat /sys/class/net/*/address

# Or use the following to get the mac of a specific network interface
cat /sys/class/net/eth0/address
```

Show system boot time
```bash
systemd-analyze # show time summary
systemd-analyze critical-chain # show time taken for critical boot path
systemd-analyze blame # show all boot processes in order of time taken
```

View system logs, all are kept under /var/log
```bash
cd /var/log
```

Run a command repeatedly every 5 seconds, the interval can be adjusted
```bash
watch -n 5 'cmd'
```
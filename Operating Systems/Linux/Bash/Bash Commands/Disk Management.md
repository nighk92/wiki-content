[TOC]

## Disk Management
### View Disk Info
Show all mounted disks, their size, and remaining space
```bash
df -h

# Options
# -h = human readable, show size in KB/MB/GB
```

Show size of all folders in a directory
```bash
du -h --max-depth=1
```

Show the total size of the current folder
```bash
du -sh
```

Show mounted filesystems
```bash
mount
```

### Format a Disk
1. Identify a drives partitions e.g. if the partition is /dev/sdb1 then the drive is /dev/sdb
```bash
fdisk -l
```

2. Unmount the drive
```bash
umount /dev/sdb1
```

3. Delete the current partition
```bash
fdisk /dev/sdb
d # delete current partition
w # write changes to the drive
```

4. Create a new partition
```bash
fdisk /dev/sdb
n # create a new partition
p # make the partition primary
1 # set the partition number
83 # set the partition type (83 = ext). Enter "L" at the fdisk command prompt for a full list of partition types
w # write changes to the drive
```

5. Format the new partition. Partition types are ext2, ext3, ext4, vfat (fat32)
```bash
mkfs.ext3 /dev/sdb1
```

6. Remount the drive and map it to a folder folder
```bash
mount /dev/sdb /destfolder
```

**Fix** a drive that fdisk can't read/write to
```bash
parted -l
```
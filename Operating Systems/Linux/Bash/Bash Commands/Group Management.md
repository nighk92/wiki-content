## Group management
List all available groups
```bash
getent group
```

Create a new user group
```bash
groupadd <group-name>
```

Add user to a group
```bash
gpasswd -a <user> <group-name>
```

Remove a user from a group
```bash
gpasswd-d <user> <group-name>
```

List members of a group
```bash
gpasswd -M <group-name>
```
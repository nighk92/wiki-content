## SSH Keys
Generate an SSH key
```bash
ssh-keygen
```

Encrypt an SSH key (openssl)
```bash
openssl rsa -aes256 -in key.pem -out key-encrypted.pem
```

Encrypt an SSH key (puttygen)
1. Open puttygen
2. Import your private key
3. Enter a secure password in 'key passphrase' and 'Confirm passphrase'
4. Click Conversions > Export OpenSSH key and save the encrypted pem file

Decrypt an SSH key
```bash
openssl rsa -in key-encrypted.pem -out key.pem
```
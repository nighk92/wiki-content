[TOC]

## Network Operations
### Network Information
Show ethernet/wifi connections and ip addresses
```bash
ip addr show
```

Get the web address of the current server
```bash
hostname
```

Shows which ports currently running applications are listening on
```bash
netstat -n -l
```

### Firewall Rules - IP tables
Start/stop the IP tables firewall
```bash
systemctl start iptables
systemctl stop iptables
```

List current rules, note that port numbers aren't often shown under the destination column instead they are listed by their common names which can be found on [Wikipedia](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers#Well-known_ports).
```bash
iptables -L

# To display port numbers add --numeric eg.
iptables -L --numeric
```

Add a new rule
```bash
iptables -I INPUT -p tcp --dport 80 -j ACCEPT # Insert a new highest priority rule allowing incoming TCP traffic on port 80
iptables -I OUTPUT -p tcp --dport 80 -j ACCEPT # Insert a new highest priority rule allowing outgoing TCP traffic on port 80
iptables -A INPUT -p udp --dport 80 -j ACCEPT # Insert a new lowest priority rule allowing incoming UDP traffic on port 80

iptables -I INPUT -p tcp --dport 80 -j REJECT # Insert a new highest priority rule rejecting incoming TCP traffic on port 80, will respond with a connection refused message
iptables -I INPUT -p tcp --dport 80 -j DROP # Insert a new highest priority rule dropping incoming TCP traffic on port 80

iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080 # Redirect traffic coming in on port 80 to port 8080, which can be used to effectively run an application on port 80 without root access

service iptables save # Make changes persistent
```

Delete a rule by specification
```bash
iptables -D INPUT -p tcp --dport 80 -j ACCEPT
```

Delete a rule by chain number
```bash
iptables -L --line-numbers # Find chain and line to delete

iptables -D <chain> <line-number>

# For example, to delete rule 5 from the INPUT chain
iptables -D INPUT 5
```

Replacing a rule
```bash
iptables -R INPUT -p tcp --dport 80 -s 192.168.0.0/24 -j ACCEPT # Replaces the previous TCP input on port 80 rule restricting traffic to the given ip range
```

### Firewall Rules - firewalld
Starting/Stopping the firewall
```bash
systemctl start firewalld
systemctl stop firewalld
```

List the currently open ports
```bash
firewall-cmd --list-ports
```

Open a port
```bash
firewall-cmd --permanent --add-port=8080/tcp
systemctl restart firewalld
```

Assign a network interface to a zone
```bash
firewall-cmd --permanent --zone=trusted --change-interface=docker0
systemctl restart firewalld
```

Remove a network interface from a zone
```bash
firewall-cmd --zone=trusted --remove-interface=docker0 --permanent
systemctl restart firewalld
```

### Copying Files (across the network)
Copy files to/from a remote computer
```bash
scp source destination

# Source/destination format:
# file_path (local)
# user@hostname:file_path (remote)

# Options:
# -r = recursive, for copying directories
```

Download files from a remote server
```bash
wget source_url

# Options:
# --no-check-certificate = disable checking of ssl certificates
# --certificate=FILE --certificate-type=PEM = connect to a https site using a client certificate in the .pem format
# --no-parent -r -nH --reject "index.html*" = recursively download a folder, without the parent directory structure
```

### Testing network speeds between two servers
`iperf` can be used to quickly setup a server and client process on two Linux servers and report the max network throughput between them.

Run the following command on the server machine:
```bash
iperf -s -i 10

# -s - Create a server that will listen on TCP 5001.
# -i 10 - Print a speed report every 10 seconds.
```

**NOTE**: The firewall on the server will need to allow traffic over port 5001, otherwise the client will fail to connect.

On the client run the following command:
```bash
iperf -i 10 -c <SERVER-HOSTNAME>

# -i - Print a speed report every 10 seconds.
# -c - The server’s IP or hostname.
```

[TOC]

## Installing and Updating Programs
### APT (Advanced Package Tool)
APT is the default package manager on Debian and Ubuntu related Linux distributions.

Update the list of programs that can be installed
```bash
sudo apt-get update
```

Search for a program to install
```bash
sudo apt-cache search PROGRAM
```

Install a program
```bash
sudo apt-get NAME
```

List the installed programs
```bash
sudo apt -list -installed
```

Remove a program and all its dependencies
```bash
sudo apt-get purge program
```

Update all programs, and install security updates
```bash
sudo apt-get update        # Fetches the list of available updates
sudo apt-get upgrade       # Strictly upgrades the current packages
sudo apt-get dist-upgrade  # Installs updates (new ones)
reboot
```

Removes packages that are no longer needed, e.g. old dependencies of programs that have now been uninstalled
```bash
sudo apt autoremove
```

### Yum Package Manager
Yum can install, update and uninstall various programs and packages on linux. It is the default package manager on Centos 7.

Install yum using apt-get
```bash
sudo apt-get install yum
```

Install a program with yum
```bash
sudo yum install PROGRAM
```

Uninstall a program with yum
```bash
sudo yum remove PROGRAM
```

List programs available with Yum
```bash
sudo yum list PROGRAM # wildcards can be used e.g. sudo yum list pytho*
```

List programs already installed with yum
```bash
sudo yum list installed
```

### Dnf Package Manager
Dnf (Dandified yum) is a replacement for Yum, and is the default package manager on Centos 8.

Install a program
```bash
sudo dnf install PROGRAM
```

Uninstall a program
```bash
sudo dnf remove PROGRAM
```

List programs available
```bash
sudo dnf list PROGRAM # wildcards can be used e.g. sudo dnf list pytho*
```

List programs already installed with yum
```bash
sudo dnf list installed
```

---
## Services
View the status of all installed services
```bash
sudo service --status-all

# + = running
# - = not running
# ? = status uncertain
```

View the status of a specific service
```bash
sudo service NAME status
```

Start a service
```bash
sudo service NAME start
```

Stop a service
```bash
sudo service NAME stop
```

Restart (stop then start) a service
```bash
sudo service NAME restart
```
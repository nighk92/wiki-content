[TOC]

## File Operations
### Basic Operations
Create a file
```bash
touch FILE
```

Copy a file
```bash
copy FILE DESTINATION

# Options:
# -r = recursive, used to copy directories
# -f = overwrite the destination file
```

Delete a file
```bash
rm FILE

# Options:
# -r = recursive, used to delete directories
# -f = deletes protected files without prompting
```

Create a symbolic link (a bit like a shortcut)
```bash
ln -s source_file myfile
```

Delete a symbolic link
```bash
unlink file_name
```

Gets the type of a file e.g. if it is a file of a directory
```bash
file FILE
```

### Reading
Print the contents of a file
```bash
cat FILE

# cat FILE1 FILE2 - concatenates the contents of the files and prints them
```

Open a file in the VI text editor. See the [Vi](Vi.md) page for more info and commands.
```bash
vi FILE

# on some versions of linux the "vi" command is replaced with "vim"
```

Open a file in the MORE text viewer
```bash
more FILE
```

Open a file in the LESS text viewer
```bash
less FILE
```

Show the first 10 lines of a file, the number of lines can be adjusted
```bash
head -10 FILE
```

Show the last 10 lines of a file, the number of lines can be adjusted
```bash
tail -10 FILE

# Options:
# -f = show new lines as they are written to the file, e.g. tail -10f FILE
```

### Permissions
Linux files have permissions for 3 groups, the creating user, a specified group, everyone else. These are abbreviated as user, group, other or u,g,o. Each of the 3 groups can read, write or execute the file.

Allow all to read, write and execute a file
```bash
chmod 777

# Usage: chmod ugo
# Options:
# 0 = no permissions
# 1 = execute
# 2 = write
# 4 = read
# 5 = read & execute
# 6 = read & write
# 7 = read, write & execute
# -r = recursive, adjust permissions for everything in a directory
```

Change the file owner and file group (group is optional)
```bash
chown USER:GROUP FILE
```

### Default Permissions
The user file-creation mode mask (umask) is use to determine the file permission for newly created files. It can be used to control the default file permission for new files. The umask masks permissions by restricting them by a certain value.

Each digit of the umask is "subtracted" from the OS's default value, so the umask tells the operating system which permission bits to "turn off" when it creates a file.

In Linux, the default permissions value is 666 for a regular file, and 777 for a directory. When creating a new file or directory, the kernel takes this default value, "subtracts" the umask value, and gives the new files the resulting permissions.

This table shows how each digit of the umask value affects new file and directory permissions:

| Umask digit | Default file permissions | Default directory permissions |
| --- | --- | --- |
| 0| rw | rwx |
| 1 | rw | rw |
| 2 | r | rx |
| 3 | r | r |
| 4 | w | wx |
| 5 | w | w |
| 6 | x | x |
| 7 | (no permission allowed) | (no permission allowed) |

So if our umask value is 022, then any new files will, by default, have the permissions 644 (666 - 022). Likewise, any new directories will, by default, be created with the permissions 755 (777 - 022).

Show the current umask value
```bash
umask
```

Set the umask value
```bash
# Usage: umask ugo
umask 077
```

### Search for Files
Find all files with a particular name, regex can be used
```bash
find /dir/ -name "pattern"
```

Find files owned by a particular user
```bash
find /dir/ -user "name"
```

Find files modified less than NUM minutes ago
```bash
find /dir/ -mmin NUM
```

Find a file by doing a quick search of the file index
```bash
locate FILE
```

### Search in Files
Search for a pattern in files
```bash
grep PATTERN FILES
# e.g. grep -r pattern* ./*

# options
# -i = case insensitive search
# -r = recursive search
= -n = show line numbers
# -o = show file name only (not the matched line)
```

### Replace in a File
Replace specific text within a file
```bash
sed -i 's/original/new/g' file.txt

# Use double quotes to inject variables into the string
# e.g. sed -i "s/original/$NEW_VALUE/g" file.txt
# 
# Options:
# -i = in place, saves the text back to the file
# /s = the substitute command
# /g = global, replacing all occurrences
```

Append to the end of a file, after the last line
```bash
sed -i '$ \aTEXT_TO_APPEND' file.txt

# $ specifies the last line of the file
# \a specifies appending
```

Delete specific lines in a file. E.g. This deletes lines 5-10 and line 12:
```bash
sed -e '5,10d;12d' file.txt
```

### Compressing and Uncompressing Files
Zip a file
```bash
zip FILE

# Options:
# -r =  to recursively zip a directory
```

Expand a zip file
```bash
unzip SOURCE
```

Tar a file
```bash
tar -cvf ARCHIVE.tar SOURCE

# Options:
# -c = create a .tar archive
# -v = display progress
# -f = specify the filename of the archive
# -z = compress with gzip as well, creates a tar.gz file
# -j = compress with bzip2 for smaller files, creates a tar.bz2 file
# -J = compress with xz for smaller files, creates a tar.xz file
```

Expand a tar file
```bash
tar -xvf ARCHIVE.tar

# Options:
# -x = expand a .tar archive
# -v = display progress
# -f = specify the filename of the archive
# -z = expand with gzip, for a tar.gz file
# -j = expand with bzip2, for a tar.bz2 file
# -J = expand with xz, for a tar.xz file
```

Make a 7zip (.7z) archive (install p7zip using apt-get or yum)
```bash
7za a FILENAME

# Options:
# -r = recurse into subdirectories
```

Extract a 7zip ( .7z) archive (install p7zip using apt-get or yum)
```bash
7za e FILENAME.7Z
```

Make a Gzip (.gz) file
```bash
gzip FILENAME
```

Extract a Gzip (.gz) file
```bash
gunzip FILENAME

# Options
# --suffix=.csv = Extract a file that doesn't have a .gz suffix e.g. .csv
```

### Hashing & Verifying Files
Hashing can be used to easily compare different files to see if their contents differ, and to verify downloaded files are correct.

Get the MD5 hash of a file
```bash
md5sum FILENAME
```

### Base64 Encoding
Base64 encode a file for more efficient transfer or storage, particularly useful for storage or binary data.

Encode in base64 format:
```bash
base64 <input-file> > <output-file>

# Or
echo "text" | base64

# To base64 encode and remove linebreaks use:
base64 <input-file> | tr -d '\n' > <output-file>
```

Decode from base64:
```
base64 -d <input-file> > <output-file>

# Or
echo "text" | base64 -d
```

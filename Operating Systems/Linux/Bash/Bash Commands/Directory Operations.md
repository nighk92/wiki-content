## Directory Operations
Show the full path of the current directory
```bash
pwd
```

List the files in a directory
```bash
ls

# Options
# -a = show all files including hidden files
# -h = display file sizes in human readable format (KB, MB, GB)
# -l = long listing format showing additional file information
# -S = sorts results by file size
# -t = sorts results by their last modified date
# -r = reverse order
```

Make a new directory
```bash
mkdir PATH

# Options:
# -p = Create parent directories if they don't exist
```

Change into a directory
```bash
cd PATH # go to the specified directory
cd . # go to the current directory
cd .. # go to the parent directory
cd - # go to the previous directory
cd ~ # go to the user home directory
```
## Bash Shortcuts
Kills current command, by interrupt
```bash
CTRL + c
```

Stops current command, effectively putting it to sleep
```bash
CTRL + z
```

Go to start of line
```bash
CTRL + a
# or use the HOME button
```

Go to end of line
```bash
CTRL + e
# or use the END button
```

Search command history, pressing CTRL + r again will cycle through the potential matches
```bash
CTRL + r
```

Repeat last command
```bash
!!
```

Repeat last command with different arguments
```bash
!!:0 newArg newArg2
```

Run last command staring with abc
```bash
!abc
```

Get all arguments of the previous command
```bash
!*
```

Run last command replacing abc with 123
```bash
^abc^123
```
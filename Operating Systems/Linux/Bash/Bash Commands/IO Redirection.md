## IO Redirection
Take the file as input for the command
```bash
cmd < file
```

Take the output of cmd2 as the input for cmd1
```bash
cmd <(cmd2)
```

Redirect the standard output of the cmd to the file, overwriting any existing file
```bash
cmd > file
```

Append the output of the cmd to the end of the file
```bash
cmd >> file
```

Discard the output of cmd
```bash
cmd > /dev/null
```

Take the error output of the cmd, and write it to the file
```bash
cmd 2> file
```

Send the standard output of the cmd to the same place as the error output
```bash
cmd 1>&2
```

Send the error output of the cmd to the same place as the standard output
```bash
cmd 2>&1
```

Send every output of cmd to the file
```bash
cmd &> file
```

Pipe the standard output of cmd1 to cmd2
```bash
cmd1 | cmd2
```

Pipe the standard error output of cmd1 to cmd2
```bash
cmd1 |& cmd2
```
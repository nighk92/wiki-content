## User Management
Create a user and a default home directory under /home/<username>
```bash
sudo useradd -m <username>
```

Delete a user, and their home directory
```bash
sudo userdel -r <username>
```

Set or change a users password, this will prompt for you to enter a new password
```bash
passwd <username>
```

List all registered users
```bash
cut -d: -f1 /etc/passwd
```
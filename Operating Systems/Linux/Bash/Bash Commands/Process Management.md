## Process Management
Show running processes in real time, with CPU and RAM usage.
```bash
top
```

A nicer GUI version of top can be installed called htop
```bash
# Install (use apt or yum depending on your version of Linux)
sudo apt-get htop
sudo yum install htop

# Run
htop
```

Show a snapshot of running processes
```bash
ps
```

Search for a name in the running process list
```bash
ps -ef | grep NAME
```

Kill a process with a specific process id
```bash
kill PID

# Options:
# -9 = force the process to stop
```

Kill a process with a specific name
```bash
pkill NAME
```

Kill all processes with a specific name
```bash
killall NAME
```

Kill a process running on the specified port
```bash
fuser -k PORT/tcp
```
## Overview

Bash supports a rich command line interface with lots of in-built commands, as well as thousands more that can be installed as programs.

### Cheat Sheet
[Linux Cheat Sheet.docx](.files/Linux Cheat Sheet.docx)
## Overview

Bash is the language that native Linux shell scripts are written in. These files are executable on any linux machine, and normally end with .sh

All normal [bash commands](../Bash Commands) can also be used within shell scripts.
[TOC]

## Overview
**if** - Perform a set of commands if a test is true
**else** - If the test is not true then perform a different set of commands.
**elif** - If all previous tests returned false then try this one.

## Syntax
If statements have a condition within square brackets, with spaces either side. This is followed by a new line and the then keyword. The code to execute when the condition is true follows. Finally the if statement is ended with an fi. Additional elif and else statements can also be added.
```bash
if [[ CONDITION ]]
then
     CODE
elif [[ CONDITION ]] && [[ CONDITION ]]
then
     CODE
else
     CODE
fi
```

## Inline if statement
If statements can be used inline, to evaluate conditions through the command line or just to save typing!
```bash
# Print hello if condition is true
[[ condition ]] && echo "hello"
```

## Conditional Operators
| Operator | Description |
| --- | --- |
| && | And operator, only true if both conditions are true e.g. true && true = true |
| II | Or operator, true if either condition is true e.g. true II false = true |

## Conditions
| Condition | Description |
| --- | --- |
| $VARIABLE == true | Test if a variable is true, same syntax for false. This works for normal booleans and strings of "true" or "false" |
| ! CONDITION | Not EXPRESSION, e.g. ! true == false. Note there must be a space after the ! |
| -n STRING | True if the length of a string is greater than zero |
| -z string | True if the string is empty or null |
| STRING1 == STRING2 | True if the strings are equal |
| STRING1 != STRING2 | True if the strings are not equal |
| INTEGER1 -eq INTEGER2 | INTEGER1 is numerically equal to INTEGER2 |
| INTEGER1 -gt INTEGER2 | INTEGER1 is numerically greater than INTEGER2 |
| INTEGER1 -lt INTEGER2 | INTEGER1 is numerically less than INTEGER2 |
| -f PATH | PATH exists and is a file |
| -d PATH | PATH exists and is a directory |
| -e PATH | PATH exists |
| -r PATH | PATH exists and the read permission is granted |
| -s PATH | PATH exists and it's size is greater than zero (ie. it is not empty) |
| -w PATH | PATH exists and the write permission is granted |
| -x PATH | PATH exists and the execute permission is granted |

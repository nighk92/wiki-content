[TOC]

## Overview
Variables can be created to store values. In bash variables are untyped as the are all essentially a string of characters. However it is possible to store Strings, Integers and Arrays, and perform specific operations on each of them.

## Creating and Using Variables
Creating and using basic variables
```bash
#!/bin/bash

# Strings
myString="test" # set variable value
echo $myString # prints "test"


# Integers
myFirstInt=1 # set variable value
mySecondInt=2 # set another int variable valie
echo $((myFirstInt+mySecondInt)) # Adds numbers together and prints "3"


# Arrays
myArray[0]="test 1" # creates an array adding one element
myArray[1]="test 2" # adds another element to the array
echo myArray[0] # prints "test 1"
echo myArray[1] # prints "test 2"
```

It is possible to assign the output of a command or script to a variable, this is called command substitution
```bash
#!/bin/bash
myVariable=$(ls /etc | wc -l) # this can also be done with backticks: `ls /etc | wc -l `

echo There are $myVar entries in the directory /etc
```

## Command Line Arguments
Arguments can be provided to any script via the command line, and can be used within the script as variables. These arguments are number in the order they were provided and can be used within the script as $1, $2, $3 etc.

For example this script takes in 3 arguments and prints them out:
```bash
#!/bin/bash
echo $1
echo $2
echo $3
```

It can be run like this:
```bash
$: ./testScript "Hello" "I am" "Paul"

# And the following would be output
Hello
I am
Paul
```

## Variable Scope
All bash variables are available within the process they were created in. So a variable made in a script is available throughout that script, however it isn't available to other commands/scripts unless it is exported (see below).

## Exporting Variables
To make a global environment variable which is available to all scripts and processes it should be exported using the export keyword
```bash
#!/bin/bash
myVariable="test"
export myVariable

# This can also be done in one line
export myVariable="test"
```

## Special Variables
Preset system variables that can be used:

* **$HOME** - The home directory of the current user
* **$HOSTNAME** - The hostname of the machine
* **$PATH** - The path to executable files that can be executed directly as commands from any directory
* **$RANDOM** - Returns a different random number each time is it referred to
* **$USER** - The username of the current user
* **$?** - The exit status of the most recently run process

Preset variables that can be used from within a bash script:

* **$0** - The name of the Bash script
* **$1** - $9 - The first 9 arguments to the Bash script
* **$#** - How many arguments were passed to the Bash script
* **$@** - All the arguments supplied to the Bash script
* **$$** - The process ID of the current script
* **$LINENO** - Returns the current line number in the Bash script
* **$SECONDS** - The number of seconds since the script was started


[TOC]

## Overview
The Linux crontab can be used to schedule the running of bash commands or shell scripts. Jobs can be set to run repeatedly at specific times. Each user has a different crontab, where automated jobs are run with their privileges.

## Job format
This is the general format of a crontab job
```
min hour dayOfMonth month dayOfWeek CMD
```

## Examples
```bash
# Command is run every half an hour
0,30 * * * * CMD

# Command is run at midnight every weekday
0 0 * * 1,2,3,4,5 CMD

# Command is run every monday at 1 am
0 1 * * 1 CMD
```

## Editing the Crontab
Edit the crontab for the logged in user, opens in the default text editor (normally Vi)
```bash
crontab -e
```

To programatically **add** a line to the crontab e.g. in a script, run:
```bash
(crontab -l ; echo "<LINE TO ADD") | crontab -
```

To programatically **remove** a line to the crontab e.g. in a script, run:
```bash
crontab -l | grep -v "<LINE TO REMOVE>"  | crontab -
```

## Granting Access
If present only users specified in the cron.allow file can edit the crontab. If neither this file or the cron.deny exist only the superuser can edit the crontab.
```bash
/etc/cron.allow
```

If present users specified in the cron.deny file can’t edit the crontab. If this file exists and the cron.allow file doesn’t, then any users not listed here can edit the crontab.
```bash
/etc/cron.deny
```
## Overview
An Operating System (OS) is the software that runs a computer. They control many low level functions such as task scheduling, usage of peripheral hardware, and allow the running of applications. They almost always come with a command line interface and often have GUI's to make the computer more user friendly. 

The most common operating systems for home use are Windows and Mac OS. The most common operating system for web servers is Linux (or a derivative). The most common operation system for smart phones are Android or iOS.
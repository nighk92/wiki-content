## Overview
Android doesn't have a recycle bin like some operating systems, so once data is deleted there is no easy way to retrieve it.

## How To Recover Lost Data
* Root the device, see [Rooting](Rooting.md)
* Use Recuva to recover lost files or photos
* Use the GT Recovery Android app to recover lost messages or contacts
## Overview
Android is built upon Linux. It is possible to gain access to the underlying Linux functions by rooting the Android device. This gives access to recover lost data and carry out additional operations such as editing the hosts file.

## How To Root
* Install **Kingo Root** from the play store, or install it on a computer
* Follow the instructions from the app

## Problems
In order to root a device the way it boots up must be adjusted. However some Motorola and HTC phones have locked bootloaders which prevents them from being rooted.
## Signing Commits

Enabling signed commits on a git repository provides an additional level of assurance that the commit was submitted by the user owning the GPG key.

**1.** Install GPG
GPG is the encryption library. On Centos 8+ the install command is:
```bash
sudo dnf install -y gnupg2
```

**2.** Create a new GPG key
```bash
gpg --gen-key  

# Then enter the following information:
# Real name - Enter your Guest ID, e.g 'Fred X'
# Email address - Enter your Guest email address
# Choose O (capital o) to accept the user id - or one of the other letters to edit
# Enter a passphrase and confirm
```

**3.** Make a note of the keyid 
You should see ourput similar to the below. Make a ntoe of the Key ID which is the line commencing:  gpg: key ... (In the example above, "478...637")
```
gpg: key 478**********637 marked as ultimately trusted
gpg: revocation certificate stored as '/home/km-dev/.gnupg/openpgp-revocs.d/F9FE******************************.rev'
public and secret key created and signed.
 
pub   rsa2048 2022-03-10 [SC] [expires: 2024-03-09]
      F9FE************************************
uid                      Fred X <fredx@mycompany.com>
sub   rsa2048 2022-03-10 [E] [expires: 2024-03-09]
```

**4.** Obtain the public key
To add the GPG key to Bitbucket you need to export the public part of the key and then import this into Bitbucket. Export the public part of the key using the Key ID:
```bash
gpg --armor --export <KEYID> 
```

**5.** Copy the key
The output from above should start with `-----BEGIN PGP PUBLIC KEY BLOCK-----`  and end with `-----END PGP PUBLIC KEY BLOCK-----`, copy all of this to the clipboard.

**6.** Add the key into Bitbucket

1. Navigate to Bitbucket in your browser (and login if you need to)
2. Click on your profile picture in the top-right, then select "Manage account"
3. Click "GPG keys", then "Add key"
4. Paste the PGP Public key block copied from above, then click "Add key"

**7.** Configure Git to use GPG key
We need to assign the keyid of the GPG key to use in git's configuration. Follow the commands below.

```bash
git config --global user.signingkey <KEYID>
# e.g. git config --global user.signingkey 478**********637 

git config --global commit.gpgsign true
```
From now on all git commits you make will be signed automatically.

**8.** Set the repository to require GPG key
You must have administrative rights to the repository (or project) to be able to enable commit signing.

1. Open a browser and navigate to the repository in Bitbucket.
2. Open the "Repository Settings" dashboard by selecting the cog in the left pane.
3. Click on "Hooks"
4. Choose the appropriate method for signing commits. Click on the dropdown menu next to "Verify Commit Signature" then ...
5. Select "Enable" to enable signed commits for this repo only.
6. Select "Inherited (enabled)" when signed commits are enabled for the whole project.
7. Verify that signed commits are now required for the repo.

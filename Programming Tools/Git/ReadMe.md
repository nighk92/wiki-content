## Overview

Git is a command line version control system used to track changes in computer files and coordinate work on those files among multiple people. It is primarily used for software development, but it can be used to keep track of changes in any files.

It involves storing code in a repository, usually on a Git server. This repository can be copied to a local machine and changes can be pushed to the remote repository. Multiple people can pull the code to their machines and work on the code locally, and Git will merge everyones changes together.

The most common Git servers are [Bitbucket](https://bitbucket.org) and [GitHub](https://github.com/), which also provide a nice user interfaces.
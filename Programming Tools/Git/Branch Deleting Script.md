## Branch Deleting Script
The following bash script can be used to automatically delete branches from a repo

```bash
# Usage
# ./deleteBranches.sh <pathToRepo> <deleteRemoteBranches> <branchesNotToDelete>
# ./deleteBranches.sh git/repo true "branchNotToDelete remotes/origin/branchNotToDelete"

FOLDER=$1
DELETE_REMOTE=$2
RESERVED_BRANCHES=(develop master remotes/origin/develop remotes/origin/master $3)

cd $FOLDER
git fetch

OLD_IFS=$IFS
IFS=$'\n'

for BRANCH in `git branch -a`
do
    BRANCH=`echo $BRANCH | awk '{$1=$1};1'` # remove leading/trailing spaces

    if [[ $BRANCH == "*"* ]]
    then
        echo "Skipped current branch: $BRANCH"
    elif [[ " ${RESERVED_BRANCHES[@]} " =~ " $BRANCH " ]]
    then
        echo "Skipped reserved branch: $BRANCH"
    elif [[ $BRANCH == "remotes/origin/HEAD"* ]]
    then
        echo "Skipped HEAD branch: $BRANCH"
    elif [[ $BRANCH == "remotes/origin/"* ]]
    then
        BRANCH=${BRANCH#'remotes/origin/'} # remove '/remotes/origin/' prefix
        if [[ $DELETE_REMOTE == "true" ]]
        then
            git push origin --delete $BRANCH
            echo "Deleted remote branch: $BRANCH"
        else
            echo "Skipped remote branch: $BRANCH"
        fi
    else
        git branch -D $BRANCH
        echo "Deleted local branch: $BRANCH"
    fi
done

IFS=$OLD_IFS
```
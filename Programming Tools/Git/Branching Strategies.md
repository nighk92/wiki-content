## Branching Strategies
There are various branching strategies that can be applied to a project. Neither strategy is better than the other, the main aim of having a strategy is just to ensure consistency across the team and save developers from merge hell! 

Below are two of the most common strategies:

### Strategy 1
This strategy has just a single main branch called Master that feature branches are merged into and releases are created from.

1. Create a branch from Master per task, naming branch with ticket number and title
2. Commit changes to the feature branch
3. Once all changes have been made create a pull request to merge the feature branch into Master
4. At least two people must approve, the tests pass, and the CI build passes
5. The creator of the feature branch then merges it when they are ready to

**Main Rule:** Nobody can commit directly to the Master Branch

### Strategy 2 (called the "Gitflow Workflow")
This strategy has a Master branch that is used to create releases from, and a Develop branch that feature branches are merged into.

1. Create a branch from Develop per task, naming branch with ticket number and title
2. Commit changes to the feature branch
3. Once all changes have been made create a pull request to merge the feature branch into Develop
4. At least one person must approve, the tests pass, and the CI build passes
5. The creator of the feature branch then merges it when they are ready to

**Main Rules:** Nobody can commit directly to the Master or Develop Branch
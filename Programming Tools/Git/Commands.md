[TOC]

## Setup
### Initial Setup
Used to set your personalised git settings, these are global so they will affect all git repositories on the local machine.
```bash
git config --global user.name "Joe Bloggs"
git config --global user.email "you@domain.com"
git config --global core.editor notepad
```

### Setup SSH Key
Most git repositories use SSH for secure access. In order to use SSH you will need to make an SSH key locally. From a linux or git bash run:
```bash
# This will create a /home/user/.ssh/id_rsa.pub on linux
# Or C:\Users\<user>\.ssh\id_rsa.pub on windows
ssh-keygen

# Show the public SSH key
strings /home/<user>/.ssh/id_rsa.pub # strings C:\Users\<user>\.ssh\id_rsa.pub for windows

# Then copy the output of the public SSH Key
# Open repository and add your SSH key
# E.g. In Bitbucket:
# Select profile link in top right hand corner
# In the dropdown select manage account
# Select SSH keys, then add, and paste in your public SSH key
```

---

## Find Git Settings
View settings for a particular repository.
```bash
git config --list # gives all settings
git remote -v # gives repository name and url
```

---

## Repository Commands
### Clone repository
Clones an existing git repository. Copies the repository from a remote server and stores it locally.
```bash
git clone URL
```

### Create and Link to Remote Repository
Creates a new git repository in the existing folder, and links it to a remote repository
```bash
git init
git remote add origin REPOSITORY_URL
```

### Change remote repository
Changes the remote URL of a repository
```bash
git remote set-url origin REPOSITORY_URL
```

### Add Upstream Repository
An upstream repository can be useful when your repository is forked from another, and you wish to keep up tp date with the changes of the original "upstream" repository.
```bash
git remote add upstream REPOSITORY_URL
```

---

## Branch Commands
### List Branches
A git repository can have different branches (versions) of the code. These commands can list the different branches.
```bash
git branch # lists local branches
git branch -a # lists local and remote branches
```

### Make a New Branch
A new branch can be made, to work on code without affecting the main (master) version.
```bash
git branch NAME # make branch
git checkout NAME # switch to branch

# or do both in one command
git checkout -b NAME
```

### Switch Branches
To make changes to code on a branch you must switch to using that branch.
```bash
git checkout NAME # also works for tags
```

### Merge Branches
This takes all the changes from one branch and applies them to another.
```bash
git checkout origin BRANCH_TO_MERGE_INT0
git merge BRANCH_TO_MERGE # add –no-ff to always create a merge commit
```

### Rename a Branch
Renames the current branch
```bash
git branch -m NEW_NAME
```

### Delete a Branch
This deletes a branch both locally and on the remote server. Any un-merged changes on this branch will be lost.
```bash
git branch -d NAME # delete locally
git push origin --delete NAME # delete from remote repository
git fetch -p origin # removes any local branches that have been deleted from the remote repository
```

### Branch Workflow GUI
Gitk is a GUI that shows the status of all your branches, and a list of commits. It can be used to cherry pick commits from one branch to another without merging, or reset the status of a branch to a specific commit.
```bash
gitk -all
```

### Branch Status
This can show what local files you have changed, what has been committed, and the status of the remote branch.
```bash
git status
```

---

## Pushing/Pulling Changes
### Commit
To save changes to code on a branch they must be committed.
```bash
# Adds all changed files within the folder to the commit
# You can specify individual files as well
git add .

# This will open a text editor and ask for a message
git commit

# Or use the GUI version
git gui
```

### Push Local Changes
Push committed changes to the remote repository.
```bash
git push origin BRANCH

# Or use this to push changes from the current branch
git push origin HEAD
```

### Pull Remote Changes
Pull changes from the remote repository.
```bash
# Ensure you are on the correct local branch first
# Otherwise git will attempt to merge the two different branches
git pull origin BRANCH
```

### Fetch Latest Changes
Fetch changes on all branches from the remote repository. This will update the status of all local branches, but won't make any changes to local files like a git pull does.
```bash
git fetch
```

### Stash Local Changes
Local changes you don't want to loose can be saved for a merge at a later date
```bash
git stash
```

Then to re-apply these changes
```bash
git stash apply
```

### Rebase with Remote Branch
Takes all new commits on the current branch, and puts them on top of any changes from the specified branch.

Assume the following history exists and the current branch is "topic":
```
          A---B---C topic
         /
    D---E---F---G master
```

From this point, the result of either of the following commands:
```bash
git rebase master
git rebase master topic
```

Would be:
```
                  A'--B'--C' topic
                 /
    D---E---F---G master
```

###Rebase Current Branch
This can be used to change the order of commits, and merge commits together.
```bash
git rebase -i
```

### Pull Upstream Repository Changes
To pull the changes from an upstream repository, e.g. one you have forked from, and then push those changes to your own repository run the following:
```bash
git checkout BRANCH
git fetch upstream
git pull upstream BRANCH # Apply all changes from the upstream branch to your own
git push origin BRANCH
```

---

## Tags
### Create a Tag
Tags can be used to mark a specific commit e.g. to specify a version of an application ready for deployment. They are branch independent.
```bash
git pull --tags # pull tags from remote repository
git tag NAME
git push --tags # push created tag to remote repository
```

### Sign a Tag
To sign a tag follow the Create a Tag above, adding `-s` to the tag command. When prompted type in a message and save the file.
```bash
git tag -s NAME
```

### Delete a Tag
Tags can be deleted from the local and remote repositories.
```bash
git tag -d NAME # delete local tag
git push origin :refs/tags/NAME # remove tag from remote repository
```

---

## Reverting Changes
### Reset Branch to Last Commit
Reset the branch to the last commit, removing any local changes.
```bash
git reset –hard
```

### Reset Branch to Remote
Reset the local branch to the remote branch, this also removes any un-pushed commits.
```bash
git reset –-hard origin/BRANCH
```

### Reset Changes to a Single File
Reset changes to an individual file, without affecting other changes.
```bash
git checkout FILE
```

### Remove Untracked (New) Files
Removed all untracked files, which aren't removed by a reset
```bash
git clean -fd
```

---

## History & Cleaning
### View History
See the history of past commits.
```bash
git log
```

### Clean Repository
This reduces the amount of disk space the repository takes up.
```bash
git gc --aggressive --prune=now
```

### Delete all Commit History
This will delete all commit history for a branch, this can be done on all the branches in a repository to permanently delete all commit history
```bash
git checkout --orphan TEMP_BRANCH # Create a temporary branch
git add -A # Add all files to the branch
git commit # Commit the files
git branch -D ORIGINAL_BRANCH # Delete the old branch
git branch -m ORIGINAL_BRANCH # Rename the temporary branch the the old branches name
git push -f origin ORIGINAL_BRANCH # Push the changes
```

### Completeley remove a file from the repo (including removal from commit history)
Once a file is committed it remains in the git history forever. This can cause the repository to swell in size even if the file is removed in a later commit. To completely remove a file the commit history must be re-written. These commands will remove a file from the commit history:
```
git filter-branch -f --index-filter "git rm -rf --cached --ignore-unmatch-files /path/to/file" --prune-empty HEAD
git push --force <branch-name>
```
---

## Bundling
The git bundle command offers a way to save your git commits into a file, which can then be transferred to another repository. This is useful to keep two separate repositories in sync i.e. on disconnected networks.

### Create a bundle
Create a bundle of a specific branch
```bash
git bundle create file.bundle BRANCH --tags
```

### Apply a bundle
Apply a bundle to another repository by copying the file to the machine where the other repository is cloned and pulling it
```bash
git pull file.bundle BRANCH
```

---

## Subtrees
Subtrees can be used to make one git project dependent on another e.g. so multiple projects can have the same common base of code

New repos are libraries, incidents & clearance pull them all out in your workspace using:
```bash
git clone apps@git:src/libraries.git
git clone apps@git:src/incidents.git
git clone apps@git:src/clearance.git
```

Setup the use of subtrees by using the following command in the incidents and clearance folders:
```bash
git remote add -f libs apps@git:src/libraries.git
```

This creates a mapping of libs to the remote repo apps@git:src/libraries.git to be used in future commands.

To pull changes do the following:
```bash
git pull -a origin develop
git subtree pull --prefix=libs libs develop --squash

# If you don't do the git remote add -f .... command then you'd have to use:
git subtree pull --prefix=libs apps@git:src/libraries.git develop --squash
```

To commit changes and push to remote repo use the following:
```bash
git add .
git commit
git push origin develop
git subtree push --prefix=libs libs develop --squash
```

---

## Ignoring Specific Changes
Git Ignore
The .gitignore file can be used to prevent certain files/folders being uploaded to the remote repository. To use it put a .gitignore file at base directory of repository, and specify the files/folders you want to be ignored. E.g.
*.class – ignores any files ending with .class
public – ignores any files in public directory and subdirectories
public/publicImages – ignores files in publicImages directory
The files/folders listed will no longer be added to your commits, or been seen if you run a git status.

### Temporarily Ignore File Changes
This can be used as a temporary alternative to putting something in the .gitignore file.
```bash
git update-index --assume-unchanged <file>

# To make the file visible to git again run
git update-index --no-assume-unchanged <file>
```

### Remove Ignored Files From Repo Without Deleting Locally
This can be useful if a file has been added to the .gitignore file, but already uploaded to the remote repository. These commands will remove ignored files from the remote repository.
```bash
git rm –r –f –-cached
git add .
git commit
```

---

## Cheat Sheet
[Git Cheat Sheet.docx](.files/Git Cheat Sheet.docx)
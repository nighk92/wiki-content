[TOC]

### Local Build
This will build a maven project, compiling the code and running tests. This must be run for a folder with a pom.xml file in it.
```bash
mvn clean install
```

### Skip Tests
This will build a maven project without running any tests.
```bash
mvn clean install -DskipTests
```

### Fail At End
This will build every maven project under a parent project, if the build for any of the projects fail it will continue building the others and only report failures at the end.
```bash
mvn clean install --fail-at-end
```

### Deploy
This will build and deploy a maven project, usually to a nexus repository.
```bash
mvn clean deploy
```

### Update Pom Version
This will build update the version numbers in all of you project's pom files
```bash
mvn versions:set -DnewVersion=2.50.1-SNAPSHOT

# Verifiy the changes are correct then run the following, which deletes the backup pom files:
mvn versions:commit

# If the changes are incorrect this command will restore the original pom files
mvn versions:revert

# Additional Options:
# -DgenerateBackupPoms=false - Stops the generation of backup poms, essentially the
#                              same as running versions:set then versions:commit
```
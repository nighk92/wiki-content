## Overview
Eclipse is a powerful fully open source IDE. It is primarily used for Java development, but has support for many programming languages and a wide range of plugins.

### Preferences
Eclipse has many preferences and settings. These can be imported/exported as a file. Paul's preferences can be found in [eclipse-preferences.epf](./.files/eclipse-preferences.epf).

This can be imported by:
1. Go to File -> Import
2. Search for `Preferences` and select it
3. Choose a preferences file
4. Tick `Import All`
4. Click `Finish`
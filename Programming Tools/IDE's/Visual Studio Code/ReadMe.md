## Overview
Visual Studio Code (VS Code) is an IDE developed by Microsoft. It supports many languages but is best when used for writing Javascript or HTML/CSS code. 

## Features
* Syntax highlighting
* Autocomplete
* Integration with git
* Debugging
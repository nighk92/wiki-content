[TOC]

## Steps
**1.** VS Code can be downloaded from: https://code.visualstudio.com/

**2.** Update the settings, open **File > Preferences > Settings**, and enter the following into the **User Settings** window on the right:
```json
{    
    "editor.minimap.enabled": false,    
    "editor.wordWrap": "on",
    "html.autoClosingTags": false,
    "javascript.autoClosingTags": false,
    "typescript.autoClosingTags": false,
    "editor.tabSize": 2
}
```

**3.** Extensions can be installed from inside the application:

* Click the extensions icon from the bottom of the left hand menu
* Search for extensions by typing their name, see recommended extensions below

## Recommended Extensions
### ftp-sync
Automatically sync code changes to a different folder or remote server, this can be used to instantly deploy code changes to another location

### Code Spell Checker
Basic Spell Checker

#### Recommended Settings
```json
{
    "cSpell.language": "en-GB",
    "cSpell.userWords": [
        "autoscale",
        "Autowired",
        "Burstable",
        "Centos",
        "checkstyle",
        "classpath",
        "Cobertura",
        "Easymock",
        "Easymock's",
        "Fargate",
        "findbugs",
        "firewalld",
        "Hadoop",
        "jacoco",
        "Jboss",
        "Jenkinsfile",
        "Keytool",
        "Kubernetes",
        "Mockito",
        "POJO",
        "powermock",
        "Powerpoint",
        "routable",
        "Servlet",
        "Servlets",
        "sharded",
        "spotbugs",
        "Tiering",
        "timebox",
        "timeboxed",
        "timeboxing",
        "Timsort",
        "websockets",
        "Wildfly"
    ]
}
```
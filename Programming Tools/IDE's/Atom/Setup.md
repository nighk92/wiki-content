## Steps
**1.** Atom can be downloaded from https://atom.io/

**2.** Plugins can be installed from inside the application:

*  Navigate to **File > Settings**
* Select **Install**
* Search for packages by typing their name, see recommended plugins below

**3.** Adjust the editor settings so that tabs are 4 spaces wide:

* Navigate to **File > Settings**
* Select **Editor**
* Scroll down to the **Tab Length** setting and change the value to **4**

## Recommended Plugins
| Name | Description |
| --- | --- |
| docblockr | Aids in code commenting, automatically creating comment templates when a comment is started |
| file-icons | Improves the appearance of the file icons in the file tree |

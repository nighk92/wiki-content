## Oveview
Atom is an open source IDE developed by GitHub. It has support for many programming languages but is primarily used for writing Javascript and HTML/CSS code.

## Features
* Syntax highlighting
* Autocomplete
* Integration with git
## Overview
An IDE is a application that aids in the writing of code. These can range from simple text editors to complex applications. Some of their functionality includes:

* Autocomplete
* Syntax highlighting
* Code compiling
* Debugging
## Overview

A programming tool is anything that aids the development or deployment of computer applications. These include:

* IDE's which aid in the writing of code
* Source control systems like Git which store the code
* Dependency management tools like Maven
* Continuous integration tools like Jenkins, which build and test the code
* Automated deployment tools like Docker
## Installing Jenkins (Linux)
To install Jenkins on a Linux server follow these steps:

**1.** Install the Latest version of Java
```bash
sudo apt-get update
sudo apt-get install default-jre
```

***2.*** Set the JAVA_HOME environment variable
```bash
# Run the following, and copy the Java path for your preferred installation
sudo update-alternatives --config java

# Open the system environment file
sudo vi /etc/environment

# Add the following line to the end of the file
JAVA_HOME="<path>"

# Save the file and reload it
source /etc/environment

# Check the environment variable has been set
echo $JAVA_HOME
```

**3.** Add the Jenkins repository
```bash
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
```

**4.** Then add the following entry in your **/etc/apt/sources.list**
```bash
deb https://pkg.jenkins.io/debian-stable binary/
```

**5.** Install Jenkins
```bash
sudo apt-get update
sudo apt-get install jenkins
```

This package installation will:

* Setup Jenkins as a daemon launched on start. See **/etc/init.d/jenkins** for more details.
* Create a **jenkins** user to run this service.
* Direct console log output to the file **/var/log/jenkins/jenkins.log** -  check this file if you are troubleshooting Jenkins.
* Populate **/etc/default/jenkins** with configuration parameters for the launch, e.g **JENKINS_HOME**
* Set Jenkins to listen on port **8080**. Access this port with your browser to start configuration.

**6.** Initial Setup

Jenkins is initially configured to be secure on first launch. During the initial run of Jenkins a security token is generated and printed in the console log:
```bash
*************************************************************

Jenkins initial setup is required. A security token is required to proceed.
Please use the following security token to proceed to installation:

41d2b60b0e4cb5bf2025d33b21cb

*************************************************************
```

Open Jenkins in your browser at **http://<hostname>:8080**. This will open the setup wizard, which will ask you to enter the security token, and then setup an initial username and password.

**7.** Install Plug-ins

The setup wizard will install the initial plug-ins for the Jenkins server. You can install more by logging into Jenkins as an Admin user, then clicking **Manage Jenkins** on the left, then **Manage Plugins > Available**. You can search for new plug-ins and install them from this page.

Recommended plug-ins:
* [Pipeline](https://wiki.jenkins.io/display/JENKINS/Pipeline+Plugin) - Creating jobs in Groovy via a Jenkinsfile
* [Checkstyle](https://wiki.jenkins.io/display/JENKINS/Checkstyle+Plugin) - Displays the results of checkstyle reports
* [Findbugs](https://wiki.jenkins.io/display/JENKINS/FindBugs+Plugin) - Displays the results of findbugs reports
* [Cobertura](https://wiki.jenkins.io/display/JENKINS/Cobertura+Plugin) - Displays code coverage reports from Cobertura, e.g. Junit test coverage
* [Cucumber](https://wiki.jenkins.io/display/JENKINS/Cucumber+Reports+Plugin) - Displays Cucumber testing report

**8.** Install PhantomJS (optional)

PhantomJS is a headless browser which can be used for automated GUI testing. See the [PhantomJS page](PhantomJS.md) for more information on installing.
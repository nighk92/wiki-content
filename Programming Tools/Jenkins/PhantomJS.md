## Overview
PhantomJS is a headless browser that can be used for GUI testing using an automated tool such as selenium.

## Install PhantomJS (Linux)
**1.** Download PhantomJS

The download links can be found here: http://phantomjs.org/download.html. Pick the URL you need then to download the file on the server run this cmd:
```bash
wget <url>
```

**2.** Extract the tar file
```bash
tar -xvfj phantomjs-X.X.X-linux-x86_64.tar.bz2
```

**3.** Set up SSL certificates (optional)

SSL certificates will be required if you need to access secure websites over https, that you log into using a certificate. You will need a separate Certificate and Private key file, these can be extracted from a .pfx or .pem file. See the **certificates** section of the [Linux Commands page](../../Operating Systems/Linux/Bash Commands.md) for examples on extracting certificates and private keys. 

**4.** Create a start script

Create a **start.sh** script under the **phantomjs-X.X.X-linux-x86_64** folder, that runs the following:
```bash
# The ssl options are only needed if you have setup ssl certificates
~/phantomjs-2.1.1-linux-x86_64/bin/phantomjs  \
    --wd=hostname:<PORT_TO_RUN_ON> \
    --web-security=false \
    --ignore-ssl-errors=true \
    --ssl-protocol=any \
    --ssl-client-passphrase=XXXX \
    --ssl-client-certificate-file=/path/to/cert \
    --ssl-client-key-file=path/to/private/key
```

**5.** Create a Jenkins Job to run PhantomJS

You can create a Jenkins job that runs the **start.sh** script to run PhantomJS, then it will be available on the port you specified to run automated GUI tests.
## Overview
Jenkins is an open source automation server written in Java. It is used for the continuous integration of code. It can pull code from various places, build, execute tests, deploy to remote severs, and provide nice reports for all of this.

There are many plug-ins which can be installed to add extra functionality.
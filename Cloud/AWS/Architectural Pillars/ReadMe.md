[TOC]

## Overview
All AWS services are underpinned by the 5 key architectural pillars:

### Operational Excellence
* Ensure all solutions can scale and offer excellent performance under all circumstances
* Automate as much as possible, ensuring infrastructure can adapt under demand
* Use an infrastructure as code solution to build your infrastructure
* Make frequent, small, reversible changes
* Refine operations & procedures frequently
* Anticipate and cater for failures
* Learn from all operational failures

### Security
* Implement multiple layers of protection
* Enable https
* Enable encryption at rest
* Audit log users actions
* Use AWS services like Shield and WAF to protect services
* Always protect the root account
* Implement strict IAM rules, abiding by the rules of least privilege i.e. give out the minimum permissions needed and no more
* Prefer IAM roles to ssh keys
* Prefer server-side data encryption over client-side encryption, as it is easer and more performant

### Reliability
* Solution is split into multiple tiers ensuring decoupling so if there is an issue it will only affect a small part of the system
* Prevent single points of failure
* Ensure high availability by spreading across availability zones
    * A production system should never just reside in a single availability zone
* Need to choose between
    * High availability where services remain running but performance may be degraded
    * Fault tolerance with no performance degradation, where additional resources are deployed so that if something goes down performance is not affected
* RTO - Recovery Time Objective, how long until services/data are restored
* RPO - Recovery Point Objective, how long since the last backup, so the last point in time data/servers can be restored from

### Performance Efficiency
* Choose performant databases/storage
* Apply caching where possible
* Design for elasticity and scalability
* Vertical scaling = larger instances
* Horizontal scaling = more instances

### Cost Optimisation
* Pay only for what you use
* There are charges for compute, storage & data transfer
    * But instance storage is free (well included in hourly instance cost)
* Pay less by making use of reserved EC2 instances / using a savings plan
* Pay less by using spot EC2 instances
    * Can hibernate to avoid loss of data
    * Spot blocks allow you to reserve a time of up to 6 hours on the spot market
* Use serverless Lambdas so you don't pay for idle compute time
* Costs reduce per GB for large volumes of usage
* Enable auto scaling to provide the optimum amount of resource vs resource cost
* Make use of cheaper storage solutions where possible e.g. archiving data to Glacier
* CloudFront caching can save costs on data retrieval from S3
    * There is no cost for data transfer between S3 and CloudFront

## Potential Solutions
* AWS managed services should always be the preferred option as they are always performant and reliable
* Where possible decoupled or event driven architectures should be preferred
* Use an SQS Queue for async interaction
    * Messages are safely persisted in the queue
    * It acts as a buffer preventing downstream systems from becoming overwhelmed under heavy usage
    * Allows downstream systems to autoscale to cope with high loads, then scale down again to save on cost
* Load balancers can be used to increase performance and remove single points of failure
    * Provides health checks and weighted routing
        * No cache on health checks
    * Distributes requests
    * Real time responses
    * Stops a node being a single point of failure
    * Can distribute traffic over multiple availability zones within a single region
* Elastic IP's
    * Can be moved between instances
    * If a server goes down a new one can be spun up with the same ip address
    * Decouples an IP address from an instance
* Route 53
    * Provides health checks and weighted routing
    * Provides DNS's which are decoupled from instances
    * Route policies can be used to set failovers
    * Can distribute traffic over multiple regions
    * DNS is cached, making this slower than a load balancer to remove unhealthy instances
* Fully serverless applications
    * S3 for frontend websites
    * API Gateway for HTTP or REST APIs
    * Lambdas / State Machines for processing of data
    * DynamoDB or RDS databases to store data
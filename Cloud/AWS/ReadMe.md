## Overview

AWS (Amazon Web Services) is a cloud computing service offered by Amazon, and is one of the most popular cloud service providers. They have:

* Datacentres around the world
* Various locations inc. London, Ireland, Europe, America
* Each datacentre has at least 50,000 servers
* 28 total availability zones (some with multiple data centres)
* Over 5 million servers in total!

## Global Infrastructure

AWS services are split into Regions, that are made up of Availability Zones (AZ). Making use of at least two AZs in a region helps you maintain high availability of your infrastructure and its always a recommended best practice. Regions are independent of each other so not all AWS services are available in every region.

![Availability Zones](.images/Availability_Zones.png)

Within a Region each availability zone is given a code name:

![EU Ireland Region](.images/EU_Ireland_Region.png)

An interesting point to be aware of here is that AWS maps these AZ letter identifiers to different physical AZs for different AWS accounts. This ensures that there is a more even distribution of resources across all AZs within a Region. See below for an example of the AZ mappings for 3 separate accounts:

![AZ Mapping Per Account](.images/AZ_Mapping_Per_Account.png)

## Edge Locations

Edge Locations are AWS sites deployed in major cities and highly populated areas across the globe. They far outnumber the number of availability zones available.

While Edge Locations are not used to deploy your main infrastructures such as EC2 instances, they are used by AWS services such as Amazon CloudFront and AWS Lambda@Edge to cache data and reduce latency for end-user access by using the Edge Locations as a global Content Delivery Network (CDN). Edge Locations are primarily used by end users who are accessing and using your services. For example, you may have your website hosted on EC2 instances and S3 within the Ohio region with a configured Cloud Front distribution associated. When a user accesses your website from Europe, they would be re-directed to their closest Edge Location (in Europe) where cached data could be read on your website, significantly reducing latency.

In November 2016, AWS announce a new type of Edge Location, called a Regional Edge Cache. These sit between your CloudFrontOrigin servers and the Edge Locations. A Regional Edge Cache has a larger cache-width than each of the individual Edge Locations, so when data expires from the cache at the Edge Locations, the data is retained at the Regional Edge Caches. Therefore, when data is requested at the Edge Location that is no longer available, the Edge Location can retrieve the cached data from the Regional Edge Cache instead of the Origin servers, which would have a higher latency. 

![Edge Locations](.images/Edge_Locations.png)

## Shared Responsibility Model
Defines the relationship between AWS and the customer:

* AWS's responsibility = Security of the Cloud
* Customer's responsibility = Security in the Cloud

![Shared Responsibility Model](.images/Shared_Responsibility_Model.jpg)
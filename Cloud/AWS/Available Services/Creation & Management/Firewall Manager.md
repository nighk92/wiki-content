## Overview
* AWS Firewall Manager allows you to manage WAF settings over multiple AWS accounts
* AWS Organisations must first be configured
* Rule Groups
    * Are made up of a number of WAF Rules
    * Rule Groups can be made by you or selected from the AWS Marketplace
* AWS Firewall Manager Policy
    * Consists of rule groups
    * You can only have 1 custom rule group and 1 marketplace rule group per policy
    * These policies are applied to different accounts within your organisation
    * Conditions can be added to define which AWS Resources the policies will be applied to
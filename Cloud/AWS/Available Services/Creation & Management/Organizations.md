## Overview
* AWS Organisations enables the management of several AWS accounts from a single "master" account
* It can be used to set payment options and various security options
* Root:
    * Is the base folder that all other resources are put into
* Organisation Units (OU):
    * Are used to group accounts together
* Service Control Policies (SCPs):
    * Set the available services and security settings within your Organisation / Organisation Unit / Individual Accounts
    * Their settings override any IAM policies within an account
    * By default everything is enabled unless blocked by an SCP
    * Going down the organisation hierarchy the SCPs used can only get more restrictive e.g. an intersection of SCPs is applied (see diagrams below)

![SCP Simple](.images/SCP_Simple.png)

![SCP Intersection](.images/SCP_Intersection.png)

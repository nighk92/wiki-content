## Overview
AWS Systems Manager enables the control of AWS resources at scale.

It consits of 4 main features:

* Operations Management
* Application Management
* Change Management
* Node Management

### Remotely Accessing EC2 Instances
As part of the Node Management feature, Systems Manager can be used to apply operations to batches of EC2 instances. You can apply patches, run ansible scripts or even bash commands.

This involves the Systems Manager agent being installed on each EC2 instance.

The Systems Manager Run Command requires no ssh keys or inbound ports to be open; it operates entirely over outbound HTTPS (which is open by default for security groups).
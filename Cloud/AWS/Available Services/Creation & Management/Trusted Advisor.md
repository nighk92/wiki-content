## Overview
* AWS Trusted Advisor recommends improvements across your account based on best practices
* It covers the following topics:
    * Cost optimisation
    * Performance
    * Security - Security Groups, IAM Use, MFA on Root Account, EBS or RDS public snapshots
    * Fault Tolerance
    * Service Limits - Checks when a service limit reaches 80% or more
* There are 100+ checks that the Advisor Performs
* The number of checks available depend on your support contract with AWS, only Business or Enterprise accounts have access to all the checks
* Email notifications/reports can be sent out
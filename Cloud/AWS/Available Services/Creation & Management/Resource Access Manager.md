## Overview
* AWS Resource Access Manager allows the secure sharing of resources across AWS accounts
* It negates the need to duplicate resources across accounts
* It supports sharing of things like: Subnets, Capacity Reservations, Licence Configurations and Aurora DB Clusters
* For example an account can gain access to view another accounts subnet in the AWS console, and deploy EC2 instances into it
    * The secondary account can't administer or delete the subnet

![Subnet Sharing](.images/Subnet_Sharing.png)
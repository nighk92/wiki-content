## Overview
AWS provides several migration services to aid migration to the cloud.

## Migration Hub
Used to discover and manage migrations, it offers the following functionality:

* Migration Discovery
    * A service that records information about your current servers
    * It records various information like CPU, Memory, Networking and operating system info
    * It can be in the form of an agent installed on your existing servers (AWS Application Discovery Agent)
    * Or for a VMWare deployment an agentless application is available (AWS Agentless Discovery Connector)
    * The discovery stage is optional, and can be skipped for simple migrations
* Migration Management
    * The hub integrates with other AWS services to perform your migration including:
        * AWS Server Migration Service - creates AMIs from your on-premise servers, ready to be made into EC2 instances
        * VM Import/Export
        * AWS Database Migration Service
* Migration Tacking
    * Tracks migration progress

## Migration Strategies (the 6 R's)
* Rehost - lift and shift
* Replatform - optimise the application hosting, without changing code
* Repurchase - move to a new product e.g. a new database
* Refactor - update application code to work better on the cloud
* Retain - leave certain services hosted outside of the cloud
* Retire - turn off lesser used services instead of migrating

## AWS Cloud Adoption Framework (CAF) 
* Helps organisations migrate to the cloud
* Provides a framework for helping migrate services and upskilling an organisation
* It focuses 6 perspectives:
    * Business Perspective
    * People Perspective
    * Governance Perspective
    * Platform Perspective
    * Security Perspective
    * Operations Perspective
## Overview
* AWS CloudFormation is infrastructure as code
* A declarative programming language to describe and provision AWS resources
* Takes the form of JSON or YAML **Templates**
    * These essentially define the desired state of resources you want
    * CloudFormation takes these templates and creates a Stack aligning to your definitions
* A **Stack** is a single set of deployed resources
    * A stack can be created, updated and deleted as a whole
    * If there are issues when deploying a stack then the operation will be rolled back

![Cloud Formation Operations](.images/Cloud_Formation_Operations.png)

* A **Stack Set** allows Stacks to be managed over multiple AWS accounts and regions
* Stacks can be nested, one stack can reference another stack template and deploy it as part of its resources
* Code is region agnostic
* Parameters can be provided from the user running the scripts
* Mappings are config files kept in source control
    * e.g. different pre-set mappings can be provided per region, known as a "region map"
    * Can be useful as AMI ID's differ between regions
* Conditions (a bit like if statements) can be included in scripts to control when a resource is deployed or how it is configured
* Various templates are provided by AWS to use as a starting point
* A drag and drop **Designer** interface is provided
    * Code templates are generated for you based off your design
* Custom Resources
    * Additional resource providers can be added into a template e.g. where your resource isn't supported by CloudFormation
    * A resource provider must be configured e.g. a lambda that can create/update/delete your resource
    * This functionality adds infinite flexibility to CloudFormation
* Notifications can be sent out on a successful/failed stack operation, these are sent via SNS
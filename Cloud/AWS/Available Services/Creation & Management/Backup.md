## Overview
AWS Backup is a managed service to create and manage data backups.

Backup policies/plans can be created to automatically manage backups. These plans include:

* A backup schedule
* A time window for the backups
* Lifecycle rules e.g. transition of backups to cold storage
* A backup value, where backups are stored and encrypted using KMS keys
* Configuration of regional copies
* Tags for the backups

AWS Backup integrates with many services including:

* EBS
* EFS
* RDS

Depending on the service being backed up costs can be applied for both:

* Backup storage
* Backup retrieval (to restore from a backup)
## Overview
* RDS is the AWS managed relational database service
* The following databases are supported:
    * MySQL
    * PostgreSQL
    * MariaDB
    * Aurora
        * AWS's fork of MySQL
        * It has MySQL or PostgreSQL compatibility modes
        * It is the only relational database that has a serverless purchasing option
    * SQL Server
    * Oracle
* Medium read/write speeds
* Full transactional support
* Supports complex queries
* Offers high data durability
* Scaled up by increasing the instance size (vertical) or by using read replicas (horizontal)
    * Read replicas maintain an asynchronous link with the primary database
    * The CloudWatch metric "ReplicaLag" can be used to monitor how up to date read replicas are
* Different EC2 instance types and sizes can be selected depending on performance requirements
    * Instance usage can be covered by a Savings Plan to save on cost
* Databases can be deployed to single or multiple AZs
    * For multi AZs database replicas are deployed in multiple AZs
    * Data synchronisation between replicas is managed for you
    * There is automatic failover if your primary database instance goes down e.g. patching, network failure
* Data is stored in EBS
    * The storage allowances can be set, and the backing disk is scaled automatically for you
    * All the normal EBS storage performance options can be selected
    * Aurora is the exception as this uses shared cluster storage, not EBS
* Parameter Groups and Option Groups can be assigned to a database, these provide additional configuration
* Full database encryption is supported
* Both standard monitoring and enhanced monitoring options are available

## Management & Backups
Backups and patching are managed for you:

* Maintenance windows can be configured to specify the times patches and backups are done
* Automatic backups can be configured
    * Backup retention periods can be configured
    * Database transaction logs are also stored every 5 minutes, so the database can be restored to a specific point-in-time
    * When restoring every DB instance is replaced with a brand new instance made from your backup
* Manual backups can also be done in the form of database snapshots
* Backups are stored in S3
* Backup snapshots can be copied between regions and accounts
    * To copy encrypted snapshots you must first re-encrypt the snapshot with a KMS key for the target region (as KMS keys are region specific)

## Performance Insights
* Provides database specific performance metrics and dashboards
* It records information once a second
* Some queries that run and complete in less than a second may be missed, but as they put little load on the database this isn't generally an issue
* It can be used to see which SQL statements are causing load
* It logs:
    * The SQL Statement
    * The current state e.g. using disk I/O, processing, waiting (for a lock, or some other resource))
    * The user
    * The originating host
* A load chart shows the history of Average Active Sessions (AAS)
    * An AAS of zero means the database is idle
    * An AAS less than the number of CPUs shows the database is coping with load
    * An AAS higher than the number of CPUs means the database is overloaded
* Metrics are published to CloudWatch

## Backtrack
* This is a feature of Aurora MySQL compatible databases
* It allows a database to be reverted to a point-in-time to recover from a mistake e.g. an accidental delete
* It doesn't need a full backup restoration, instead it "rewinds" your database which only takes a few minutes
* This does cause a brief disruption to the database, during this time no read/writes can be made
* The backtrack time/window can be configured in hours up to 72 hours
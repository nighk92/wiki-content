## Overview
* Cloud hosted databases
* 14 different database engines available
* Fully managed and patched
* Types include:
    * Relational
    * Key-value
    * In-memory
    * Document
    * Graph
    * Time series
    * Ledger
## Overview
* A managed ledger database service
* Uses SHA-256 hashes to maintain an immutable transaction log over time (a little like how block chains work)
* It ensures integrity of the data stored
* One use case might be to store a history of insurance claims, where a history of claims needs to be stored and protected from being modified
* Data is placed into tables of Amazon Ion Documents (a superset of JSON)
    * Both structured and unstructured data can be stored
    * A full history of document versions are saved to a journal, which is an append only log of document changes over time
* Indexed storage is managed for you, to enable quick querying over the data
* Integrates well with Kinesis, QLDB streams can be sent to Kinesis for processing as part of an event driven architecture
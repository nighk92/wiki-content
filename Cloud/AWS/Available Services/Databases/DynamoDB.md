## Overview
* DynamoDB is a NoSQL database service
* It is a key-value store, consisting of multiple tables, the store is schemaless like other NoSQL databases
* Data is stored in partitions
    * Each table must have a primary/partition key field
        * This can be a simple unqiue primary key field
        * Or a combination of 2 fields - a partition key and sort key which together must be unique but means multiple records can have the same partition key
        * The partition key value is used to generate a hash which defines the partition for storing the record
    * Partition management happens automatically in the background without affecting performance
    * A partition can have a maxumum of
        * 3000 RCU
        * 1000 WCU
        * 10GB storage
* Indexes can be configured to improve performance
    * Upon querying you must specify the index to use, it isn't automatic like in an SQL database
    * There is a maximum of 20 indexes per global table, and 5 indexes per secondary table
* Supports massive read / write rates
    * This performance is constant even over large datasets, unlike relational databases
* Supports sharding and horizontal scaling
* Simple queries
    * These are quick, but features like joins aren't supported
* Supports transactions
* Grows in size automatically
* Highly available, automatically replicated over 3 AZs
    * It uses an eventual consistency model, meaning out of date data could potentially be returned
* There is a maximum record size of 400 KB
* Throughput can be configured depending on the amount of performance you need:
    * On-Demand mode
        * Capacity scales on demand
        * Is more expensive than provisioned capacity
    * Provisioned mode, used when workload can be predicted, the following settings can be adjusted:
        * RCU = Read Capacity Unit (per second)
        * WCU = Write Capacity Unit (per second)
* Encryption of data at rest is supported

## Backups
* Automatic backups can be configured
    * Backup retention periods are automatically set to 35 days
    * Point-in-time recovery can be enabled and disabled
* Manual backups can also be done in the form of database snapshots
    * These avoid the automatic retention period, so can be kept for longer
* Backups are stored in S3
* Backup snapshots can be copied between regions and accounts
    * To copy encrypted snapshots you must first re-encrypt the snapshot with a KMS key for the target region (as KMS keys are region specific)

## DynamoDB Accelerator (DAX)
* Offers extreme performance
* It is an in-memory cache that can make DynamoDB up to 10 times faster
* It consists of multi-node clusters, with 1 primary node and up to 9 read-replicas
    * Each node is an EC2 instance with a DAX agent installed
* The cluster sits inside your VPC and is a separate resource to DynamoDB
* It can help reduce costs as the RCU allocation for your database can be reduced
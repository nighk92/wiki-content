[TOC]

## Overview
Caches info from databases, providing a simple way to improve read-only performance. These caches can be spread over multiple AZs. There are 2 different types of cache supported:

### Memcached
* This is an in-memory key store service
* Multi-threaded
* Simple
* Low maintenance
* Horizontal scaling with auto discovery
* Supports cluster mode, which is made up of multiple cache nodes (essentially a chunk of RAM)

### Redis
* This is an in-memory data store
* Support for complex data structures
* Includes persistence
* Atomic operations
* Pub/Sub messaging
* Read replicas and automatic failover
* Cluster mode with sharded clusters:
    * Nodes (essentially a chunk of RAM), shards (a group of up to 6 nodes), and clusters (1-90 shards)
## Overview
* DocumentDB is a managed document database service
* Supports storing of JSON documents
* Can scale compute and storage separately as required
* Storage is automatically scaled for you
* It is compatible with MongoDB, so data from Mongo can be migrated into DocumentDB easily using the AWS Database Migration Service
* Databases can span multiple AZs, are are backed by a cluster volume
    * Read-only replicas are supported, with a single primary store
    * Data is synced between replicas synchronously
* Connecting to the database
    * Cluster endpoints - points to the primary DB instance, can be used for read and write access
    * Reader endpoint - points to the read replicas, used for read only operations, round robin is used to spread traffic over the read replicas
    * Instance endpoints - each DB instance also has its own specific endpoint
* Automatic backups are stored in S3
    * Database transaction logs are also stored, so the database can be restored to a specific point-in-time
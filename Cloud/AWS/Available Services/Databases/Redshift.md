## Overview
* Redshift is a data warehouse service, for petabyte scale data processing
* It is a large relational database, providing an SQL query interface
    * Based on PostgreSQL
* Designed for running analytical queries
* Doesn't support transactions
* It can compute aggregated stats from very large sets of data
* It can query any RDS database, Aurora or S3
* It can do federated queries over multiple data sources
* Performance is optimised through
    * Massive parallel processing
    * Columnar Data Storage (reduces disk I/O)
    * Result caching
* Integrates with CloudWatch for performance monitoring
* Data processing consists of 3 stages (an ETL job):
    * Extraction - is the process of retrieving data
    * Transformation - converts and normalises the data to make it easier to process
    * Loading - storing the data in the warehouse
* A Redshift warehouse is made up of clusters that contain:
    * A Redshift engine - including a database
    * One or more compute nodes
        * Where memory and disk space are split into slices so they can execute multiple tasks in parallel
    * A leader node (creates execution plans when multiple compute nodes are present)
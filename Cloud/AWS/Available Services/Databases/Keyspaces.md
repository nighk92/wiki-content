## Overview
* Amazon Keyspaces is a managed wide-column store NoSQL database service that is fully compatible with Apache Cassandra
* Data is queried using the Cassandra Query Language (CQL), which is similar to SQL
* It has unlimited throughput, and is designed for massive scale solutions
* It grows with demand, ensuring you only pay for what you use
* Data consists of
    * Keyspaces - group related tables
    * Tables - where data has a partition key and one or more columns
* There are two throughput modes:
    * On-Demand - Capacity scales on demand
    * Provisioned - Used to ensure a constant performance
* Clients must connect over a TLS connection
## Overview
* Neptune is a managed graph database store
* Is is fast, reliable and secure
* It works well for querying highly linked and relational data
* For querying it supports the Gremlin Traversal Language and SPARQL
* Databases are spread over 3 AZs, backed by a cluster volume
    * Read-only replicas are supported, with a single primary store
    * Data is synced between replicas synchronously
    * Data is replicated over the AZs and errors are automatically repaired (Neptune storage auto-repair)
* Connecting to the database
    * Cluster endpoints - points to the primary DB instance, can be used for read and write access
    * Reader endpoint - points to the read replicas, used for read only operations, round robin is used to spread traffic over the read replicas
    * Instance endpoints - each DB instance also has its own specific endpoint
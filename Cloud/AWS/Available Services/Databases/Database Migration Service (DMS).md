## Overview
* The AWS Database Migration Service (DMS) allows the migration of data to/from databases
* It can be used to transfer data between databases of the same type e.g. from an on-premise database into the cloud (a homogeneous migration)
* In combination with the AWS Schema Conversion Tool (SCT), DMS can also migrate data between different database platforms (a heterogeneous migration)
* Considerations for database migrations:
    * Which target database engine is best
    * What are the performance requirements
    * What are the availability and resiliency requirements
    * Does all the data need to be migrated
    * How will the switchover be managed e.g. temporary pause in writing data, switchover to new database
* Migration Steps:
    1. Create a snapshot/backup of the database to migrate
    2. Create Source and Destination endpoints in DMS
    3. Create & configure a migration task
    4. Create a pre-migration assessment
    5. Run the migration task
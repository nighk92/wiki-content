## Overview
* The Simple Queue Service (SQS) is a message queueing service
* It is highly scalable and reliable
* Encrypting is supported via KMS keys
* It supports the standard producer/consumer model:
    1. A producer puts a message on the queue
    2. A consumer reads the message
    3. The Visibility timer is set to ensure no other producers read the message (default is 30 seconds)
    4. The consumer processes the message
    5. The consumer deletes the message from the queue
        * If proceeding fails and this step isn't reached, the Visibility timer will expire allowing another consumer to read and process the message

## Queue Types
#### Standard
* Unlimited throughput
* Guaranteed at least once delivery
* Best effort ordering, but not guaranteed

#### First in First Out
* High throughput but less than a standard queue
* Exactly once delivery
* Ordering is guaranteed (first in first out)

## Dead Letter Queue
* Can be enabled to store messages that have failed to process for troubleshooting purposes
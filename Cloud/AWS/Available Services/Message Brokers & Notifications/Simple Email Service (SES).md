## Overview
* The Simple Email Service (SES) sends and receive emails
* Rules can be set to automatically receive and reply to emails through this service
    * Emails can be sent to S3, SNS, Lambda or WorkMail
* AWS automatically scans emails for spam and viruses
* By default all emails from EC2 instances are blocked, unless the email allow list is updated
[TOC]

## Overview
* Kinesis can collect, process and analyse real time streaming data
* Data can be available in a stream within milliseconds
* It can ingest real time data such as:
    * Application logs
    * Website clickstreams
    * Social media feeds
* This data can then be processed and stored in a downstream AWS service
* Data within a stream can be encrypted with KMS

![Kinesis Overview](.images/Kinesis_Overview.png)

## Kinesis Streams
Enables the custom processing of data streams, there are two types:

* Data Streams
* Video Streams

A stream is a set of Shards, each of which is just an ordered sequence of up to 1000 data records, which each record consisting of:

* A sequence number
* A partition key
    * Used to split data between shards
    * AWS recommends this is a random number to ensure equal spread of data between shards
* A data blob
    * Base64 encoded
    * Up to 1 MB in size

A stream scales up and down through the process of resharding:

* More shards mean more throughput for a stream
* This is a manual process, and is not managed by AWS, but CloudWatch can be used to automate the process
* Streams are typically made per producer
* **Hot** shards that are being highly utilised can be split into 2 shards
* **Cold** shards that are under utilised can me merged into a single shard
* A stream can only be resharded 10 times in a 24 hour period

![Kinesis Stream Sharding](.images/Kinesis_Stream_Sharding.png)

Streams support the standard producer/consumer model:

* Producers can be anything that sends data into a stream, a single shard can ingest up 1,000 records or 1 MB of data per second
* Consumers are known as Kinesis Stream Applications, and process the data received in real time
* Consumers typically run on a fleet of EC2 instances
* There is a Kinesis API and producer/client library to aid in writing producers & consumers
* A Standard Consumer, uses a polling method to get data from a shard, it can make up to 5 API calls per second to retrieve data, up to a limit of 2 MB per second of data
* An Enhanced Fan-Out Consumer, has data pushed to it via the shard, this removes any data limits but has an additional cost
* Batch operations (Putting/Getting multiple records) can be used to avoid the request rate limits
* Data within a stream can be kept for 1 - 7 days, so a stream can be "replayed" as needed

## Kinesis Data Firehose
* It is a fully managed service that enables the loading of data streams into Kinesis Analytics, S3, Redshift, ElasticSearch or Splunk. 
* There is no need to write your own consumers
* It supports custom data transformation, via a lambda, before data is stored
* It will autoscale as needed, unlike a Kinesis Stream

## Kinesis Analytics
* Enables the writing of SQL queries to continuously read and analyse data
* It is made of the following parts:
    * An Input Stream - such as a Kinesis Stream
    * SQL processing Logic
    * An Output Stream - this can store the output somewhere like S3, or feed into another stream for further processing
## Overview
* The Simple Notification Service (SNS) is a publisher/subscriber messaging service
* It has topics which are a groups of messages
* When a message is published to a topic, **all** subscribers for that topic receive a push notification
* Notifications can be sent in many ways including: 
    * HTTP
    * Email
    * SMS
    * or sent to other AWS services like Lambda and SQS
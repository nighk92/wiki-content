## Overview
DataSync is a service used to sync data between file systems. This can be used to sync on-premise data into AWS, or to sync data between different AWS storage services.
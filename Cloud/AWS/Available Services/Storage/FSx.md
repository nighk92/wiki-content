## Overview 
A shared storage solution similar to EFS. Single and multi Availability Zone deployments are supported.

## FSx for Windows File Server
* Provides a fully managed Windows file system
* The solution is built on Windows server
* Supports the SMB protocol and Windows NTFS
* Supports Active Directory integration
* Backed by SSD for enhanced throughput and low-latency
* Data deduplication can be enabled to save on storage costs
    * Entire files, or parts of files are only stored once
    * This can save up to 80% on storage space
* Supports the use of Microsoft's Distributed File System (DFS) Namespaces
    * You can use DFS Namespaces to group file shares on multiple file systems into one common folder structure (a namespace) that you use to access the entire file dataset.

## FSx for Lustre
* A file system designed for compute-intensive workloads
* It offers the ability to process massive datasets
* Performance can run into hundred of GB per second, millions of IOPS and sub-second latency
* Integrates with S3
* Supports cloud bursting workloads over Direct Connect and VPN connections
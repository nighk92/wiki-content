[TOC]

## Overview
* Each instance of S3 storage is known as a "bucket"
* Object Storage - general-purpose object storage that works well for unstructured data sets (i.e. things that don't fit well into a database like files, images, videos)
* Objects are immutable, to update an object the old one is deleted and a new one made
* Unlimited amount of storage, you just pay per GB for what you use 
* Additional charges:
    * Transfers of data out of the region
    * Per each Put, Copy, Post, List, Get request
* Things that are free of charge:
    * Transfer of data between S3 and EC2 within the same region
    * Transfer out to Cloud Front
* Maximum file size is 5 Terabytes
* Maximum single PUT upload limit of 5 GB
    * For objects larger than 100 MB the multipart upload capability should be used, this allows for large files to be uploaded in separate chunks in parallel, improving speed and reliability of the upload
* Regional availability, storage is replicated across availability zones
    * Cross region replication can also be enabled
* Highly available with 99.99% uptime
* Highly durable - 99.999999999% (11 9's) reliable
* Consistency model
    * Distributed system
    * Strongly consistent for new objects
    * Eventually consistent for updates
* The bucket name is used in the DNS for itself, so the name must be globally unique
* Buckets are tied to an AWS region but can be accessed from anywhere over the internet

## Storage Types
* S3 Standard - cheaper access, expensive storage
* Standard Infrequent Access - 30 day minimum storage time, expensive access, cheaper storage
* One Zone Infrequent Access - Doesn't replicate data over AZs, cheapest storage type
* Intelligent Tiering - Moves data between storage types depending on its usage, a management fee is applied for this service
* Reduced Redundancy Storage - no longer recommended as Standard is now cheaper

![Storage Types Table](.images/S3_Storage_Types.png)

## Security
* By default the AWS account of the user creating the bucket/object is the "owner" of the bucket
    * This can be changed to ensure the AWS account hosting the bucket has full access, even if users from another account upload objects
* Encryption at rest is supported
    * Data can be encrypted/decrypted server side (server side encryption)
        * SSE-S3 - S3 managed key
        * SSE-KMS - KMS managed key
        * SSE-C - Customer provided key
    * Or client side (client side encryption)
        * CSE-KMS - KMS managed key
        * CSE-C - Customer provided key
* HTTPS encryption for data in transit is supported
* Access controls
    * IAM user policies
        * Re-usable policies that can be used on multiple buckets
        * Can be applied to a bucket as a whole
    * Bucket policies
        * Access controls for a bucket as a whole
        * Can be used in combination with IAM policies
        * Written in JSON
        * You can specify user, ip address and time access restrictions
        * By default no policy exists on a new bucket
    * Access control lists
        * Less fine grained than other access controls
        * Controls access for users outside of your account
        * e.g. use this to disable public access, or control public write access
        * Permissions can be set at the bucket, or object level
        * ACL's can't explicitly deny access, but they can control if someone has read or write access
* MFA access to a bucket can be enabled
* MFA required on delete can be enabled
* All access to a bucket can be audit logged
    * Server-Access Logging - https access logs for the bucket, logs stored in S3
    * Object Level Logging - more detailed logging, integrates with CloudTrail
    * It is best practice to store the logs in a separate S3 bucket to the one being logged
* Access Analyser
    * A tool that flags up when public access or access to another AWS account has been enabled for a bucket

## Versioning
* Disabled by default, but easily enabled via the UI
* Allows multiple versions of the same object to exist
* Increases the storage cost as more space will be used
* Protects from accidental deletes and overrides
* By default only the latest version of an object is shown in the UI
* If something is deleted with versioning on, S3 will simply mark the object as deleted, all versions will remain hidden in the bucket

## Internet accessible API
* Can be used to serve static web content like plain HTML pages, images, videos
* This improves performance by reducing the load on your main web servers, which should be used for dynamic content only
* The bucket must have static website hosting enabled
* The bucket must be publicly accessible for read access
* S3 doesn't support HTTPS hosting by default, but CloudFront can be used to provide this
* CORS is fully supported by S3 - A CORS policy can be configured per bucket

## Object Lock
* Enables Write Once Read Many (WORM)
* Versioning must be enabled before object locks can be applied
* Once turned on object lock can't be turned off
* Retention modes:
    * Governance Mode - prevents objects being deleted within a retention period, however certain users can override this
    * Compliance Mode - same as governance except no users can override the rules, even the root user

## Transfer Acceleration
* A bucket-level feature that enables fast, easy, and secure transfers of files over long distances
* Makes use of the globally distributed edge locations in Amazon CloudFront. 
* As the data arrives at an edge location, the data is routed to S3 over an optimized network path
* Comes with additional costs

## Events
* Event notifications can be configured for a bucket
* They can be configured on several attributes e.g. when an object is created or deleted
* These can be sent to SNS, SQS or a Lambda

## Requester Pays
* Can be configured to make the requester pay for data access costs
* Authentication must be enabled for this to work
* The requester must include the `x-amz-request-payer` http header, otherwise they will get a 403 response
* The data access bill will be sent to the requesters AWS account
* The hosting account will still pay for the data storage costs
* This functionality does not work for SOAP requests, the host account will pay for access in this case

## Access Points
* Simplifies sharing data on a large scale
* Each access point can only be applied to a single bucket, but a bucket can have multiple access points
* These allow object level operations, but not bucket level operations
* These can be restricted to a VPC or opened up to traffic from external locations
* JSON policies can be created to enable more fine grained policy control

## Batch Operations
* Lets you manage billions of objects at scale with just a few clicks in the Amazon S3 Management Console or a single API request
* Allows changes to:
    * Object metadata and properties
    * Copying objects between buckets
    * Replacing object tag sets
    * Modifying access controls
    * Restoring archived objects from S3 Glacier

## S3 and Glacier Select
* Allows the use of SQL expressions to retrieve data from within objects
* This can enable cheap and fast access to specific bits of data within an object
* e.g. It can be used to return a specific column from a set of CSV files
```sql
select * from s3object s where s.\"Country (Name)\" like '%United States%
```
* Not available in Glacier Deep Archive

## Lifecycle Policies
* Automatic rules applied to objects based on their age
* Can be used to:
    * Move data from S3 to Glacier to archive old data for cheaper long term storage
    * Delete unused or old objects
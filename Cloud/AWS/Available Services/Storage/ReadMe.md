## Overview

AWS has many different storage services to match different use-cases. Cloud storage can be cheaper, faster and is much more scalable than on premise solutions.

## Storage Types

There are 3 main storage types used within AWS:

#### Block Storage
* Data is stored in chunks know as blocks
* Blocks are stored on a volume and attached to a single instance
* Provides very low latency data access
* Comparable to DAS storage used on premises

#### File Storage
* Data is stored as separate files within a series of directories
* Data is stored within a file system
* Shared access is provided for multiple users
* Comparable to NAS storage used on premises

#### Object Storage
* Objects are stored across a flat address space
* Objects are referenced by a unique key
* Each object has associated metadata to help categorise and identify it
## Overview
* Backup and archive storage
* Low cost compared to S3
* Consists of Archives & Vaults (which are collections of archives)
* Data is encrypted by default
* An access policy can be applied to a vault
* A vault lock policy can be applied to control certain things, e.g. ensure files can't be deleted until they are a certain age. Once set a vault policy can't be removed
* Regional availability, storage is replicated across availability zones
* Highly durable - 99.999999999% (11 9's) reliable
* Can only be accessed via code e.g. a Web Service API, AWS SDK's or using S3 lifecycle rules

## Types
* Glacier
    * For general data archiving
    * Data access is not instant, data retrieval methods are:
        * Expedited - 5 mins - fast but expensive, max size of 250MB
        * Standard - 5 hours - compromise between cost and speed
        * Bulk - 12 hours - cheap but slow
* Glacier Deep Archive
    * For long term data archiving e.g. more than a year
    * Data can be retrieved within 12 hours
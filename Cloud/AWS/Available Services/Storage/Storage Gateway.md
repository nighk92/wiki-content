## Overview
A software program, deployed as a VM, that allows on premise access to cloud storage.

## Types
#### File Gateway
* Allows objects to be stored in S3
* Files sent over HTTPS
* Drives can be mapped to an S3 bucket, as if it was a local NFS file share
* The only gateway that allows an unlimited amount of storage
* An on premise cache is kept for recently accessed data

#### Volume Gateway
* Stored Volume Gateway
    * All files remain on premise for fast access
    * Files are backed up to S3 in the background
    * Full snapshots of your storage disks can also be taken, and are stored as EBS snapshots within S3
* Cached Volume Gateway
    * S3 is the primary store of data
    * An on premise cache is kept for recently accessed data
    
With Volume Gateways you must use the iSCSI protocol to connect.

#### Gateway Virtual Tape Library
* Allows backup of data to S3 and Glacier for data archiving
* Used to replace on premise backup tape drives
* Data can be moved to glacier by "archiving" tape drives
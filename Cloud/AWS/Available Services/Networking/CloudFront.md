## Overview
* CloudFront is a fast Content Delivery Network (CDN)
* It caches static web content improving performance
* It can also improve performance for dynamic content
* It caches data across regions and uses AWS edge locations to keep data close to the users
    * The caching behaviour is configurable
* Like a standard cache, data is only cached after the first access
* Once web traffic reaches CloudFront communications it all goes over the internal AWS network providing better performance than the open internet
* It supports caching from S3, EC2, ELB and any other HTTP Servers
* Supports SSL
* Integrates with AWS Shield and WAF
* Access logging can be turned on and are stored in S3
    * Logs are only stored in S3 once they reach a specific size
    * Logs can take 1 - 24 hours to reach S3
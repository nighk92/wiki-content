## Overview
* AWS Global Accelerator is a service used for fast routing of network traffic on a global scale
* It can speed up access in different regions by up to 60%
* It saves deploying your application in different regions
* It utilises Edge Locations
* It operates at the network layer, so supports TCP and UDP traffic
* Health checks are supported
* It consists of:
    * An endpoint group - registered in a different region, with it's own DNS address
    * A server registered to the endpoint group - Load balancer, EC2 instance etc.
## Overview
* Build, Deploy and Manage API's
* Create and publish API endpoints to route clients to internal services
* Supports HTTP / REST / Websockets
* Can be put in front of many AWS services, it is often used with Lambdas or a State Machine
* Has security and monitoring features
* Can be edge optimised, regional, or private to a VPC
* APIs can be versioned and staged (e.g. test/production environments)
* It can be configured with Caching and Throttling
* Provides metric dashboards and sends metrics/logs to CloudWatch
* Generates SDKs (using swagger) and can distribute API keys to developers
* Swagger API definitions can be imported (to create or update an API) and exported
* Mock data can be provided to test the API before the backend functions are in place
* The API gateway has a default rate limit of 10,000 requests per second, if this is hit a a 429 error for too many requests is returned
* The gateway will return 502 bad gateway errors if it can't connect to the underlying resource e.g. a lambda has hit its concurrency limit
## Overview
* Route 53 is the AWS Domain Name Service (DNS)
* It is a network of DNS servers managed by AWS
* Connects user requests to infrastructure running in AWS, or outside of AWS
* Includes health check monitoring, and can be set to only route traffic to healthy endpoints
* Traffic flow and DNS failover services can ensure low-latency and fault-tolerance

## Hosted Zone
A hosted zone is a container that holds information about how to route traffic. The following types of zone are supported:

* Public Hosted Zone
    * This determines how DNS traffic is routed on the internet
    * It is created when you register a domain with Route 53
    * Internet accessible domains must be registered and paid for
* Private Hosted Zone
    * Determines how DNS traffic is routed within a VPC
    * You can use any domains you wish within your VPN, there is no need to buy or register them

## Routing Policies
This determines where Route 53 will send traffic for a DNS. There are several types:

* Simple Routing Policy
    * This is the default policy
    * It routes traffic to a single resource
* Failover Routing Policy
    * Allows traffic to be routed to different resources based on their health
* Geo-Location Routing Policy
    * Routes traffic based on the geographic location of your users
    * It can also be used to restrict access to users from particular locations
* Geo-Proximity Routing Policy
    * Routes traffic based on the geographic location of **both** users and your AWS resources
* Latency Routing Policy
    * Routes to resources that have the lowest latency
    * Suitable when you have resources deployed across multiple regions
* Multivalue Answer Routing Policy
    * Allows responses from up to 8 resources at once, that are picked at random
    * This essentially performs random load balancing
* Weighted Routing Policy
    * This routes to multiple resources based of a weighting
    * This essentially performs weighted load balancing
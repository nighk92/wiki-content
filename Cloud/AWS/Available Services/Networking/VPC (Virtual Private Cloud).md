[TOC]

## Overview
* A logically isolated section of the AWS Cloud
* Provides a private ip address range that can be split into subnets as required
    * Routing rules can be configured to allow traffic between subnets
    * AWS reserves the first 4 and last 1 ip address in a subnet for internal use

## Subnets
* A logical grouping of ip addresses
* Having multiple subnets can improve security with the implementation of NACLs, and controlling public internet access
* All subnets within your VPC can communicate with each other
* Each subnet belongs to a single Availability Zone
* Available addresses are defined by a CIDR block e.g. 0.0.0.0/16
* CIDR blocks can range from /16 to /28 for a subnet
* The first 4 ip addresses and the final address are reserved by AWS
* A route table is used to control where traffic goes e.g.
    * Routing between subnets
    * Routing to the internet or a NAT gateway
    * Routing to the Virtual Private gateway for Direct Connect or Site-to-Site VPN connections
    * Priority is given to the most precise definition/CIDR when picking a route
    * Route propagation allows a virtual private gateway to automatically propagate routes to the route tables. This means that you don't need to manually enter VPN routes to your route tables.
* A public subnet is internet accessible
    * To make a subnet public its route table needs to be configured with an internet gateway
    * EC2 instances within this can be given a public ip address
* A private subnet is only accessible from within the VPC

## Security
* Network Access Control Lists (NACLs)
    * A virtual firewall for a **subnet**
    * Supports allow and deny rules
    * Rules are processed in number order, and if one matches processing is stopped at that rule
    * Always put space between rule numbers to allow for additions later on, e.g. use 10, 20, 30 instead of 1, 2, 3
    * These rules will automatically apply to any EC2 instances in the subnet, so they don't need to be configured per instance
    * It is stateless, so returning traffic must be explicitly allowed through by a rule

## External Connectivity
Your VPC is isolated from the world, with external access provided via gateways:

* Virtual Private Gateway
    * Sits at the end of a VPC allowing traffic in and out
    * This is used to route VPC tunnelling connections like Direct Connect or Site-to-Site
* Internet Gateway
    * Provides a target in VPC route tables for internet-routable traffic
    * A NAT Gateway can be put in front
        * This translates between private and public ip's
        * Allowing traffic from servers with only private ip's to be routed over the internet
        * NAT gateways are placed into a subnet, for full resiliency you should have multiple placed in different AZs

![VPC Diagram](.images/VPC_Diagram.png)

#### VPC Peering
* Fully connects two VPCs together
* The IP CIDR ranges of the VPCs can't overlap
* Routing tables must be updated to allow routing of traffic between the VPCs
* These connections can't be chained e.g. if connections were as follows VPC 1 -> VPC 2 -> VPC 3, then VPC 1 can only connect to VPC 2 resources, and not VPC 3

#### Transit Gateway
* Allows connecting of multiple VPCs together
* It provides a central hub that each VPC can be connected to in order to access all other connected VPCs
* This is a more manageable way of connecting lots of VPCs, as opposed to VPC peering which requires a individual connection to each VPC

![Transit Gateway Topology](.images/Transit_Gateway_Topology.png)

#### VPC Endpoints / Private Link
* Provides a way to access AWS resources outside of your VPC
* This can be another VPC or an AWS service that sits outside of your VPC e.g. S3, CloudFormation, SNS
* It provides an endpoint within your VPC, that securely connects to the remote resource
* Connections can only be initiated from within your VPC, the remote resource can't see into your VPC
* There are two types of endpoint:
    * Interface Endpoint
    * Gateway Endpoint - used by S3 and DynamoDB

#### Bastion Host
* A bastion host can be used as a "jump box" to gain access to private EC2 instances
* The bastion host is configured in a public subnet, and allows ssh connections
* It uses an ssh forwarding agent

#### Site-to-Site VPN
* Allows remote access to the VPN from an external location
* VPC tunnelling is used to create an encrypted connection over the internet
* This can be used to securely access private resources within your VPN
* It uses IP Security (IPsec) tunnel connections:
    * This encrypts the data at the network layer
    * Verifies data integrity with a hash
    * Provides authentication using certificates/keys
* A customer gateway is a physical device/software that you manage in your on-premises network on your side of a Site-to-Site VPN connection

#### Direct Connect
* Links a company data centre directly to a VPC using co-locations
* Unlike a Site-to-Site VPN this solution bypasses the internet, ensuring high throughput and low latency

## VPC FLow Logs
* Logs network traffic to/from/within a VPC at the network level
* Logs are stored in CloudWatch
* Can be configured to log at the following levels: 
    * The entire VPN
    * A Subnet
    * A Network Interface
    * An EC2 instance
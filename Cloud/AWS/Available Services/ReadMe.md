## Overview
As of 2020 AWS offers more than 175 different services including computing, storage, networking, database, analytics, application services, deployment, management, mobile, developer tools, and tools for the Internet of Things.

### Tags
Tags are key-value pairs that can be added to many resources. These can be used to add metadata, billing information or to filter resources within automated jobs.
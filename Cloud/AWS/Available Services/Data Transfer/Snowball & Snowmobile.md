## Overview
Large data import & export services, to transfer on premise data to S3. These are used when data transfer over the network would take more than a week. They can be used to import anything e.g. filesystems, database data, VM images

### Snowball
* A physical data transfer device
* Comes in 50TB or 80TB versions
* Multiple interfaces to allow fast transfer of data (RJ45, SFP+)
* Multiple devices can be used at once to transfer data
* All data stored is encrypted
* End to end tracking is enabled via an E Ink Shipping label, SNS notifications are used to track the device
* Once data is transferred AWS securely wipe the data from the device

### Snowmobile
* An Exabyte-scale data transfer service used to move extremely large amounts of data to AWS
* It is a 45-foot long rugged shipping container, pulled by a semi-trailer truck
* You can transfer up to 100PB per Snowmobile
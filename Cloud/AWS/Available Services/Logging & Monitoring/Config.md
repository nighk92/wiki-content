## Overview
* AWS Config tracks resources like EC2 & EBS and verifies they comply with config rules
* It captures and stores resource config changes
* Any config is region specific
* It has the following features:
    * Configuration Recorder - discovers current resources within your account
    * Configuration Item - is made for each resource, and contains
        * Metadata
        * Resource Attributes
        * Relationships to other resources
        * The Current Configuration
        * Related config change events
    * Configuration Stream - captures config changes
    * Configuration Rules - can be configured to send alerts if a resource becomes non-compliant
    * Configuration History - sent to S3
## Overview
* Cloud Watch is a monitoring and observability service
* The functionality of CloudWatch includes:
    * Dashboards
    * Metrics - events logged 5 minutes by default, but detailed monitoring can be enabled which logs every minute
    * Anomaly Detections
    * Alarms - can alert developers or run an automated script (via EventBridge) e.g. for auto-scaling
    * Events - can be run from a timer e.g. Lambdas can be run on a schedule to perform automated tasks
    * Logs
* The Unified CloudWatch Agent allows the collection of logs from Windows/Linux servers
    * Both EC2 instances and on-premise servers are supported
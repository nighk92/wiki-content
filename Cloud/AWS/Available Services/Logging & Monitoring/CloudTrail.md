## Overview
* Cloud Trail is a service which logs user activity and API usage in your AWS Account
* Logs are stored in S3 / CloudWatch
    * They are formatted in JSON
    * New log files are created every 5 minutes
    * Logs are delivered to S3 up to 15 minutes after the API event
    * Logs from multiple AWS accounts can be aggregated into a single S3 bucket in a single account
* As well as the event itself the following metadata is logged:
    * The identity of the caller
    * The timestamp
    * The source ip address
* A digest file is stored with the logs
    * This is made every hour
    * It includes a hash for your log files, to help verify the logs haven't been edited since written
    * The digest file is signed by an asymmetric key
* Via CloudWatch it can be used to monitor account activity, and alert if thresholds are breached
* SNS can be configured to alert you if new log files (for activity on new services) are created
* CloudTrail can't trigger Lambda functions unlike CloudWatch
* Log files can be encrypted using KMS
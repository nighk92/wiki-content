## Overview
Amazon GuardDuty is a threat detection service that continuously monitors your AWS accounts and workloads for malicious activity and delivers detailed security reports.

GuardDuty findings can be sent to Amazon SNS topics and CloudWatch Events.
## Overview
* The Key Management Service (KMS) provides software based encryption key management
* Integrated with many other AWS services
* Enables flexible management of encryption keys, and offers features like key rotation and CloudTrail logging
* CloudTrail audits how encryption keys are being used and by whom
* Typically, you'll use a symmetric KMS key, but you can create and use asymmetric KMS keys for encryption or signing
* KMS keys are region specific, they can't be shared across regions

## KMS Key (formerly a customer master key - CMK)
* A KMS key is a logical representation of an encryption key
* It consists of the key used to encrypt and decrypt
* And metadata such as the key ID, creation date, description, and key state
* They can be AWS Managed or Customer Managed

## Data Encryption Key (DEK)
* These are the keys used to encrypt your data
* They are encrypted by the KMS Key, then typically stored alongside the encrypted data
* To decrypt the data, this key first needs to be decrypted by the KMS Key

![Data Key Encryption](.images/Data_Key_Encryption.jpeg)

## Key Policies
These are resource based policies that control access to a KMS Key. They are written in JSON.

* Standard Key Policy
    * Applied within KMS to a particular key
    * Defines key administrators and users
    * The root account has full access to the key by default
* Key Policy with IAM
    * The "resource" section of an IAM policy can be used to restrict which users can use a KMS Key
* Key Policy with Grant
    * Allow permissions to be delegated to other AWS resources within your account
    * A Grant ID and Token are created for programmatic access

## Key Deletions
* A KMS Key can be marked to be deleted in 7 - 30 days time
* CloudWatch alarms can be setup to alert if the key is used within this timeframe
* A key can also be disabled instead of deleted
    * Use this if you are unsure about where the key might be used
    * Once a key is deleted any data encrypted by it will be irretrievable!
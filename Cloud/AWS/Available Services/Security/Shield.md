## Overview
* Provides DDOS protection
* Two tiers: Standard & Advanced
* The standard tier is automatically enabled on all the following services:
    * EC2
    * ELB
    * CloudFront
    * Route 53
    * Global Accelerator
* Note S3 is not included in the above list, to protect an S3 bucket from excessive downloads CloudFront can be put in front of it
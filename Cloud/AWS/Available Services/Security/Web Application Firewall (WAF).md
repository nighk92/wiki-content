## Overview
* The Web Application Firewall (WAF) helps protect web applications from common attacks, it works on http and https traffic
* It can be added to a Load Balancer, Cloud Front or an API Gateway
* Rules are added to Web ACLs
    * Rules can be Allowed, Blocked or just Counted
    * Rules are executed in order
    * Recommended order of rules is: Allow, then Deny, then Bad Signature
    * The Web ACL has a default action if no rules are met, this can be Allow or Deny
* Rules consist of a set of conditions, there are two rule types:
    * Regular
    * Rate Based Rule - allows the setting of a rate limit for requests
* A set of conditions are available to turn on:
    * Cross Site Scripting
    * Geo Match - lock down to a global region
    * IP Addresses - lock down to certain ip addresses
    * Size Constraints
    * SQL Injection Attacks
    * String and Regex Matching
* If the WAF blocks a request it will return a 403 (forbidden) response code
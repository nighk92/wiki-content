## Overview
* Secrets Manager allows the secure storage of sensitive information such as keys and passwords
* Many AWS services integrate with Secrets Manager e.g. it can be used to provide credentials for CloudFormation templates
* Key/password rotation is supported, you can write custom lambda functions to define the behaviour
* KMS is used to encrypt your secrets
    * KMS Key policies can be used to restrict access to specific secrets, basically stopping people from decrypting them
* Secrets can be shared between AWS accounts
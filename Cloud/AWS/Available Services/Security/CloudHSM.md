## Overview
* The AWS CloudHSM is a hardware security module, providing encryption key management that is FIPS 140-2 compliant
* It can create and store cryptographic keys
* HSMs are assigned to your VPC
* They are deployed in a cluster for resiliency
    * If one HSM fails, AWS will automatically provision a new one for you
    * The should be deployed across at least 2 availability zones
* KMS uses AWS managed CloudHSMs under the hood - and it can be configured to use your own HSMs
* EC2 instances can be configured to use the HSMs within your VPC by installing the CloudHSM client
* HSMs come with their own user controls on top of IAM:
    * Precrypto Office - temporary user for initial setup
    * Crypto Office - admin (KMS will need the login info for this user)
    * Crypto User - performs the cryptographic operations
    * Appliance User - performs cloning and synchronisation over your cluster
* They are protected against physical tampering and brute force password attacks
* Various metrics are sent to CloudWatch to monitor the health of your HSM cluster
* Audit logs are also sent to CloudWatch to allow for a full audit of the HSMs usage, this feature can't be disabled

![CloudHSM and KMS](.images/CloudHSM_and_KMS.png)
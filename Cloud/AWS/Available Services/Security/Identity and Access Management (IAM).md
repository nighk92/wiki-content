## Overview
* Identity and Access Management (IAM) provides access restrictions within an AWS account
* It is a global service, policies set cover all regions
* Users / groups / roles are assigned policies
    * Users represent an identity, the can have AWS console access (with a password) or programmatic access (with an access key via AWS STS)
    * Groups consist of several users, they make administration of multiple users easier
    * Roles are assigned to an instance or resource, to allow it access without the user providing a password or access key
        * Users can assume a role for temporary access
        * Roles can enable resources from another trusted AWS account access to certain resources within your account
            * To do this a role is setup in your account
            * Then you give users from the trusted account access to assume that role
            * Within the trusted account they can grant permission to individual users, allowing them to assume the role within your account
        * Roles can be assigned to authenticated users e.g. those coming through AWS Cognito
    * Policies are JSON documents that define the access controls
        * They are made up of statements which Allow or Deny access to a resource
        * Deny policies always take precedence over an Allow
        * Access is Denied by default if no matching allow clause is found
        * There are AWS Managed Policies (preconfigured by AWS), and Customer Managed Policies (configured by you)
* Give users different policies, to control their level of access
* Roles are temporary identities used by EC2 / Lambda / External users
* The authentication methods supported for users are:
    * Username/Password
    * MFA
    * Federated Access
* Federation can be used to assign roles to users who have authenticated via an external ldap / an active directory
    * Web Identity Federation - where users with web identities from amazon.com or another OpenID/SAML provider can be logged in using AWS SSO and assigned roles
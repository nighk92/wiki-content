## Overview
* AWS Batch allows the running of large processing tasks
* Job are submitted for processing
* Jobs are placed into a queue until they run, they can be prioritised by the user
* Multiple jobs can be run in parallel
* On demand and spot EC2 instances are supported

## Environments
Two types are available:

* Managed Environment
    * The service handles the provisioning of resources
    * It is created as an ECS cluster
* Unmanaged Environment
    * Resources are provisioned by the user
    * Requires the user to create an ECS cluster
    * Allows for greater customisation, but also involves more maintenance
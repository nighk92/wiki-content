[TOC]

## Overview
* Virtual machines that can be used for anything you like ;-)
* Easily scalable resource
* Can start one in just a few minutes
* Different Types:
    * A1 – Arm Processors
    * T2 / T3 / T3a – Burstable Performance
    * M4 / M5a / M5n – General Purpose
    * C5 / C5n / C4 - Compute Optimised
    * R5 / R5a / R5n / R4 - Memory Optimised
    * X1e / X1 - For High-performance Databases
    * z1d - Compute & Memory Optimised
    * High Memory - More memory than you could ever need! For Large In-Memory Databases
    * P3 / P2 - General GPU Computing
    * G4 / G3 - Optimised for GPU Intensive Applications
    * F1 - Customisable Hardware Acceleration
    * I3 / I3en - Low Latency SSD Storage
    * D2 - Large Amounts of HDD Storage
    * H1 - Large Amounts of HDD Storage with additional CPU and Memory

Key for instance types:

* First Letter = Instance type
* Number = Generation Version
* Other Letters:
    * a = AMD Processors instead of Intel, usually cheaper
    * n = Additional network bandwidth

## Storage
### EC2 Instance Store (Ephemeral Storage)
* Fixed Size
* Only provided with certain instance types
* Lost when instance is stopped
* Application level durability
* Used for caching / storing temporary data
* Fast, stored on the same server as the EC2 instance
* Free (included in instance cost)

### EBS (Elastic Block Store)
* Block storage
* Treat it like traditional computer storage
* Can be encrypted
* Both standard and provisioned IOPS
* Can be used in a RAID
* Snapshots can be taken as a backup or to clone the drive
    * Snapshots can be copied between regions
* Persistent even if the instance is shutdown
* Data is replicated within a single availability zone
* Multiple can be attached to a single instance
    * But a single store can only be attached to one instance at a time
* Different types available:
    * SSD
        * Better suited for smaller blocks of data e.g. for a database
        * General purpose
            * 50 IOPS per GB
            * Up to 16 TB in size
            * Up to 10,000 IOPS
            * 160 Mib/s throughput
        * Provisioned IOPS options
            * Up to 16 TB in size
            * Up to 32,000 IOPS
            * 500 Mib/s throughput
        * Good for random access
        * Better IOPS than HDD
    * HDD
        * Better suited for larger blocks of data e.g. big data, logging information
        * Throughput optimised
            * Up to 16 TB in size
            * Up to 500 IOPS
            * 500 Mib/s throughput
        * Cold HDD options
            * Up to 16 TB in size
            * Up to 250 IOPS
            * 250 Mib/s throughput
        * Good data throughput but low IOPS
        * Good for sequential access
        * Lower price

### EFS (Elastic File System)
* Block storage
* Treat it like network file storage or a shared drive
* Multiple instances can be connected at the same time
* Petabyte scale - grows/shrinks as needed
* NFS 4.0 + 4.1
* Compatible with Linux but not with Windows
* Mount points can be created within a VPC
    * Each EFS can only serve a single VPC
    * The EFS Mount helper can be installed on an EC2 instance to simplify connecting to an EFS
* Replicated across all availability zones in a region
* Different types available:
    * Standard - charges apply to the amount of data stored, data retrieval is free
        * Two performance options:
            * General Purpose - allows up to 7,000 IOPS
            * Max IO - higher throughput but slightly higher latency, designed to have 1,000's of servers connected at the same time
        * Two throughput options:
            * Bursting Throughput - credits are used to allow bursts of throughput until the credits run out, usage can be monitored by CloudWatch metrics
            * Provisioned Throughput - additional charge for consistent throughput
    * Infrequent Access - cheaper data storage, but the are additional charges for read/write access
* Lifecycle rules can be used to move files from Standard to Infrequent Access automatically
* Security for EFS is managed using IAM roles
* Encryption at rest and in-transit is supported using a KMS key

## Security
* Security Groups
    * A virtual firewall for an **EC2 instance**
    * Each instance must be assigned security groups for them to be utilised
    * Each instance can have up to 5 security groups
    * Each group is configured with rules to allow specified inbound and outbound traffic
    * Only allow rules are supported, if traffic doesn't match an allow rule then it is dropped
    * All rules are checked before allowing traffic through
    * It is stateful, so returning traffic is automatically allowed through regardless of rules
* Key Pairs
    * All EC2 instances can have a key pair in order to be accessed
    * For Linux this is an ssh key used to logon
    * For windows the key is used to encrypt/access the username and password
    * If an instance doesn't have a key pair it can't be accessed over ssh, it could instead be administered via Systens Manager

## Auto Scaling
* Spins up / tears down instances based off a condition, the conditions can be based on CPU Usage, network traffic, or number of load balancer requests
* Supports instance health checks
* Works over availability zones
* Cloud Watch -> Alarm -> Policy specified action -> ELB balances over the group of instances
* Instance template is provided by the Launch Configuration
* An auto scaling group can only have 1 Launch Configuration
* A Launch Configuration specifies:
    * The base AMI ID
    * Instance Type
    * SSH Key Pair
    * Instance Security Groups
    * Block Device Mapping (for storage)
    * Additional user data scripts, these are run at startup to configure an instance
* A Launch Template supports all of the above, but also adds additional features like versioning
    * This should be the preference over a Launch Configuration
    * It can support both spot and on-demand instances, and multiple instance types
    * Versioning can be used to create a base template from which new configurations can be made
* There are different types of auto scaling groups available, including:
    * Simple Scaling - Scales up/down based off a CloudWatch alarm
    * Step Scaling - Additionally allows you to specify how much to scale as a percentage based off of the alarm metric
    * Target Tracking - Configure a CloudWatch scaling metric, the alarms are then made and managed for you, and your services are scaled in line with the metric. e.g. You can specify CPU usage should remain at 40%

## Placement Groups
* Influences the provisioning of EC2 instances
* The following types are available:
    * Cluster
        * Instances are positioned closely to each other in the same availability zone
        * Allows for low-latency network communication between instances
    * Partition
        * Spread instances over logical partitions
        * e.g. over availability zones
        * However there may still be multiple instances on the same hardware rack
    * Spread
        * All instances are spread over availability zones
        * Instances are all provisioned on separate hardware racks
        * Provides ultimate resiliency at the cost of higher latency network connections between instances

## Application Machine Image (AMI)
* A VM image that provides a template for an EC2 instance
* AWS provides several default images for different operating systems
* There is a marketplace where additional images can be purchased from various companies
* Custom AMI's can be made based off any EC2 instance

## Tenancy
* Enables control of the underlying physical host an EC2 instance will reside on
* The different options are:
    * Shared Tenancy
        * The EC2 instance is launched on any available host
        * The same host may be used by multiple customers
        * Various security mechanisms provided by AWS prevent one EC2 instance accessing another
        * This is the default and cheapest option
    * Dedicated Instances
        * The EC2 instance is hosted on hardware no other customer can access
        * Multiple resources within your account use the same hardware
    * Dedicated Hosts
        * Offers greater access to the underlying hardware
        * Allows licenses from vendors such as Microsoft and Oracle to be used, saving money if you already have licences
        * Multiple EC2 instances can run on a single host

## Elastic Load Balancer
* Load balancing is essential to allow scaling and resiliency for applications
* AWS load balancers are highly available and resilient
    * The load balancer can have multiple nodes in different availability zones
    * Each node can only distribute traffic to instances within it's availability zone
    * Cross Zone load balancing can be enabled to spread traffic evenly between availability zones (this is always enabled on an Application Load Balancer)
* Listeners are configured on a load balancer to define routing rules
* All traffic is routed to a Target Group, which is a group of resources e.g. EC2 Instances
    * Health checks are used to monitor instances within a target group
    * Target groups can include EC2 instances, lambdas or external ip addresses
* There are 3 types of load balancer:
    * Application Load Balancer (ALB)
        * Supports http and https
        * Operates at the request level (layer 7 - the application layer)
        * Supports advanced routing, TLS termination, authentication
    * Network Load Balancer (NLB)
        * Offers ultra-high performance, but with a reduced number of features
        * Supports TCP, TLS and UDP
        * Operates at the connection level to route traffic (layer 4 - transport layer)
    * Classic Load Balancer
        * Largely deprecated
        * Operates at both the network and request level
        * It supports sticky sessions which the ALB doesn't

## Cost Optimisation
* Spot Instances
    * Allows bidding for unused EC2 compute resources
    * Typical cost savings are 80 - 90%
    * Prices fluctuate based on supply and demand, but are lower than the standard on-demand prices
    * The instances can be stopped at short notice (2 minutes)
        * However you can request spot instances run continuously for up to 6 hours at a flat rate that is 50% less than the on-demand price
    * Useful for background data processing, where it doesn't matter if it is interrupted
    * There are 3 different configurations available:
        * Load balancing workloads - launches instances of the same size in any AZ
        * Flexible workloads - launches instances of any size in any AZ
        * Big data workloads - launches instances of any size in a single AZ
    * The spot instance advisor helps determine the pools with the least chance of interruption and highest cost savings
* Savings Plan
    * Offer significant cost savings (up to 72%) over a 1 or 3 year period
    * Can be used on EC2, Lambda and Fargate
    * An hourly commitment must be made when purchasing a plan
    * There are 3 types:
        * Compute Savings Plans - very flexible, covers usage on EC2, Lambda and Fargate
        * EC2 Instance Savings Plans - provide the highest cost savings, but tie you to a specific family of EC2 instances
        * SageMaker Savings Plans - specific to machine learning
    * There are All Upfront, Partial Upfront, or No Upfront payment options
* Reserved Instances
    * Now deprecated in favour of savings plans which are more flexible
    * Allows purchase and specific type of EC2 instance upfront for a 1 or 3 year period, giving up to a 75% cost saving
* Scheduled Instances
    * This service is no longer offered
    * Allowed reservation of specific instances at specific times for batch jobs
* On Demand Capacity Reservations
    * Allow specific EC2 instances to be reserved upfront to ensure capacity is available
    * They don't offer any cost savings but they can be used with the other optimisations above

## Instance Checks
There are 2 default instance checks:

* System Status Check
    * Checks for issues with the underlying hardware
    * The best way to resolve a failure is to stop the instance and restart it, so it gets moved to a different host
* Instance Status Check
    * Checks for issues with the EC2 VM itself
    * Failures can be caused by:
        * Incorrect network configuration
        * Corrupt file systems
        * Exhausted memory
        * Incompatible kernel

## Elastic IP Addresses (EIPs)
* This provides a persistent ip address for an EC2 instance, as opposed to the default ip addresses given which are dynamic
* It can be a private or public ip address
* Assigning an elastic ip incurs a small cost
* The elastic ip is registered to your account, so it can be moved between EC2 instances as required

## Elastic Network Interfaces (ENI)
* Virtual network cards that can be assigned to any EC2 instance
* Can be used to give EC2 instances an additional network connection
* Can enable an EC2 instance to be connected to multiple subnets at the same time

## Enhanced Networking
* Offers greatly enhanced network speeds
* Only supported by specific EC2 instance types
* Often enabled by default on compatible instance types
* Can be enabled manually by adding an Elastic Network Adapter (ENA)

## EC2 Fleet
* A configuration that can launch multiple EC2 instances in a single API call
* It can provision both on-demand and spot instances
* If an instance goes down e.g. a spot instance is removed, it can automatically provision a new one

## Instance Metadata
It is possible to obtain metadata about an EC2 instance from within the instance itself. This is provided by the IMDSv2 (Instance Meta-Data Service v2). This service uses a reserved link-local ip address of `169.254.169.254`.

First you need to obtain an access token:
```bash
TOKEN=`curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600"`
```

Then you can query the metadata endpoint like this:
```bash
curl -H "X-aws-ec2-metadata-token: $TOKEN" -v http://169.254.169.254/latest/meta-data/
```

To query for specific attributes you can extend the url path e.g. the following example gets the AMI ID of the instance
```bash
curl -H "X-aws-ec2-metadata-token: $TOKEN" -v http://169.254.169.254/latest/meta-data/ami-id
```
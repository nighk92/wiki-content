## Overview
* EKS is a managed Kubernetes service
* It takes care of running the Kubernetes management software (control plane)
* Then leaves provisioning of the worker nodes to the user
* AMIs are provided for the worker nodes
* CloudFormation templates are provided to provision a full EKS cluster and worker nodes
* IAM roles are used to secure the cluster
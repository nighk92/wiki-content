## Overview
* Amazon Elastic Map Reduce (EMR) is a platform for rapidly processing big data
* It is based on Apache Hadoop
* EMR integrates with several other AWS services like S3, 
* There are 3 node types within an EMR cluster:
    * Master Node - there is only ever one of these, it manages the cluster jobs and resources
    * Core Node - manages data storage in the hadoop distributed file system (HDFS), and the parallel processing of tasks
    * Task Node - (optional) they can be scaled to perform parallel processing
* An EC2 Instance Fleet configuration can be used to provision and control the number of nodes you need

![EMR Cluster](.images/EMR_Cluster.PNG)
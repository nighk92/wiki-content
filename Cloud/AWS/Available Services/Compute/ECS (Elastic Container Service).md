## Overview
ECS is a service to run docker containers.

## Launch Options
* Fargate Launch - a managed service to run and scale your containers, it is serverless meaing all underlying compute resources are managed for you
* EC2 Launch - launch your own instances to run your containers, offers more granularity and control

## ECS Cluster
* A cluster of EC2 instances to run your docker containers
* Enables a dynamically scalable solution
* Can span multiple availability zones within a single region
* All EC2 instances must be running the ECS Container agent in order to register itself within a cluster
## Overview
* Lambda runs code without the need to provision servers
* All servers are managed for you
* Your account is charged per invocation of a lambda, and cost varies depending on run time, cpu (measured per 100ms) and memory usage
* Up to 10GB of RAM is supported per lambda
* A timeout limit must be set, the lambda will be terminated once this limit is reached
* Concurrency limits can be set, to control the number of lambda functions that can run in parallel
* Code can be triggered via a Web API, URLs can made to look like any traditional API
* Several programming languages are supported including Java, Javascript and Python
    * AWS provides a rich set of SDKs for each language to easily interact with other AWS services from your code
    * Code deployments can be versioned
* Lambda functions can use many Downstream AWS resources including: Databases, Other Lambdas
* Role Execution Policies - determine what resources the lambda can access when running
* The Function Policy - defines which AWS resources are allowed to invoke your function
* The lambda function is called with a JSON object as its single argument
* Environment variables can be provided to configure the function, these can be encrypted upon being stored

## Event Sources
* Lambdas are event driven
* An event source provides the events to invoke your lambda function
* Events can be:
    * Push based - these invoke your lambda following an event e.g. SNS, S3, CloudWatch
    * Poll based - the lambda must check the source for new events e.g. Kinesis, SQS, DynamoDB
* A lambda can be invoked as a synchronous or an asynchronous function
    * Push based events can by sync or async
    * Poll based events are always synchronous

## Monitoring
* Output metrics & logs are sent to CloudWatch
* Output from console logs are included in the CloudWatch output
* The following metrics are available:
    * Number of invocations
    * Errors
    * Dead letter errors
    * Execution duration
    * Throttling events
    * Iterator Age
    * Sum of Concurrent Executions
    * Sum of Unreserved Concurrent Executions (for functions without a concurrency limit set)

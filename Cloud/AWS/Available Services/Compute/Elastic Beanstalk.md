## Overview
* Elastic Beanstalk is a service for deploying and scaling web applications
* Platforms supported include Java, Node.js, Python, Docker and Tomcat
* Just upload your code, then servers are provisioned automatically
* Databases, Load balancing, auto-scaling and health checks are all managed automatically
* There is no additional charge, you only pay for the AWS resources used
* Once deployed resources like EC2 instances can be maintained and monitored using the standard AWS services
* A Configuration Template is provided to deploy the application
    * Applications are versioned, with deployable code uploaded to Beanstalk and typically stored in S3
* The Platform consists of information about the Operating System, programming language and components of elastic beanstalk itself

## Running Environments
Two running environments are available:

* Web Server Environment:
    * For applications that serve http requests
    * The stack consist of: Route 53 -> ELB -> Auto Scaling Group -> EC2 Instances -> Security Groups
    * Each EC2 instance has a Host Manager installed, this:
        * Aids in the deployment of the application
        * Collates metrics and events
        * Monitors the application logs and the application server
        * Patches instance components
        * Manages logs files and publishes them to S3
* Worker Environment:
    * Used for backend processing tasks
    * Integrates with SQS
    * The stack consist of: SQS -> Auto Scaling Group -> IAM Service Role (to access SQS) -> EC2 Instances
    * A Daemon is installed on each EC2 Instance
        * This connects to SQS in order to pull tasks/events from the queue

The environments can be deployed in two modes:

* A load-balancing, autoscaling environment
* A single-instance environment e.g. for lower costs when in development

## Deployment Options
Various deployment/update methods are provided:

* All At Once (Default) - simple but causes outages during updates
* Rolling - updates are done in batches, no chance of outages during updates
* Rolling with additional batch - the same as rolling, but this adds another batch of instances during an update to ensure there is no drop in resources as instances are updated
* Immutable - an entirely new set of resources are provisioned in the background, then the old environment is replaced with the fully working new one

## Health Checks
* Basic Health Reporting
    * All resources are monitored
    * CloudWatch metrics are sent in 5 minute intervals
    * There are 4 statuses per environment: 
        * Green - Healthy
        * Yellow - Occasional requests are failing
        * Red - Requests are consistently failing
        * Grey - Updating
* Enhanced Health Reporting and Monitoring
    * ECS instances have a health check agent installed for more detailed metrics
    * CloudWatch metrics are sent every 10 seconds
    * There are 7 statuses per environment: 
        * Green - Healthy
        * Yellow - Occasional requests are failing
        * Pink - Environment is degraded, with a high number of failing requests
        * Red - Requests are consistently failing
        * Black - Updating
        * Grey - Not enough data to determine health
        * White - Monitoring has been suspended
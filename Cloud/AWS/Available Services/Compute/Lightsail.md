## Overview
* Lightsail is a simple and small scale server resource
* It can be used to host simple websites or services
* It is essentially a very basic version of EC2
* It has in built support for hosting various websites like WordPress
* It can be linked with other AWS services like S3 or Databases
* Every server provisioned must have a public ip address


## Overview
* ECR is a fully managed Docker container registry
* AWS authorisation tokens are used to authenticate to the registry
* An ECR policy can be set to control user access to a repository

## List of Other Services
* **ACM** - AWS Certificate Manager, create and manage SSL certificates
* **AppSync** - Create a GraphQL API
* **Athena** - Serverless SQL query system for S3 data, integrates with AWS Glue, can be used on any structured S3 data like CSV or JSON, for example it can be used to query CloudTrail logs
* **Auto Scaling** - Separate to EC2 auto scaling, this quickly scales resources within ECS, DynamoDB and Amazon Aurora
* **Budgets** - Set custom budgets and receive alerts
* **Cloud9** - IDE
* **CodeBuild** - CI/CD
* **CodeCommit** - Private git repository, it can link with CodeBuild and CodeDeploy
* **CodeDeploy** - Automates software deployments to a variety of compute services such as EC2, Fargate, Lambda, and on-premises servers
* **Cognito** - A user access control service
* **Cost Explorer** - Visualise costs
* **EventBridge** - Serverless event bus
* **Glue** - Extracts, transforms & transfers data, used for pre-processing before running queries or storage
* **Inspector** - Checks EC2 instances for vulnerabilities
* **Lake Formation** - Setup a data lake with data from multiple sources for analytical purposes, uses S3 for the underlying storage
* **MSK** - Apache Kafka Service
* **Pinpoint** - Manager user engagement, sending automated emails, SMS's, or push notifications
* **Polly** - Turns text into speech using deep learning
* **SSO** - Single Sign On service with support for OpenID and SAML provides
* **Step Functions** - A low-code visual workflow service, can be used to create a State Machine (a sequence of Lambdas)
* **STS** - Security Token Service, enables you to request temporary credentials for IAM users or federated users
* **SWF** - Simple Workflow Service, helps developers build, run, and scale background jobs that have parallel or sequential steps
* **TCO** - Total Cost of Ownership/AWS Pricing Calculator is a web based service that you can use to create cost estimates to suit your AWS use cases
* **Trusted Advisor** - Checks account for best practices on security, reliability, performance, cost & service limits
* **WorkMail** - A managed business email and calendar service with support for existing desktop and mobile email clients
## Overview

Openshift is a suite of containerisation products developed by RedHat. The flagship product is the Openshift Container Platform, which is a PaaS product that is used for hosting/running docker containers, it sits on top of Kubernetes.
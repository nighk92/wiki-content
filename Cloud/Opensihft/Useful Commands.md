[TOC]

### Internal Docker Registry
Find the internal docker registry address
```bash
oc registry info
```

Vies available image streams within the internal registry
```bash
oc get is -n openshift
```

Search docker images available within the internal registry
```bash
oc get imagetags -n openshift | grep <image-name>
```
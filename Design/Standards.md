## General Standards
The general programming standards vary depending on what language is being used, however these general standards should always be adhered to:

* Use Indentations (tabs or 4 spaces) for different sections of code
* Add comments at the beginning of each class/method
* File Names and Classes begin with capital letters and further words being with capitals E.g. MyClass
* Folders, Packages, Variables and Methods begin with lower case letters and further words begin with capitals (camel case) E.g. myVariable
* Constants are written in all capitals, with words separated by underscores E.g. MY_CONSTANT
* Keep data access and UI code separate and as independent from each other as possible
* Split the code up into logical modules, e.g. data access, server side logic, UI code, integration testing
* Avoid dependencies between code modules where possible
* Use interfaces to define the APIs for any dependencies, so each module remains unaware of specific implementation details

## When to use Methods
* There is a section of code that does something useful
* A section of code is repeated, i.e. you have to copy and paste code
* Another method gets hard to navigate (i.e. over 200 lines long)

## Object-oriented Standards
* Divide a program into distinct logical parts (classes)
* Encapsulate data inside classes, and only access variables with getter/setter methods
* This means validation can be easily added without breaking the application
* It encourages low coupling and high cohesion
* Create easy to reuse classes
* Inherit code from related classes where possible, to avoid repeating functionality
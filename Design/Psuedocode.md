## Overview
Pseudocode is high level design code, which can be implemented in any object-oriented programming language. It can be used to define complex sections of code. e.g.

```
BEGIN Main
     FOR 4 times
           Read in a number
           IF number is 10
                Print out “Number 10 is the best!”
           END IF
     END FOR
END Main
```
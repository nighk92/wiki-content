## Overview
The design of a UI is critical to enhance the usability of an application, and the users view of the application.

## Design Advice
* Consider using a pre-defined design standard like Material-UI
* Keep things simple
* Don’t use too many colours on one screen, and avoid using strongly contrasting colours
* Don’t mix too many fonts
* Think carefully about how the user might navigate the program, ensure they can do this without navigating through lots of menus/pages and without an excessive number of clicks
* Always provide a simple route back to the main menu
* Just provide one way forward or back, so the user isn’t confused
* Think carefully about who might use the program. Will it be an expert or novice user? Then make the appearance and language appropriate to them.
* Think carefully about making functionality accessible for users with disabilities:
* Ensure the tabbing order is logical
* Colours contrast each other enough, e.g. no pale grey text on white backgrounds
* Add aria-labels for all input fields, buttons, icons and pictures
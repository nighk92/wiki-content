[TOC]

## Overview
The DAO/DTO design pattern is a common pattern used to abstract the data storage layer from the rest of the application. The aim is to make it theoretically possible to change the data storage layer of the application and not have to change any code apart from the DAO layer.

## DAO
The Data Access Object contains all the code needed to write/read data from the storage layer e.g. a database. The functions for the DAO should be defined in an interface, so it then becomes possible to write different implementations of the interface for different storage mechanisms, ultimately making the underlying storage layer invisible to the rest of an application.

See the below for an example DAO interface and implementation:

![DAO Design](.images/DAO_Design.png)

## DTO
The Data Transfer Object is an object that stores data, and transfers it between layers of the application. It should be a POJO that reflects the data that needs to be stored. At this point that data validation can be carried out before it is sent to the DAO.

## DAO & DTO Pattern
The DAO/DTO designs can be used together so data can flow between layers of the application. The DAO deals with the low level storage layer and the DTO puts the data into a format that the rest of the application can use.

See below for an example of using the DAO/DTO design pattern:

![DTO DAO Design](.images/DTO_DAO_Design.jpg)
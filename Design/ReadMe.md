## Overview

The initial design of an application/program has a big effect on the structure of the code, the efficiency of the application, the flexibility of the code, and the support costs for the application. There are many different design concepts and programming styles that should be adhered to in order to make better applications and more reusable code.

Pseudocode, UML, Design Diagrams and Use Case Diagrams can be used to refine the design of an application or specific section of code.
[TOC]

## Overview
UML can be used design the high level structure of an Object-Oriented application.

## Syntax
### Classes
Used to define the structure of a class:

![UML Class Syntax](.images/UML_Class_Syntax.png)

* **-** A minus sign indicates a private attribute/method
* **+** A plus sign indicates public
* **#** A hash sign indicates protected
* **~** A tilde sign indicates package-protected
* **Underline** indicates static

### Relationships
Are used to link the classes together:

![UML Relationship Syntax](.images/UML_Relationship_Syntax.png)

### Multiplicity
Specifies how many instances of each class there might be in a relationship. It can be used for Associations, Aggregations, and Compositions. 

The values can be:

* **0** - No Instances
* **0..1** - None or one instance
* **1** - Exactly one instance
* **0..\*** - Zero or more instances
* **1..\*** - One or more instances

## Example Diagram
Class diagram for an ordering processing system:

![UML Class Diagram Example](.images/UML_Class_Diagram_Example.png)
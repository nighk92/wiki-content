[TOC]

### Stop text area resizing
```css
textarea
{
    resize: none;
}
```

### Text Overflow Ellipsis
Replaces overflowing text with ...
```css
.text-overflow-ellipsis
{
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}
```

### Word Break All
Breaks words up if they overflow, even if there are no spaces.
```css
.word-break-all
{
    overflow-wrap: break-word;
    word-wrap: break-word;
    -ms-word-break: break-all;
    word-break: break-word;
 
    /* Adds a hyphen where the word breaks, if supported by the browser */
    -ms-hyphens: auto;
    -moz-hyphens: auto;
    -webkit-hyphens: auto;
    hyphens: auto;
}
```

### Display \n as a newline in a <div> or <span>
Adding the following allows `<div>` or `<span>` tags to display `\n` as a new line just like a `<pre>` tag would:
```css
.pre-wrap
{
    white-space: pre-wrap;
}
```

### Spinner Animation
Makes a spinner animation, using css3 animation. Can be used on `<div>` elements. 
```css
.spinner
{
    height: 30px;
    width: 30px;
    display: inline-block;
    -webkit-animation: rotation .6s infinite linear;
    -moz-animation: rotation .6s infinite linear;
    -o-animation: rotation .6s infinite linear;
    animation: rotation .6s infinite linear;
    border-left: 6px solid rgba(100, 100, 100, .15);
    border-right: 6px solid rgba(100, 100, 100, .15);
    border-bottom: 6px solid rgba(100, 100, 100, .15);
    border-top: 6px solid rgba(100, 100, 100, .8);
    border-radius: 100%;
}

@-webkit-keyframes rotation 
{
    from 
    {
        -webkit-transform: rotate(0deg);
    }
    to 
    {
        -webkit-transform: rotate(359deg);
    }
}

@-moz-keyframes rotation
{
    from
    {
        -moz-transform: rotate(0deg);
    }
    to 
    {
        -moz-transform: rotate(359deg);
    }
}

@-o-keyframes rotation 
{
    from 
    {
        -o-transform: rotate(0deg);
    }
    to 
    {
        -o-transform: rotate(359deg);
    }
}

@keyframes rotation 
{
    from 
    {
        transform: rotate(0deg);
    }
    to 
    {
        transform: rotate(359deg);
    }
}
```

### Shake Animation
Makes any html element shake/wobble when hovering over it.
```css
.shake:hover
{
    -webkit-animation-name: shake;
    -webkit-animation-duration: 1.3s;
    -webkit-transform-origin:50% 50%;
    -webkit-animation-iteration-count: infinite;
    -webkit-animation-timing-function: linear;

    animation-name: shake;
    animation-duration: 1.3s;
    transform-origin:50% 50%;
    animation-iteration-count: infinite;
    animation-timing-function: linear;

    -moz-animation-name: shake;
    -moz-animation-duration: 1.3s;
    -moz-transform-origin:50% 50%;
    -moz-animation-iteration-count: infinite;
    -moz-animation-timing-function: linear;
}

@-webkit-keyframes shake 
{
    0% { -webkit-transform: translate(1px, 1px) rotate(0deg); }
    10% { -webkit-transform: translate(-1px, -1px) rotate(-1deg); }
    20% { -webkit-transform: translate(-1px, 0px) rotate(1deg); }
    30% { -webkit-transform: translate(0px, 1px) rotate(0deg); }
    40% { -webkit-transform: translate(1px, -1px) rotate(1deg); }
    50% { -webkit-transform: translate(-1px, 0px) rotate(-1deg); }
    60% { -webkit-transform: translate(-1px, 1px) rotate(0deg); }
    70% { -webkit-transform: translate(1px, 1px) rotate(-1deg); }
    80% { -webkit-transform: translate(-1px, -1px) rotate(1deg); }
    90% { -webkit-transform: translate(1px, 1px) rotate(0deg); }
    100% { -webkit-transform: translate(1px, -1px) rotate(-1deg); }
}

@keyframes shake
{
    0% { transform: translate(1px, 1px) rotate(0deg); }
    10% { transform: translate(-1px, -1px) rotate(-1deg); }
    20% { transform: translate(-1px, 0px) rotate(1deg); }
    30% { transform: translate(0px, 1px) rotate(0deg); }
    40% { transform: translate(1px, -1px) rotate(1deg); }
    50% { transform: translate(-1px, 0px) rotate(-1deg); }
    60% { transform: translate(-1px, 1px) rotate(0deg); }
    70% { transform: translate(1px, 1px) rotate(-1deg); }
    80% { transform: translate(-1px, -1px) rotate(1deg); }
    90% { transform: translate(1px, 1px) rotate(0deg); }
    100% { transform: translate(1px, -1px) rotate(-1deg); }
}

@-moz-keyframes shake
{
    0% { -moz-transform: translate(1px, 1px) rotate(0deg); }
    10% { -moz-transform: translate(-1px, -1px) rotate(-1deg); }
    20% { -moz-transform: translate(-1px, 0px) rotate(1deg); }
    30% { -moz-transform: translate(0px, 1px) rotate(0deg); }
    40% { -moz-transform: translate(1px, -1px) rotate(1deg); }
    50% { -moz-transform: translate(-1px, 0px) rotate(-1deg); }
    60% { -moz-transform: translate(-1px, 1px) rotate(0deg); }
    70% { -moz-transform: translate(1px, 1px) rotate(-1deg); }
    80% { -moz-transform: translate(-1px, -1px) rotate(1deg); }
    90% { -moz-transform: translate(1px, 1px) rotate(0deg); }
    100% { -moz-transform: translate(1px, -1px) rotate(-1deg); }
}
```
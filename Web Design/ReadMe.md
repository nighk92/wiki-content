## Overview
Websites are coded using HTML to provide the structure, and CSS to provide the styling. Javascript is frequently used to add additional functionality.
HTML can be made more dynamic, by basing the structure and page content on data. There are many technologies which can be used to make dynamic websites such as JSP's, AngularJS, and EJS.
## Wiki

Welcome to Paul's programming wiki!

### Stats (as of 23/01/2021)
* Number of .md files = **208**
    * Calculated by running `find . -name "*.md" | wc -l`
* Word Count = **44,187**
    * Calculated by running ```find . -name "*.md" -exec cat {} + | tr -d '*#|[]`{}-/()=+$%&;"\' | wc -w```
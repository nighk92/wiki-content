## Common File Types

**.class** – The compiled java byte code from a .java file

**.css** - Styling information for web applications

**.doc** / **.docx** - Microsoft Word file format, .docx is the current format, .doc is a legacy format

**.ear** – Enterprise Archive, like a .jar file but for Java EE applications

**.exe** – Windows executable

**.html** - Hypertext Markup Language - the standard format for web pages

**.jar** - Java Archive - Is a combination of compiled class files that can be used as a library or run as a standalone Java program

**.java** - Java source code

**.jpg** / **.jpeg** - A common image format using lossy compression

**.js** - Javascript source code

**.json** - Javascript Object Notation - A human and machine readable data format

**.jsp** - Java Server Pages - A framework for creating dynamic web pages using Java 

**.jsx** - A Javascript extension language used within React to create HTML

**.mp3** - A common audio format using lossy compression

**.mp4** - A common video format using lossy compression

**.pdf** - Portable Document Format, commonly used to distribute read-only documents

**.pom** - The pom.xml is a standard file used by Maven, it lists the dependencies for an application

**.png** - A common image format that uses lossless compression

**.ppt** / **.pptx** - Microsoft Powerpoint file format, .pptx is the current format, .ppt is a legacy format

**.pub** - Microsoft Publisher file format

**.py** - Python source code

**.txt** - Plain text

**.war** – Web Application Archive, used to distribute a java web application including .jar, .html and other resource files

**.xls** / **.xlsx** - Microsoft Excel file format, .xlsx is the current format, .xls is a legacy format

**.xml** - Extensible Markup Language - A human and machine readable data format
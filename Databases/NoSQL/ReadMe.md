[TOC]

## Overview
A NoSQL database is a database that doesn't use SQL, and the are generally non-relational. The advantages of NoSQL databases are a simpler design and increased flexibility. The most popular NoSQL database is MongoDB.

There are several different types of NoSQL database, each storing it's data in a different way. Types include: Column, Document, Key-Value, Graph, and Multi-Model.

## Database Comparison
NoSQL databases vary greatly. They store their data in different ways and use different query languages. Each type of database has it's own use case, but the table below gives a high level comparison of the most common NoSQL databases:

| | Cassandra (Column) | MongoDB (Document) | Oracle NoSQL (Key-value) | Neo4J (Graph) | AraganoDB (Multi-Model) |
| --- | --- | ---| --- | --- | --- | --- |
| **Overall** | \* \* \* | \* \* \* \* | \* \* \* | \* \* \* | \* \* \* \* |
| **Speed** | \* \* \* \* | \* \* \* \* | \* \* \* \* | \* \* \* \* | \* \* \* |
| **Features** | \* \* \* | \* \* \* \* | \* \* | \* \* \* \* | \* \* \* \* \* |
| **Flexibility** | \* \* | \* \* \* \* | \* \* | \* \* | \* \* \* \* \* |
| **Reliability** | \* \* \* | \* \* \* \* | \* \* \* \* | \* \* \* | \* \* \* |
| **Support** | \* \* | \* \* \* \* | \* \* \* \* \* | \* \* \* | \* \* |
| **Cost** | \* \* \* \* \* | \* \* \* \* \* | \* \* | \* \* \* \* \* | \* \* \* \* \* |

### Cassandra (Column)
The Column storage type is the most similar to SQL's out of all the NoSQL storage types. It does have slight differences however, as the data model is row-orientated instead of column-orientated. This means that each row can have different columns, and don’t have to contain all the columns that other rows have. Data is stored in "Column Families" which are similar to tables in an SQL database. However columns can be freely added to a family at any time, and individual columns can also contain another Column Family, so you can end up with Column Families nested within each other.

Cassandra is a Column database originally developed by Facebook and now by Apache. It can be used to store large amounts of data, and is particularly fast at writing data to the database. Being so similar to SQL the learning curve for Cassandra is less steep than other NoSQL databases, when converting from SQL. However it does lack scalability when data models are complex and data is spread over multiple tables. It has excellent community support, but enterprise support is only available through third party sources. It should be used to store large amounts of data in a single table, and where high fault tolerance is needed.

**Pros**

* Can efficiently store large amounts of data
* Very fast for writing data to the database (faster than SQL databases)
* Less of a learning curve compared to other NoSQL databases

**Cons**

* Lacks scalability for complex data models spread over multiple tables

### MogoDB (Document)
The Document storage type stores documents together in a collection. The information inside a document is generally encoded as JSON or XML.

MongoDB is one of the most popular NoSQL databases. As such it has a wide level of community and enterprise support. It is very flexible with objects being stored as JSON, which can be easily converted and used programmatically without the need for complex configuration. It is fast, scales well. Data consistency when running over several servers can be maintained by using the “read concern” option, which ensures that data held by the “majority” of the server is returned instead of the local copy which may be out of date. It does have a limit on the size of a single document which is 16mb, however this can be overcome by using the GridFS API. There is good support for many different programming languages, and it naturally fits in with any Javascript program. MongoDB should be used if you need a fast database that is very flexible and can store almost anything successfully.

**Pros**

* Fast and scales well (faster for writing/deleting data than an SQL database)
* Flexible, so it can store almost any data
* Good API support for many programming languages
* Powerful Javascript query language

**Cons**

* Lack of relationships and consistency between data

### Oracle NoSQL (Key-Value)
In a Key-value database data is stored as simple Key-value pairs, using a hash table. These pairs are linked together by a “Master” key (e.g. a unique id) which links all the “Child” keys together.

The Oracle NoSQL database stores its data as key-value pairs, but it also has support for SQL style tables and JSON documents. There are both commercial and open source versions available. Being Oracle it has excellent enterprise level support available. It can store large amounts of data and process it quickly, it is best at storing simple data models without relationships. It can cope with a large number of read/write requests. However it doesn’t support transactions like a standard Oracle SQL database. The ideal use case for Oracle NoSQL is an application with a simple data model and a high volume of traffic such as network monitoring.

**Pros**

* Fast and simple storage model
* Can cope with large number of read/write requests

**Cons**

* Doesn't support transactions
* Lack of relationships and consistency between data

### Neo4J (Graph)
Graph databases store data as key-value pairs (nodes) which are all linked together in a graph like structure. For example a person may be a node, they may be related to an address which is another node, there may also be another person which is also related to the same address. These relationships between nodes help maintain a certain amount of referential integrity.

Neo4j is a Graph database written in Java, its design works around many of the limitations that hold other NoSQL databases back. Graph databases offer excellent performance when the data model is complex with a lot of links between different objects, in this sense graph databases outdo most other SQL and NoSQL databases while still maintaining a flexible data model. Both community and enterprise support is available for Neo4j. The main use case is for heavily linked data, where data would be spread across several tables in an SQL database. However it will be slower than other databases for data models just requiring a single table in SQL.

**Pros**

* Offers great performance when data is heavily linked with a lot of relationships between fields
* Ideal for storing complex data models

**Cons**

* Slow for storing simple data with very few relationships

### AraganoDB (Multi-Model)
Multi model databases support several different storage types. For example AragnoDB supports the storage of data in Document, Key-Value and Graph formats.

**Pros**

* Very flexible
* Can save having to use multiple database technologies

**Cons**

* Can be complex to use
* Not as well optimised as a database focusing on one storage type
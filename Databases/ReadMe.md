[TOC]

## Overview
A database is an organised collection of data. It is typically the collection of schemas, tables, queries, reports, views, and other objects. The data is typically organised to model aspects of reality in a way that supports processes requiring information, such as modelling the availability of rooms in hotels in a way that supports finding a hotel with vacancies.

There are two main forms of database: SQL and NoSQL.

## Type Comparison
**Projects where SQL is ideal:**

* There is logically related data which can be identified up-front
* Data integrity is essential
* Consistency of data is more important than partition tolerance
* You want standards-based proven technology with good developer experience
* Commercial applications with good enterprise support is preferred

**Projects where NoSQL is ideal:**

* There are unrelated, indeterminate or evolving data requirements
* Simpler or looser project objectives, able to start coding immediately
* Speed and scalability is imperative
* Partition tolerance is more important than consistency
* Open source technology with good community support is preferred

### SQL
| Pros | Cons |
| --- | --- |
| Powerful query language | Predefined and inflexible data model |
| Optimised for large numbers of table rows | Can be difficult to convert data from Objects into database tables |
| Can handle large numbers of transactions in a single query | Vertically scalable - can only run on one server, so increasing speed means upgrading hardware |
| Many different choices of database, and wide support | Lack of partition tolerance |
| Fast for searching and querying of data | |
| Fast for retrieving data from multiple tables | |
| High availability and consistency of data | |

### NoSQL
| Pros | Cons |
| --- | --- |
Flexible data models, can be changed on the fly without affecting existing data | Doesn't verify the referential integrity of data |
| It is easy to convert data from Objects into the database tables | Lack of enterprise level support |
| Horizontally scalable - across multiple servers | Query language isn't very powerful, so difficult to do complex queries |
| Good at storing large datasets/objects | Slow for searching and complex queries across multiple tables/collections |
| Fast for simple queries, from a single table/collection | Data retrieved isn't always up to date |
| High availability and partition tolerance | |

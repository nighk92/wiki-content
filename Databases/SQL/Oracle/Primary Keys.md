## Auto Incrementing Primary Keys
Oracle databases don't support the AUTO_INCREMENT keyword. Instead a sequence and trigger must be set up to create an auto incrementing field.

1) Firstly create the table
```SQL
CREATE TABLE tab1
(
    id NUMBER(10) PRIMARY KEY,
    name VARCHAR(255)
);
```

2) Create the sequence
This sequence will provide the number for the primary key
```SQL
CREATE SEQUENCE tab1_id_sequence;
```

3) Create the trigger
This trigger will be excuted before a new row is inserted into the table.  It will get the next number from the sequence and set the primary key field to that value.
```SQL
CREATE OR REPLACE TRIGGER tab1_on_insert
    BEFORE INSERT ON tab1
    FOR EACH ROW
BEGIN
    SELECT tab1_id_sequence.nextval
    INTO :new.id
    FROM dual;
END;
```
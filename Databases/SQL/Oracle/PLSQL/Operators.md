[TOC]

## Arithmetic Operators
| Operator | Description | Example |
| --- | --- | --- |
| + | Addition | 1 + 1 = 2 |
| - | Subtraction | 2 - 1 = 1 |
| * | Multiplication | 2 * 2 = 4 |
| / | Division | 4 / 2 = 2 |
| ** | Exponentiation | 3 ** 2 = 9 |
| MOD(value, divisor) | Modulus (remainder) function | MOD(5, 2) = 1 |

## Relational Operators
| Operator | Description | Example |
| --- | --- | --- |
| = | Equal to | 1 = 1 is true |
| != or ~= or ^= or <> | Not equal to | 1 != 1 is false |
| > | Greater than | 2 > 1 is true | 
| < | Less than | 2 < 1 is false |
| >= | Greater than or equal to | 2 >= 2 is true |
| <= | Less than or equal to | 2 <= 2 is true |

## Comparison Operators
| Operator | Description | Example |
| --- | --- | --- |
| LIKE | Searches a string or CLOB for a pattern | 'Hello' LIKE 'el' = true |
| BETWEEN | Tests if a value is in a specified range | 10 BETWEEN 5 AND 20 = true |
| IN | Tests set membership | 'b' IN ('a', 'b', 'c') = true |
| IS NULL | Tests for a null value | 'a' IS NULL = false |
| IS NOT NULL | Tests for a non-null value | 'a' IS NOT NULL = true |

## Logical Operators
| Operator | Description | Example |
| --- | --- | --- |
| AND | Logical and | true AND false = false |
| OR | Logical or | true OR false = true |
| NOT | Reverses logical state | NOT true = false |

## String Operators
| Operator | Description | Example |
| --- | --- | --- |
| 'string' | String delimiter | 'This is a string' |
| ll | Concatenate | 'String 1 ' || 'string to concatenate' |

## Other Operators
| Operator | Description | Example |
| --- | ---- | --- |
| := | Assignment Operator | variable1 := 5 |
| % | Attribute operator | %TYPE |
| : | Binding variables | INSERT INTO table (column) VALUES (:myValue); |
| ; | Statement terminator | SELECT * FROM table; |
| @ and @@ | Runs an external script | @script.sql | 
| => | Name function parameters | myFunction(parm1 => value1, param2 => value2); |
| "variable" | Put quotes around variable names that are keywords, or odd characters | "X+Y" |

## Overview
PL/SQL is a procedural programming language designed to be used with SQL. It adds the ability to store variables, create functions and use external libraries.
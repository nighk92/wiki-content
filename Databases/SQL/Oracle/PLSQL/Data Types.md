## Overview
PL/SQL supports the same data types as an Oracle database. See [Oracle Data Types](../Data Types.md).
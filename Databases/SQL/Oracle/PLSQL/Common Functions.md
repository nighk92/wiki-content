[TOC]

## Overview
PL/SQL supports all the functions of a normal Oracle database (see [here](../Common Functions.md)), as well as the additional functions described below...

## Print to Console
To print to the console server output must be enabled and the dbms_output library must be used.
```SQL
SET serveroutput ON;
BEGIN
  dbms_output.put_line('This is printed to the console!');
END;
```

## Generate a Random Number
To generate a random number the dbms_random library must be used. The value() method generates a random number greater than or equal to the lowest number, but less than the highest number. The numbers generated are decimal numbers, not necessarily integers.
```SQL
dbms_random.value(lowestValue, highestValue);
dbms_random.value(1, 10);
```
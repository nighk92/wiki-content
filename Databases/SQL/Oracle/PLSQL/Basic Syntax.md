[TOC]

## Blocks
PL/SQL code is run in blocks.
First there is a DECLARE section where all variables used in the block must be created (with or without values)
The BEGIN section is where all the code is executed
Finally the END keyword closes the block.
```SQL
DECLARE
  numberVariable NUMBER := 0;
  stringVariable VARCHAR(255);
BEGIN
  stringVariable := 'Hello';
  SELECT COUNT(*) INTO numberVariable FROM tab1 WHERE col1 = stringVariable;
  dbms_output.put_line('There are ' || numberVariable || ' records where col1 = ' || stringVariable);
END;
```

## Functions
Functions can be made to store frequently used code. Function names aren't case sensitive.
Firstly the function must be declared, with any arguments
Then a return value can be specified, and underneath any local variables used in the function must be created
The BEGIN section is where all the code is executed
The RETURN clause is used to specify the return value
Finally the function is closed with the END clause
Functions without arguments don't have brackets after their name.
```SQL
-- Function to add two numbers together and multiply the result by 2
CREATE OR REPLACE FUNCTION myFunction(arg1 NUMBER, arg2 NUMBER)
  RETURN NUMBER IS returnNum NUMBER;
  numberVariable NUMBER;
BEGIN
  numberVariable := arg1 + arg2;
  returnNum := numberVariable * 2;
  RETURN returnNum;
END;

-- Call the function
BEGIN
  myFunction(2, 3); -- Result is 10
END;
```

## IF Statements
IF statements have the following syntax, they must be closed with an END IF and a semicolon
```SQL
IF condition
THEN
    --code
ELSIF condition
THEN
   --code
ELSE
   --code
END IF;
```

##CASE statement
A CASE statement is like a more convenient way of testing a lots of conditions instead of using IF, ELSIF, ELSE.

The selector is a variable. If it matches a selector value then that THEN statement is executed, and no other conditions are evaluated. If no selector values are matched then the ELSE statement is executed.
```SQL
CASE selector
    WHEN selector_value_1
    THEN 
        statements_1
    WHEN selector_value_2
    THEN 
        statements_2
    ELSE
        else_statements
END CASE;
```

## FOR loop
FOR loops can be used to iterate over lists or queries. 
```SQL
FOR item IN (
    SELECT * FROM table
)
LOOP
    dbms_output.put_line("Column 1 = " || item.column1);
END LOOP;
```

The REVERSE keyword can be added to loop backwards, the following prints: 3, 2, 1,
```SQL
FOR item IN REVERSE 1..3
LOOP
    dbms_output.put_line(i || ', ');
END LOOP;
```

The CONTINUE WHEN statement can be used to skip to the next iteration of the loop, the  CONTINUE  statement can also be used on it's own. The following prints: 1, 3,
```SQL
FOR i IN 1..3
LOOP
    CONTINUE WHEN i = 2;
   dbms_output.put_line(i || ', ');
END LOOP;
The EXIT WHEN statement can be used to exit the loop, the following prints: 1, 2,
The EXIT statement can also be used on it's own.
FOR i IN 1..3
LOOP
   EXIT WHEN i = 3;
   dbms_output.put_line(i || ', ');
END LOOP;


## WHILE loop
WHILE loops run statements while a condition is true.

WHILE condition
LOOP
    -- code
END LOOP;
```

## GOTO statement
The GOTO statement can be used to jump to a labelled section of the code.
```SQL
BEGIN
     FOR i IN 1..3
    LOOP
        dbms_output.put_line(i);
         GOTO print_now;
     END LOOP;

    dbms_output.put_line('This is not printed');

     <<print_now>>
    dbms_output.put_line('Print Now');
END;
```

## Comments
Comments can be made using two dashes
```SQL
-- This is a comment
```
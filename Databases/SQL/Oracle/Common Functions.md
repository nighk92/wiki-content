[TOC]

## Overview
Oracle databases support many functions over the normal SQL syntax. All these functions can be used within SQL statements or in PL/SQL code.

## Conversion Functions
The TO_CHAR function can convert a date or a number to a string. When converting a date a format can be specified.
```SQL
TO_CHAR(123); -- Result: '123'

TO_CHAR(sysdate, 'DD/MM/YYYY'); -- Result: 01/01/2017
TO_CHAR(sysdate, 'Day, ddth Month YYYY'); -- Result: Monday, 1st January 2017
``

The TO_NUMBER function converts a String to a NUMBER
```SQL
TO_NUMBER('123'); -- Result: 123
```

## String Functions
The LENGTH function counts the number of characters in a string.
```SQL
LENGTH('hello'); -- Result: 5
```

The UPPER and LOWER functions convert a string to upper or lower case.
```SQL
UPPER('Hello'); -- Result: 'HELLO'
LOWER('Hello'); -- Result: hello
```

The SUBSTR function is used to get part of a string. It takes in the String, the starting position (the first letter is always number 1) and an optional length. If the length isn't provided then all characters after the starting position are returned. For more complex substrings use REGEXP_SUBSTR.
```SQL
SUBSTR(String, StartingPosition, Length);
SUBSTR('hello', 2); -- Result: ello
SUBSTR('hello', 2, 2); -- Result: el
```

The REPLACE function replaces all occurrences of the specified string with the replacement string. For more complex replacements use REGEXP_REPLACE.
```SQL
REPLACE(originalString, toBeReplaced, replacement)
REPLACE('hello', 'llo', 'at'); -- Result: 'heat'
```

The TRIM function will remove the specified characters from the start and end of a string.
```SQL
TRIM('   tech   '); -- Result: 'tech'
TRIM('hello'  FROM  'hellotechhello'); -- Result: 'tech'
```

The CONCAT function appends one string to another, the Oracle version of this function can only take 2 strings unlike the MySQL version which can take an unlimited number.
```SQL
CONCAT('string1 ', 'string2'); 
-- Result: 'string1 string2'
```

## Mathematical Functions
The ROUND function rounds a number to the nearest whole integer.
```SQL
ROUND(4.5) -- Result: 5
```

The CEIL function rounds a number up to the next whole integer
```SQL
CEIL(4.5) -- Result: 5
```

The FLOOR function rounds a number down to the next whole integer
```SQL
FLOOR(4.5) -- Result: 4
```

The MOD function gives you the remainder of a division
```SQL
MOD(15, 4); -- Result: 3
```

## Aggregate Functions
The MAX function returns the maximum value from a column or expression
```SQL
SELECT MAX(col1) FROM table1;
```

The MIN function returns the minimum value from a column or expression
```SQL
SELECT MIN(col1) FROM table1;
```

The COUNT function returns the number of rows in a table/column, or counts the result of an expression
```SQL
SELECT COUNT(*) FROM table1;
```

The SUM function adds all the numbers in a column, or numbers returned from an expression
```SQL
SELECT SUM(numCol) FROM table1;
```

The AVG function returns the average of all numbers in a column, or numbers returned from an expression.
```SQL
SELECT AVG(numCol) FROM table1;
```

## Date/Time Functions
The SYSDATE function gets the current date and time of the local database
```SQL
SYSDATE; -- Result: Current date/time e.g. 01-Jan-17 09.30.15 AM
```

## JSON Functions
The JSON_VALUE function obtains the value of a variable stored inside a JSON object
```SQL
JSON_VALUE('{a: 100}', '$.a'); -- Result: 100
```
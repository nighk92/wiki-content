## Overview
The Oracle database is an enterprise database that uses SQL for it's queries and functions. It also supports PL/SQL and many additional functions, over a normal SQL database.
[TOC]

## Common Data Types
These are the most common data types used within SQL:

| Data Type | Description |
| --- | --- | 
| VARCHAR2(length) | Variable number of characters, up to specified length | 
| BOOLEAN | TRUE or FALSE | 
| INTEGER / NUMBER | A whole number | 
| FLOAT | A decimal number | 
| DOUBLE | A large decimal number |
| DATE | Oracle Format: YYYY-MM-DD HH:MM (time is optional) | 
| TIMESTAMP | Oracle Format: YYYY-MM-DD HH:MM:SS |
| BLOB | A binary large object that can hold a variable amount of data. Useful for storing JSON. |
| CLOB | A character large object that can hold a variable amount of data. However due to the encoding it takes up twice as much space as a BLOB, so should only be used to store large amounts of text |

## Column Constrains
These are additional constrains that can be added to columns of any data type:

| Constraint | Description |
| --- | --- |
| PRIMARY KEY | Specifies the column to uniquely identify a row in a table, this value must be unique for every row of the table and never null |
| AUTO_INCREMENT | Used to auto increment a primary key column | 
| UNIQUE | Value must be different in every row in the table |
| NOT NULL | Value can't be null |
| DEFAULT value | Specifies a default value if no other value is entered |
| CHECK(condition) | Ensures the value meets the specified boolean condition |
| FOREIGN KEY REFERENCES table(column) | Specifies that the value must correspond to values in the specified primary key or unique column. The value can also be null, unless the NOT NULL constraint is added to the column |

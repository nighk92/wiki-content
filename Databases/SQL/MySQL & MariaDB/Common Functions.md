[TOC]

## Overview
MySQL databases support many functions over the normal SQL syntax. All these functions can be used within SQL statements.

## Conversion Functions
The CONVERT function can convert any datatype to the specified type.
```SQL
CONVERT(123, CHAR) -- Result: '123'
CONVERT('123', SIGNED) -- Converts to a signed integer, Result: 123
```

## String Functions
The LENGTH function counts the number of characters in a string.
```SQL
LENGTH('hello'); -- Result: 5
```

The UPPER and LOWER functions convert a string to upper or lower case.
```SQL
UPPER('Hello'); -- Result: 'HELLO'
LOWER('Hello'); -- Result: hello
```

The SUBSTR function is used to get part of a string. It takes in the String, the starting position (the first letter is always number 1) and an optional length. If the length isn't provided then all characters after the starting position are returned.
```SQL
SUBSTR(String, StartingPosition, Length);
SUBSTR('hello', 2); -- Result: ello
SUBSTR('hello', 2, 2); -- Result: el
```

The REPLACE function replaces all occurances of the specified string with the replacement string.
```SQL
REPLACE(originalString, toBeReplaced, replacement)
REPLACE('hello', 'llo', 'at'); -- Result: 'heat'
```

The TRIM function will remove the specified characters from the start and end of a string.
```SQL
TRIM('   tech   '); -- Result: 'tech'
TRIM('hello'  FROM  'hellotechhello'); -- Result: 'tech'
```

The CONCAT function appends one string to another
```SQL
CONCAT('string1 ', 'string2 ', 'string3 '...); 
-- Result: 'string1 string2 string2 3'
```

## Mathematical Functions
The ROUND function rounds a number to the nearest whole integer.
```SQL
ROUND(4.5) -- Result: 5
```

The CEIL function rounds a number up to the next whole integer
```SQL
CEIL(4.5) -- Result: 5
```

The FLOOR function rounds a number down to the next whole integer
```SQL
FLOOR(4.5) -- Result: 4
```

The MOD function gives you the remainder of a division
```SQL
MOD(15, 4); -- Result: 3
```

## Aggregate Functions
The MAX function returns the maximum value from a column or expression
```SQL
SELECT MAX(col1) FROM table1;
```

The MIN function returns the minimum value from a column or expression
```SQL
SELECT MIN(col1) FROM table1;
```

The COUNT function returns the number of rows in a table/column, or counts the result of an expression
```SQL
SELECT COUNT(*) FROM table1;
```

The SUM function adds all the numbers in a column, or numbers returned from an expression
```SQL
SELECT SUM(numCol) FROM table1;
```

The AVG function returns the average of all numbers in a column, or numbers returned from an expression.
```SQL
SELECT AVG(numCol) FROM table1;
```

## Date & Time Functions
The SYSDATE function gets the current date and time of the local database
```SQL
SYSDATE; -- Result: Current date/time e.g. 01-Jan-17 09.30.15 AM
```

## JSON Functions
The JSON_EXTRACT function obtains the value of a variable stored inside a JSON object
```SQL
JSON_EXTRACT('{a: 100}', '$.a'); -- Result: 100
```
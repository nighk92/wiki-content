[TOC]

## Common Data Types
These are the most common data types used within MySQL & MariaDB:

| Data Type | Description |
| --- | --- |
| VARCHAR(length) | Variable number of characters, up to specified length |
| ENUM(x, y, z, etc.) | Field with a  list of possible choices |
| SET(x, y, z, etc.) | Like an ENUM but can store more than one type |
| BOOLEAN | TRUE or FALSE |
| INTEGER | A whole number |
| FLOAT | A decimal number |
| DOUBLE |A large decimal number | 
| DATE | MySQL Format: YYYY-MM-DD |
| DATETIME | MySQL Format: HH:MM:SS |
| TIME | MySQL Format: HH:MM:SS |
| BLOB | A binary large object that can hold a variable amount of data. Useful for storing JSON. |
| TEXT | A character large object that can hold a variable amount of data. However due to the encoding it takes up twice as much space as a BLOB, so should only be used to store large amounts of text |
| JSON |A MySQL specific type, optimised to store JSON data |

## Column Constraints
These are additional constraints that can be added to columns of any data type:

| Constraint | Description |
| --- | --- |
| PRIMARY KEY | Specifies the column to uniquely identify a row in a table, this value must be unique for every row of the table and never null | 
| AUTO_INCREMENT | Used to auto increment a primary key column | 
| UNIQUE | Value must be different in every row in the table | 
| NOT NULL | Value can't be null |
| DEFAULT value | Specifies a default value if no other value is entered | 
| CHECK(condition) | Ensures the value meets the specified boolean condition | 
| FOREIGN KEY REFERENCES table(column) | Specifies that the value must correspond to values in the specified primary key or unique column. The value can also be null, unless the NOT NULL constraint is added to the column |
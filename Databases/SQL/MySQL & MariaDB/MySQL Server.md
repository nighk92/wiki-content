[TOC]

## Overveiw
The MySQL server can be installed on Linux or Windows from here. The default port for the server is **3306**.

## Starting/Stopping
Starting and stopping the server on windows, these commands must be run from an administrator command prompt
```bash
net start MySQL57

net stop MySQL57
```

Starting and stopping the server on linux
```bash
sudo service mysql start

sudo service mysql stop
```

## Access the SQL command prompt
Login to the server, to access the SQL command prompt
```bash
mysql --user=NAME -p

# Options
# --user = the user account to login with
# -p = Prompt for a password
# --host = specify the host of the server, default is localhost
# --port = specify a port number to connect to, default is 3306
```
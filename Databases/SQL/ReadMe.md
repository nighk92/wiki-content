[TOC]

## Overview
SQL is the language widely used by Relational Databases. It revolves around the storing and manipulation of data. It is used by many databases including MySQL, MariaDB, and Oracle. Each database has its own variant of SQL.

SQL is a command line language, each command contains keywords and ends with a semi-colon.

## Database Comparison
There are many different implementations of SQL databases. Each have their own advantages and disadvantages. See the table below for a high level comparison of the common SQL databases:

| | MySQL | MariaDB | Oracle DB | PostgreSQL |
| --- | --- | --- | --- | --- |
| **Overall** | \* \* \* | \* \* \* \* | \* \* \* \* | \* \* \* |
| **Speed** | \* \* \* | \* \* \* \* | \* \* \* | \* |
| **Features** | \* \* \* | \* \* \* | \* \* \* \* \* | \* \* \* \* |
| **Flexibility** | \* \* | \* \* | \* \* \* | \* \* |
| **Reliability** | \* \* \* \* | \* \* \* \* | \* \* \* \* \* | \* \* \* \* |
| **Support** | \* \* \* | \* \* \* | \* \* \* \* \* | \* \* \* |
| **Cost** | \* \* \* | \* \* \* \* \* | \* | \* \* \* \* \* |


### MySQL
MySQL is a partly open source database engine, with MySQL server edition being closed source, both are managed by Oracle. It has a large user base and a large amount of enterprise support. It also has extensive community support due to its open source nature. It is lightweight and fast to set up, and still has almost all of the features that an Oracle database does, except that it doesn't support PL/SQL. It has wide support across many programming languages. One downside of MySQL is that performance can drop off when storing large amounts of data. This should be used as a good free alternative to Oracle, for smaller applications that need a fast setup and don’t need a lot of computational resources.

### MariaDB
MariaDB is a fork of the MySQL database, it was created when Oracle took over the development of MySQL in 2010. It has many of the features of MySQL and is fully compatible with MySQL drivers and libraries, so programming language support is high. It has speed improvements over MySQL, and so scales better to large amounts of data. It has a lot of community support as it has been growing in popularity over the past few years, but its enterprise support is not up to the standard of Oracle’s. It is however beginning to diverge from MySQL so may not always be compatible with it in the future. MariaDB should be used as a slightly faster alternative to MySQL.

### Oracle DB
Oracle is the most popular enterprise standard database, it has a large number of users and so has extensive support if you are willing to pay for it. There is a free version (express), but the features are limited so it is only suitable for small applications. It is designed to hold large amounts of data, has support for PL/SQL, has additional commands like tablespace, synonyms and packages, and has several backup mechanisms. It has wide support across many major programming languages. This is the SQL database to use for large enterprise applications that hold lots of data, and need a fast reliable database.

### PostgreSQL
PostgreSQL is an open source database. It has some specific use cases, and so in general isn’t as fast as other SQL databases. However it excels at joining large numbers of tables, and searching through large amounts of data. It also has support for PL/SQL, unlike MySQL and MariaDB. PostgreSQL should be used for complex data models, where lots of tables are present and queries are complex. It can be used as an open source alternative to oracle when PL/SQL support is needed.
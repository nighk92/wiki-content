[TOC]

## Database Commands
Create a database
```SQL
CREATE DATABASE db1;
```

Delete a database
```SQL
DROP DATABASE db1;
```

Specifies a database to use, so table operations can be performed on it
```SQL
USE db1;
```

Show all tables in the currently selected (used) database
```SQL
SHOW tables;
```

Shows all columns in a table
```SQL
DESCRIBE tableName;
```

Show the SQL structure of a table, including the names of foreign key constraints
```SQL
SHOW CREATE TABLE table1;
```

## SELECT Commands
Shows all data from a table
```SQL
SELECT * FROM tab1;
```

Shows all the data for the specified columns in a table
```SQL
SELECT col1, col2 FROM tab1;
```

Select data from multiple tables
```SQL
SELECT tab1.col1, tab2.col1 FROM tab1, tab2;
```

## WHERE Clause
Shows all rows from a table where the constraint is met e.g. to show all rows where col1 equals the integer 123
```SQL
SELECT * FROM tab1
WHERE col1 = 123;
```

Shows all rows where col1 is equal to the string
```SQL
SELECT * FROM tab1
WHERE col1 = 'hello';
```

Show all rows where col1 is null
```SQL
SELECT * FROM tab1
WHERE col1 IS NULL;
```

Show all rows where col1 is not null
```SQL
SELECT * FROM tab1
WHERE col1 IS NOT NULL;
```

The IN keyword can be used to decide if a value is in a set
```SQL
SELECT * FROM tab1
WHERE col1 IN (1,2,3);
```

A SELECT clause can also be used inside a WHERE clause
```SQL
SELECT * FROM tab1
WHERE col1 IN (SELECT col1 FROM tab2);
```

The AND keyword is used to link WHERE clauses together
```SQL
SELECT * FROM tab1
WHERE col1 IN (1,2,3)
AND col2 = 'test'
AND col3 IS NULL;
```

## ORDER BY Clause
The ORDER BY clause is useful to sort large amounts of data, the default order is descending by ID.

Show all rows ordered by col1's values in ascending order (which can be either alphabetical for words or the logical order for numbers)
```SQL
SELECT * FROM tab1
ORDER BY col1 ASC;

-- use DESC to sort in descending order
```

## LIMIT Clause
The LIMIT clause can be very useful to speed up queries likely to retrive a large number of results.

Show the first 10 rows from the table
```SQL
SELECT * FROM tab1
LIMIT 10;
```

## COUNT Clause
COUNT gives you the number of rows returned from a  query, not the rows themselves.

Counts the number of rows in a table
```SQL
SELECT COUNT(*) FROM tab1;
```

Counts the number of rows in a table and stores it in a variable called rowNumber
```SQL
SELECT COUNT(*) AS rowNumber FROM tab1;
```

Adds up all the integer values from the specified column
```SQL
SELECT COUNT(int_col) FROM tab1;
```

## DISTINCT Clause
The DISTINCT clause is used to return only distinct (different) values, stripping out any duplicates.

Select all distinct records from a column, or combination of columns
```SQL
SELECT DISTINCT col1, col2 FROM tab1;
```

Count how many distinct values there are for a column
```SQL
SELECT COUNT(DISTINCT col1) FROM tab1;
```

Count how many distinct values there are for a combination of columns, this has to be done with a subquery as the DISTINCT clause within a COUNT only accepts one column as an argument
```SQL
SELECT COUNT(1) FROM (SELECT DISTINCT col1, col2 FROM tab1);
```

## GROUP BY Clause
GROUP BY must be used with an aggregate function, which reduces data. Aggregate functions are MIN, MAX, SUM, AVG, COUNT.

Group all results by col1
```SQL
SELECT col1, aggregate_func(col1) FROM tab1
GROUP BY col1;
```

This query counts the number of orders each ShipperName had got, storing it in a row called numOrders
```SQL
SELECT Ship.ShipperName, COUNT(Orders.ID) AS numOrders
FROM Orders
    LEFT JOIN Ship
        ON Orders.ID = Ship.ShipperID
GROUP BY ShipperName;
```

To group by multiple columns the columns in the GROUP BY statement must match the SELECT statement, apart from the aggregate function.
This query counts the number of orders by the ShipperName and LastName (which is retrieved from the Employees table)
```SQL
SELECT Ship.ShipperName, Employees.LastName, COUNT(Orders.ID) AS NumOrders
FROM ((Orders
        INNER JOIN Ship
            ON Orders.ShipperID = Ship.ShipperID)
    INNER JOIN Employees
        ON Orders.EmployeeID = Employees.EmployeeID)
GROUP BY ShipperName, LastName;
```

## JOINS
Join tables together to get info from multiple tables in one query, more efficiently than a SELECT statement

Return rows that have at least one match in **both** tables
```SQL
INNER JOIN;
```

Return all rows from the LEFT table, and matched rows from the RIGHT table
```SQL
LEFT JOIN;
```

Return all rows from the RIGHT table, and matched rows from the LEFT table
```SQL
RIGHT JOIN;
```

Return all rows where there is a match in **one** of the tables
```SQL
FULL JOIN;
```

This query gets the customer name for an order, by doing an INNER JOIN to get the customers information
```SQL
SELECT Orders.ID, Orders.Date, Customers.Name
FROM Orders
    INNER JOIN Customers
        ON Orders.CustID = Customers.ID;
```

Multiple joins can be done in one query, and the WHERE clause must go after the joins
```SQL
SELECT Orders.ID, Orders.Date, Customers.Name, Item.Name
FROM Orders
    INNER JOIN Customers
        ON Orders.CustID = Customers.ID
    INNER JOIN Item
        ON Orders.ItemID = Item.ID
WHERE Orders.Date >= '2016-01-01';
```

## Table Structure
Create a table
**Note**: Oracle databases don't support AUTO_INCREMENT. For information on creating auto incrementing primary keys in Oracle see [here](Oracle/Primary Keys.md)
```SQL
CREATE TABLE tab1 (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50)
);
```

Delete a table
```SQL
DROP TABLE tab1;
```

Adds a new column to a table
```SQL
ALTER TABLE tab1
ADD col1 VARCHAR(50);
```

Rename a column
```SQL
-- MySQL
ALTER TABLE tab1
CHANGE old_name new_name datatype;

-- Oracle
ALTER TABLE tab1
RENAME COLUMN old_name TO new_name;
```

Deletes a column
```SQL
ALTER TABLE tab1
DROP COLUMN col1;
```

Add a foreign key constraint, which automatically gets updated/deleted when the parent column (tab1.col1) is changed
```SQL
ALTER TABLE tab1    
ADD CONSTRAINT FK_NAME FOREIGN KEY (col1)    
    REFERENCES tab2.col1    
    ON DELETE CASCADE   
    ON UPDATE CASCADE;
```

Inserts a new row into a table. If the primary key is auto incrementing, then don't include it in the INSERT
```SQL
INSERT INTO tab1 (id, col1)
VALUES (1, 'test');
```

Change rows in a table, this example changes all occurances of old to new
```SQL
UPDATE tab1
SET col1 = 'new',
col2 = 'new2'
WHERE col1 = 'old';
```

Deletes a row from a table
```SQL
DELETE FROM tab1
WHERE id=1;
```

Deletes all rows from a table (no WHERE clause)
```SQL
DELETE FROM tab1;
```

Makes table changes permanent, this is needed after any INSERT, UPDATE or DELETE operations
```SQL
COMMIT;
```

Can be used to reverse changes that haven't been commited
```SQL
ROLLBACK;
```

## Table Indexes
Indexes allow data in the specified column to be found more quickly.

Shows all indexes for a table
```SQL
SHOW INDEX FROM tab1;
```

Add an index to a table
```SQL
CREATE INDEX index_name ON tab1 (col1);
```

Deletes the specified index
```SQL
DELETE INDEX index_name ON tab1;
```

## Users
A database can have multiple users, each with different access levels.

Creates a new user
```SQL
CREATE USER name IDENTIFIED BY password;
```

Give a user unlimited access to all tables within a database
```SQL
GRANT ALL ON db1.* TO name;
```

Give a user specific access to all tables within a database
```SQL
GRANT SELECT, UPDATE, INSERT, DELETE ON db1.* TO name;
```

Make a user Admin
```SQL
GRANT dba TO name;
```

Change a users password
```SQL
ALTER USER name IDENTIFIED BY new_password;

-- or if changing for the current user who isn't an admin
ALTER USER name IDENTIFIED BY new_password REPLACE old_password;
```

Check when a users password next expires, when logged in as that user
```SQL
SELECT account_status, expiry_date FROM user_users;
```

Unlocks a user account E.g. if it has been locked by too many incorrect login attempts
```SQL
ALTER USER name ACCOUNT UNLOCK;
```

Lock a user account, so it can no longer be logged into
```SQL
ALTER USER name ACCOUNT LOCK;
```

## Comments
Single line comments in SQL are inserted using two dashes
```SQL
-- this is a comment
-- this is another comment
```

## Escaping Special Characters
Apostrophes in a String can be escaped by putting two of them
```SQL
'String with an '' apostrophe'
-- Will become: String with an ' apostrophe
```

## Cheat Sheet
[SQL Cheat Sheet.docx](.files/SQL Cheat Sheet.docx)
[TOC]

## Select sparingly
Avoid using SELECT * as much as possible. Only SELECT the columns you need especially when searching through lots of data.

## Avoid coding loops
When running database queries from a program avoid running them in a loop. E.g. instead of running a single insert over and over again in a loop, use a multiple insert query to insert several records at once.

## Favour joins over subqueries
Using a subquery to filter information based on the parent query is slow
```SQL
SELECT c.Name,
        c.City,
       (SELECT CompanyName FROM Company WHERE ID = c.CompanyID) AS CompanyName
FROM Customer c;
```

Instead replace the above with an LEFT JOIN
```SQL
SELECT c.Name,
        c.City,
       co.CompanyName
FROM Customer c 
    LEFT JOIN Company co
        ON c.CompanyID = co.CompanyID;
```

## Prefer INNER JOIN
Inner joins are the fastest type of join.

## Avoid multiple joins in one query
This reduces the amount the SQL optimiser can do to optimise your query, this is particularly true when using outer joins.

## Remove calculations from JOIN and WHERE clauses
Don't use calculations in JOIN and WHERE clauses as that calculation will be carried out on every row in your table. Replace any calcualtions e.g. adding two numbers together, with a column that already has the answer and retrieve it with a SELECT command.

## Avoid using wildcards at the start of a LIKE statement
Doing a query like this means that a full table scan is done instead of using any indexes on that column
SELECT id, name WHERE name LIKE '%john%';

## Use NOCOUNT for UPDATE, INSERT and DELETE
When doing an UPDATE, INSERT or DELETE the database will return a count with the number of records that have been updated. These query's can be made faster by turning NOCOUNT on beforehand, so avoiding the overhead of counting the number of records affected.

This command must be run before each procedure/execution batch:
```SQL
SET NOCOUNT ON;
```

## Indexes
Indexing a column speeds up the SELECTING and SORTING of data from that column.  In general indexes should be applied to primary keys, foriegn keys, and columns that are searched/sorted a lot e.g. using WHERE and SORT commands. However don't index everything as too many indexes will slow things down more.

Follow this diagram to decide which columns to index:

![SQL Indexing Diagram](.images/SQL_Indexing_Diagram.png)
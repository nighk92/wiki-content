## Overview

A message broker is a system tha can receive messages, store them, and pass them on. This usually works on a provider/consumer basis, where the provider system puts messages onto a queue/topic, and multiple consumers read that queue/topic to process the message. They are commonly used to decouple systems, and enable async processing.
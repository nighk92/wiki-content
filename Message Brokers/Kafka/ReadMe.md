## Overview

Apache Kafka is a software bus for processing data streams. Often these are in the form of messages that multiple consumers can subscribe to. It aims to provide a low-latency, high-throughput platform for processing of real-time data feeds.
[TOC]

### Topic Operations

List all topics
```bash
kafka-topics --list --zookeeper <hostname>:2181
```

Delete a topic
```bash
kafka-topics --delete --topic ocx-mkb-explore --zookeeper <hostname>:2181
```
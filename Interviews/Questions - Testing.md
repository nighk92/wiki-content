## Questions - Testing

### What is TDD? (Easy)
* Test Driven Development
* Write the tests before the code

### What is the difference between Unit Testing, Integration Testing and Usability Testing?	(Easy)
* Unit Testing - Test individual parts of the code in isolation
* Integration Testing - Testing of the program as a whole, to ensure it all comes together and works properly
* Usability Testing - Check that users are happy with the system and it is intuitive to use

### What testing technologies have you used? (Easy)
* Java - JUnit, EasyMock, Mockito, Cucumber, Selenium
* Javascript - Enzyme / Jest

### What level of code coverage should you aim for with unit tests? (Easy/Medium)
* Aim for 100%
* +1 - If they recognise that 100% coverage isn't always achievable, and that in reality a minimum of 80% coverage should be aimed for

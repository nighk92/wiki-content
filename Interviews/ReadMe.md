## Outline
The following steps should be followed for the interview. The technical and softer questions are placed in order of importance, and parts may be omitted where they aren't applicable.

* Outline of Interview - [Telephone](Outline For Telephone Interview.md) / [Face-to-Face](Outline For Face-to-face Interview.md)
* [Introduction to the Company](Introduction To The Company.md)
* Technical Questions
    * [Core Java](Questions - Core Java.md)
    * [Spring](Questions - Spring.md)
    * [Cloud](Questions - Cloud.md)
    * [General Technology](Questions - General Technology.md)
    * [Testing](Questions - Testing.md)
    * [Process & Agile](Questions - Process And Agile.md)
    * [Problem Solving & Debugging](Questions - Problem Solving And Debugging.md)
    * [Design](Questions - Design.md)
    * [JavaScript](Questions - Javascript.md)
        * [React](Questions - React.md)
        * [Angular](Questions - Angular.md)
    * [JavaEE](Questions - JavaEE.md)
    * Databases
        * [SQL](Questions - SQL Databases.md)
        * [NoSQL](Questions - NoSQL Databases.md)
* Softer Questions
    * [Core Strengths](Questions - Core Strengths.md)
    * [Clean Code & Good Engineering](Questions - Clean Code And Good Engineering.md)
    * [Tools](Questions - Tools.md)
    * [Situational Questions](Questions - Situational.md) (to get a better idea of personality and leadership skills)
* Scenario (Face-to-Face interview only)
* Time for their Questions
* Clarify next steps
    * Ask if they have applied for or have offers from other companies

## Tips
* Keep in mind the main aim is to test they know what they say they do
* Ask lots of **why** questions, to ensure they really understand programming as opposed to just doing it.
* Only ask 3-4 questions from each area otherwise you will run out of time!
* All questions have difficulty rating:
* Easy - graduate / slightly experienced developer should be able to answer most of these for core skills, senior developer should be able to answer these for most technologies
* Medium - more senior developers should be able to answer these for their core skills, team leaders should be able to answer these for most technologies
* Hard - team leaders should be able to answer these
## Motivation
* What do they already know about the Company?  Have they done research on us beforehand?  Do they know people who work for us?
* What is their reason for moving?  If they have moved around very frequently, try to establish if there's a reason for this.
* What has attracted them to the Company?  What do they think we will offer that their previous employer did not?

## Overview of the Company
Give an overview of the company, what is does, number of employees, recent performance.

## Outline of Role
Ensure that they have applied for the role you are interviewing them for, then go onto explaining the team and what their role might be.

## Ability to Work
Check they are a UK resident or have no constraints to work in the UK. If they do hold a Visa clarify the expiry date and what their long term plans are.

## Mobility 
Check if the candidate is willing to be mobile.

## Questions - General Technology

### What is Continuous Integration and why would you do it?	(Easy)

### What is Continuous delivery? (Easy)

### What CI/CD tools have you used?	Jenkins or Bamboo (Easy)

### What is a Docker Container? What are the advantages of them? How do they differ from a virtual machine? (Easy/Medium)
* A container is a format where an application and its dependencies can be packed together to form a deployable unit. 
* Containers do not contain an OS and instead make use of the underlying host to isolate processes and resources. 
* The most common container format is Docker but others do exist.

### How do you handle boring repetitive tasks? (Low/Medium)
* Should mention scripting or automation etc.

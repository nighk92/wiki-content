## Questions - React

### What is a component? (Easy)
* It is a JavaScript class or function that exports a React element

### What are the advantages of components? (Easy)
* They separate the code into logical parts, making it more understandable and maintainable
* They encourage code re-use
* They encourage separation of concerns leading to more de-coupled code

### How should components be used within an application? (Easy)	
* An application should have many components. the UI should be split into many logical parts.
* Examples of components might be:
    * A title bar
    * A sidebar menu
    * A search bar
    * A data table on a page

### What are props? (Easy)
* The properties/arguments passed into a component

### What is state? (Easy)
* The local collection of variables for a component
* These can be used anywhere within the component
* They generally helps to decide how the component is rendered, and how is behaves

### What is Redux? (Easy/Medium)
* It is a global state store for JavaScript apps. It enables communication between components

### How is the Redux store updated, and what is the lifecycle for this?	(Medium)
![Redux Store Lifecycle](.images/Redux_Store_Lifecycle.png)
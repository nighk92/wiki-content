## Questions - Situational

### Describe a situation where you had to collaborate with a difficult colleague
* Look for a professional approach and ability to increase communication.
* It isn't possible to always get on well with everyone but the candidate should show the ability to create a good working relationship with other colleagues.

### Describe a situation where you had to work with a difficult manager or important client/customer
* The candidate should be able to deal with difficult situations in an amicable way. 
* They should listen to authority but also not be afraid to communicate with people and offer advice.

### Describe a situation where you needed to persuade someone to accept your point of view or convince them to change something	
* The candidate should be able to argue their point of view and back up their arguments with sound facts. 
* They should facilitate communication to solve conflicts
* Show willing to listen to others points of view at all times
* Perhaps even coming to a compromise

### Describe a difficult problem you faced and how you approached it
* Should look for a logical solution
* Calling on help of others where necessary
* Conducting research and prototyping solutions are also good things to do here

### Describe a mistake you've made professionally
* Look for their ability to criticise themselves
* They should take responsibility for fixing their own mistakes
* Retrospectively improving themselves

### Describe a situation where you worked under a tight deadline
* Look for their ability to work under stress and meet deadlines
* Not necessarily working longer hours, but instead being organised, reducing scope where necessary
* Communicating regularly with other team members and the client to keep everyone informed of progress

### Describe a time when you received criticism
* The candidate should be able to take criticism from others and improve themselves
* The should argue their corner in a calm and logical way where appropriate

### Describe a situation when you needed to take initiative	
* This is aimed more towards candidates aiming for higher grades
* A good answer will consist of a time that they came up with a new solution, facilitated team work, or lead on a particular piece of work
* It should help to show how organised and proactive they can be

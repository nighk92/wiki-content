## Questions - Javascript

### What versions of Javascript have you used? (Easy)
* ES5, ES6, ES2019

### What are some of your favorite new features in Javascript? (Easy)
* Arrow functions (ES6)
* Object destructing (ES6)
* Ability to create classes (ES5 / ES6)
* Spread operator (ES2018)

### What is the difference between const, var and let? (Easy/Medium)
* **var** is function scoped, basically it is available anywhere within the function where it is declared. Even if it is declared inside a for loop or try/catch block, it is still available outside of the block as long as it is inside the parent function.
* **let** is block scoped (like a standard variable in Java)
* **const** is block scoped like let, but it's value can't be re-assigned (doesn't necessarily make it immutable though!)

### What does 'use strict' mean when used at the beginning of a source file? (Medium)
* Enforce stricter parsing and error handling on your JavaScript code at runtime. 
* Code errors that would otherwise have been ignored or would have failed silently will now generate errors or throw exceptions. 
* In general, it is a good practice.

### How do you organise your Javascript code? (Easy)

### What are the different JavaScript types? (Easy)
* Unlike Java or C#, JavaScript is a loosely-typed language (some call this weakly typed); this means that no type declarations are required when variables are created.

### What is the difference between undefined and null? (Easy/Medium)

### What JavaScript frameworks have you used? (Easy)

### How does inheritance work in JavaScript? (Medium)

### How does JavaScript differ from Java? (Easy/Medium)

### How do you test your JavaScript? (Easy)

### How do you build and release your JavaScript? (Easy)


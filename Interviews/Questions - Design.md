## Questions - Design

### What is design? (Easy)

### What is the difference between design and architecture?	(Medium)

### How do you communicate your design with developers?

### What design artefacts / diagrams do you use most and why? (Easy/Medium)

### How do you express an ‘is a’ relationship and a ‘has a’ relationship in code or explain inheritance and composition? (Medium)
* "Is a" relationship is expressed with inheritance
* "Has a" relationship is expressed with composition. 
* Inheritance is uni-directional. For example House is a Building. But Building is not a House. 
* Inheritance uses extends key word. 
* Composition is used when House has a Bathroom. It is incorrect to say House is a Bathroom. 
* Composition simply means using instance variables that refer to other objects. The class House will have an instance variable, which refers to a Bathroom object.

### When do you favour inheritance or composition? (Medium)
* Don’t use inheritance just to get code reuse. 
* If there is no ‘is a’ relationship then use composition for code reuse. 
* Overuse of implementation inheritance (uses the “extends” key word) can break all the subclasses, if the superclass is modified. 
* Do not use inheritance just to get polymorphism. 
* If there is no ‘is a’ relationship and all you want is polymorphism then use interface inheritance with composition, which gives you code reuse (Refer Q10 in Java section for interface inheritance)

### Select a pattern and discuss it’s potential use, advantages and disadvantages (Medium)

### Name 2 bad smells that the DTO pattern exhibits	(Medium/Hard)
* "Shotgun change" smell – a small change to some system requirement (e.g. a field on a UI) requires knock-on changes to multiple classes
* "Parallel class heirachy" smell – 2 different class heirachies contain similar classes in a one-to-one correspondence

### Which design pattern is illustrated by the implementation of the Java I/O classes? (Hard)
* Decorator
* A buffered reader is just a decorator for reader

### Have you heard of the enterprise integration patterns. If so name one.(Medium)

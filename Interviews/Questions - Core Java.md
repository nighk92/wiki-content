## Questions - Core Java

### What is the latest version of Java you have used? If they haven't used it commercially, what about side projects?  
* Latest version is Java 11 (as of 2019)

### What are the features you like in the latest (or next if there's one coming soon) version of Java (Easy)
* Less-commonly known: Reactive capabilities of Java 9, private interface methods, multi-version JARs. 
* More commonly-known: Modularisation of Java 9, REPL, new Collection factory methods. 
* Poor: There's a Java 9???!!**

### What are the differences between C++ and Java? (probably only worth asking if they have C++ on their CV) (Medium)
* C++ has pointers, Java has object references. 
* C++ has full support for multiple inheritance, Java does not (as it's usually more trouble than it's worth). Bonus points for knowing about default methods in interfaces. 
* C++ requires explicit memory management, Java uses garbage collection. 
* C++ has destructors which are called when memory is de-allocated, Java has finalizers that are called when the memory is garbage collected (and therefore not predictable). 
* Java does not include structs or unions. 
* All Java code is encapsulated in classes and therefore does not have global variables or functions. 

### What is the difference between an interface and an abstract class? (Easy)
* An interface cannot declare fields but an abstract class can. Interfaces can only declare constants. 
* An interface cannot declare constructors. 
* All methods in an interface are implicitly public and do not need to be declared as such. 
* Generally an interface declares all abstract methods and provides few (or no) default behaviours but as of Java 8 they can include these. In prior versions of Java interfaces only included abstract methods 

### What does static mean? (Easy)
* Static means one per declaration, not one for each object no matter how many instance of a class might exist. 
* You can use them without creating an instance of a class. 
* Static methods are implicitly final, because overriding is done based on the type of the object, and static methods are attached to a class, not an object. 
* A static method in a superclass can be shadowed by another static method in a subclass, as long as the original method was not declared final. However, you can't override a static method with a nonstatic method. In other words, you can't change a static method into an instance method in a subclass. 

### What is final? (Easy)
* A final field of variable has a fixed value and must be initialised immediately (or in the constructor for fields). 
* An object that is referenced by the variable is mutable, however. 
* A final method cannot be overridden in a sub-class. 
* A final class cannot have sub-classes. 
* Constants are declared static final. 

### What does immutable mean? (Easy)
* An immutable object is an object whose state cannot be modified after it is created. 
* This can be achieved by making a primitive final, or on POJOs only have a constructor and get methods and no public fields. 
* Some languages/frameworks add explicit ways to declare immutable classes (e.g. Groovy's @Immutable annotation).

### What are the advantages of immutable classes? (Medium)
* The lack of side-effects on object operations removes the need to provide synchronisation between multiple threads/processes allowing for greater scalability. This is a key feature of many functional programming languages. 

### What are checked and un-checked exceptions? (Easy)
* Checked Exceptions
    * Are some subclass of Exception (or Exception itself), excluding class RuntimeException. 
    * Making an exception checked forces client programmers to deal with the possibility that the exception will be thrown. eg, IOException thrown by java.io.FileInputStream's read() method
* Unchecked exceptions
    * Are RuntimeExceptions and any of its subclasses. Class Error and its subclasses also are unchecked. 
    * With an unchecked exception the compiler doesn't force client programmers either to catch the exception or declare it in a throws clause. In fact, client programmers may not even know that the exception could be thrown. eg, StringIndexOutOfBoundsException thrown by String's charAt() method
    * Checked exceptions must be caught at compile time. Runtime exceptions do not need to be. Errors often cannot be. 

### What are overloading and overriding? (Easy)
* When a class defines a method using the same name, return type, and arguments (in the same order) as a method in one of its superclasses, the method in the class overrides the method in the superclass. 
* At runtime, the type of the object the method is being called on determines the method which is called, not the type of the reference to the object. 
* When a class declares two methods with the same name but different argument types then those methods are said to be "overloaded". 
* Java imposes restrictions on what can be overloaded (e.g. two methods with the same argument list but different return types cannot be declared in the same class).  

### What is a stateless class? (Easy)
* A Stateless object is an instance of a class without instance fields (instance variables). 
* The class may have fields, but they are compile-time constants (static final). 
* Classes can also be Stateless if it has some fields, but their values are known and don’t change.

### What are pointers/references? (Easy)
* A pointer or reference is a variable that stores a memory address which points at a value, as opposed to the value itself.  

### What is the difference between StringBuilder and String? (Easy)
* String is an immutable class whereas StringBuilder is not. When appending to a String, a new Object must be created and the data copied each time making it inefficient for this purpose. 
* StringBuilder allows efficient appending by only copying the data when it is finally converted using the toString() method.  

### What is the difference between StringBuilder and StringBuffer? (Medium)
* StringBuffer is thread-safe
* StringBuilder is not 

### Describe syncronization with respect to multi threading (Medium)
* With respect to multithreading, synchronization is the capability to control the access of multiple threads to shared resources. 
* Without synchonization, it is possible for one thread to modify a shared variable while another thread is in the process of using or updating same shared variable. This usually leads to significant errors.  

### What is Garbage collection? (Easy)
* Garbage collection is a background process by which Java programs perform automatic memory management. 
* The garbage collector finds unused objects and deletes them to free up memory. 

## Questions - Clean Code & Good Engineering

### Describe the process that you go through to get X	  

### How do you ensure code is maintainable? (Easy) 

### What do the SOLID principles stand for? Can you give me an example of using them? (Medium) 
* **Single responsibility principle** - A class should have only a single responsibility. Only changes to one part of the software's specification should be able to affect the specification of the class. 
* **Open–closed principle** - Software entities should be open for extension, but closed for modification. 
* **Liskov substitution principle** - Objects in a program should be replaceable with instances of their sub-types without altering the correctness of that program. 
* **Interface segregation principle** - Many client-specific interfaces are better than one general-purpose interface. 
* **Dependency inversion principle** - One should depend upon abstractions, not concretions  Medium 

### What do you look for in a code review? (Easy) 

### What does a good API look like? (Easy) 

### What does good code look like?

### What is clean code? (Easy)
* Easy to read
* Well structured
* Maintainable
* Easy to change
* Well named
* Follows SOLID principles 

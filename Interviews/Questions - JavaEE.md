## Questions - JavaEE

### What is a session bean?	(Easy)

### What are the different kinds of enterprise beans? (Medium)
* Stateless session bean - An instance of these non-persistent EJBs provides a service without storing an interaction or conversation state between methods. Any instance can be used for any client.
* Stateful session bean - An instance of these non-persistent EJBs maintains state across methods and transactions. Each instance is associated with a particular client.
* Entity bean - An instance of these persistent EJBs represents an object view of the data, usually rows in a database. They have a primary key as a unique identifier. Entity bean persistence can be either container-managed or bean-managed. Now called simply "entities" in EJB 3.0. They are now not remotable, can be instantiated with "new" and are attached/detached/reatached to persietent storage using JPA.
* Message-driven bean - An instance of these EJBs is integrated with the Java Message Service (JMS) to provide the ability for message-driven beans to act as a standard JMS message consumer and perform asynchronous processing between the server and the JMS message producer.

### What does the JEE spec say about threads (Medium)
* Do not create them

### Which application servers have you used? (Easy)
* JBoss, Wildfly, Tomcat

### What does @Inject do? (Easy)
* Provides dependency injection, injecting the specified bean into the current class.
* It can be used on Constructors, Methods or Class Variables.
* Similar to Spring's @Autowired.

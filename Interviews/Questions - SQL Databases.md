## Questions - SQL Databases

### How might you improve the performance when searching over a large set of data? (Easy)
* Apply indexes
* Use LIMIT and GROUP BY 

### How would you implement caching and what are the considerations you would have to give to it? (Medium/Hard)
* Caching frameworks (often key/value stores)
* Memcached
* redis
* EHCache / C3PO
* Spring Cache
* Read cache or write-through cache
* Memory usage/eviction
* Cache warming/pre-seeding
* Row cache vs query caching

### What are the different types of join, and what are each of them used for? (Easy)
* INNER JOIN - Return rows that have at least one match in both tables
* FULL JOIN / FULL OUTER JOIN - Return all rows where there is a match in one of the tables
* LEFT JOIN - Return all rows from the LEFT table, and matched rows from the RIGHT table
* RIGHT JOIN - Return all rows from the RIGHT table, and matched rows from the LEFT table

### What are the transaction isolation levels? (Easy)
* If they don't know the levels, half a point for knowing about dirty reads, row/table locks or optimistic locking
* Read uncommitted - A transaction can see updates from other transactions in progress before they have been committed
* Read committed - A transaction will see changes that are committed by other transactions
* Repeatable read - A transaction will not be affected by data updates from other transactions but may see additional rows being added by them
* Serializable - A transaction's view of the database state cannot be affected by any other transaction

### What does ACID stand for. What does each term mean?	(Easy)
* Atomic - Either an operation completes entirely or fails entirely. On failure, none of the sub-operations are persisted.
* Consistency - After an operation is committed, all constraints specified on the database must hold true
* Isolation - When transactions operate concurrently the outcome must be identical to that has they run sequentially
* Durability - When a transaction complete, the final state will remain even in the event of application or system failure

### What is an ORM? When would you use one? (Medium)
* An ORM maps table/view structures to Java classes and automatically reads database data into objects and persists objects into table rows.
* ORMs were exclusively used for relational databases but some now have the capability to work with other models
* Use when there are complex tables and relationships between tables.
* Use in new projects where data structure will not cause unnecessary complexity in the mapping and the data model can be defined based on the object model
* To provide a layer of abstraction that will allow you to switch database vendors

### What is CAP theorem? (Medium) Which two of these apply to relational databases? (Medium)
* Consistency - (see ACID)
* Availability - (see Durability in ACID)
* Partition-tolerant - Allows data to be highly distributed
* In theory a database system can only be two of these things
* RDBMS are generally offer high availability and constitency at the cost of partition-tolerance

### What is XA or two-phase commit? (Medium)
* Two phase commit is a transaction completion process that requires a commit request and confirmation step for each resource involved
* Unlike a single-phase commit, this allows a single transaction to co-ordinate multiple resources (e.g. queue-to-queue or queue-to-database) in a single atomic operation

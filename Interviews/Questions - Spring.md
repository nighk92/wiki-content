## Questions - Spring

### What is the default embedded container used by Spring Boot? Can this be changed? (Easy)
* Tomcat is the default
* By manipulating external dependencies (e.g. through Maven exclusions), this can be replaced with Jetty or Undertow or different versions of Tomcat

### What are the main advantages of using Spring Data JPA? (Easy)
* Spring Data JPA provides out-of-the box CRUD operation support for any entity without writing boilerplate implementations
* By using a method naming convention it is possible to have Spring automatically implement more complex query behaviour simply by declaring methods in a repository interface (e.g. findByNameContainsAndDateBeforeOrderByDateDesc())

### What is a Spring Boot Starter? (Easy)
* Starters are collections of external dependencies that are known to work together to provide a specific area of functionality or specific technology to an application.

### What Spring Boot Starters are available? (Easy)	
* Web
* Camel
* Data-JPA
* ActiveMQ
* Actuator
* Test
* Mail
* Batch
* Cache
* (many many more)

### What does the @Autowired annotation do?	(Easy)
* Provides dependency injection, injecting the specified bean into the current class. 
* It can be used on Constructors, Methods or Class Variables.
* Similar to JavaEE's @Inject.

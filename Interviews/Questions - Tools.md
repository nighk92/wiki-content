## Questions - Tools

### What's your favourite IDE and why? (Easy)
* Eclipse
* IntelliJ
* VS Code

### What version control system do you use for your code? (Easy)
* Git
* Subversion

### What branching strategies have you used with git? (Easy/Medium)
* Expect them to mention having a Master, Develop and Feature branches, perhaps with a release branch.
* +1 for mentioning standard strategies like Git Flow

### Git - what's the difference between a merge and rebase?	(Medium)

### What is Maven? (Easy)
* A Java based build tool
* It relies on convention over configuration and explicit dependency management

### Maven or Gradle? (Medium)

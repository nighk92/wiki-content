## Outline For Face-to-face Interview

Tell the candidate the interview will take the following structure:

* Overview of the Company (10 mins)
* Talk through their CV and background (15 mins)
* Ask some technical questions (20 mins)
* Scenario (30 mins)
* Questions about scenario (10 mins)
* Time for them to ask questions (5 mins)

Total time = 1 hour 30 mins
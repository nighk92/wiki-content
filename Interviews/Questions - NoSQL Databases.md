## Questions - NoSQL Databases

### What is a NoSQL Database? (Easy)

### What are the different types of NoSQL database? Name a NoSQL Database and tell me how they differ and what you would use them for? (Easy)
* Mongo - Document database
* Cassandra - Column database
* HBase - Column database
* Neo4J - Graph database
* Redis - Key/value store

### What's the advantage of NoSQL? What's some of the disadvantages of NoSQL? (Easy/Medium)
* Highly scalable and resilient (e.g. Hadoop data sharding/replication)
* Optimised for batch processing/analysis (e.g. with Hadoop tech such as MapR and Spark)
* Optimised for specific storage/query types (e.g. high volume writes/occasional reads or graph queries)
* Often unstructured/schema-less (could be a pro or a con)
* They are rarely transactional or provide consistency constraints
* Sometimes asynchronous (e.g. a read immediately following a write may fail)

## Questions - Problem Solving & Debugging

### How would you go about identifying performance problems and/or memory leaks in your Java application? (Medium)

### How would you go about minimizing memory leaks in your Java/J2EE application? (Medium)

### How would you go about identifying any potential thread-safety issues in your Java/J2EE App? (Medium)

### How would you improve performance of a Java application? (Medium)
* Pool valuable resources e.g. sockets, db connections etc.
* Minimise network overhead - roundtrips, moving unnecessary data.
* Lazy loading or eager loading

### How would you go about identifying any potential transactional issues in your Java/J2EE? (Medium)

## Questions - Angular

### What versions of Angular have you used? (Easy)
* Newest version is 7 (as of 2019)

### How does Angular (version 2+) differ from AngularJS (version 1)? (Easy/Medium)
* Angular was a ground-up rewrite of AngularJS
* Angular does not have a concept of "scope" or controllers
* It's architecture encourages the use of a hierarchy of components/modules, much of the core functionality has moved to modules
* Angular recommends the use of Microsoft's TypeScript language
* Has its own suite of UI components called Angular Material

### What is dependency injection, and how does it work? (Easy/Medium)
* It deals with how components get hold of their dependencies. 
* You can use it when defining components or when providing run and configblocks for a module. 
* The AngularJS injector subsystem is in charge of creating components, resolving their dependencies, and providing them to other components as requested.

### What is a directive? (Easy)
* Directives are JavaScript functions that manipulate and add behaviors to HTML DOM elements 

### What are some of the standard directives Angular provides? (Eaasy)
* The ng-app directive initializes an AngularJS application. 
* The ng-init directive initializes application data. 
* The ng-model directive binds the value of HTML controls (input, select, textarea) to application data.

### What is a component? (Easy)
* It is a simplified version of a directive, which is optimised for a component-based architecture. 
* They are used to create a web-component on a page, with HTML and view logic bound to the HTML.

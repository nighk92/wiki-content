## Questions - Process & Agile

### Agile - what does it mean to you (Easy)
* -1 if it means having a standup and months of project are just called iterations or sprints.
* +1 is they start talking about a way of working, Scrum, XP etc

### Scrum (Easy/Medium)
* What is it?
* What's a backlog?
* Who decides what you work on?
* How do the teams plan their work?
* How do multiple teams work together?
* What's a burn down chart?
* How do you estimate?
* What if you have too much work?

### Extreme Programming (XP) - what is it? what did you use it for? can you explain it to me?	(Medium)

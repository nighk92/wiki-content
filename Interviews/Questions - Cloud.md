## Questions - Cloud

### What are the differences between PaaS and IaaS? (Easy)
* IaaS or Infrastructure as a Service (e.g. AWS's EC2) provides virtual compute resource (hardware and networking) that can be highly customised by customers. 
* PaaS or Platform as a Service (e.g. OpenShift) provides a runtime on which to execute code while providing an opinionated infrastructure on which it executes

### What is SaSS? (Easy)
* Software as a service - online services that can be configured and run within the providers environment e.g. Payment services are often SAAS. 

### AWS Specific - What is a VPC? (Medium)
* A virtual private cluster is a restricted sub-network within AWS that allows a closed set of EC2 instances to communicate privately 

### AWS Specific - How would you protect an API endpoint with an IP address whitelist? (Medium)
* NACLs (Network Access Control Lists) allow controlled access to a VPC
* ELBs (Elastic Load Balancers) can have IP address whitelists associated through security groups 

### What's the difference between block and object storage? When would you use each one? (Medium)
* Block storage can be attached to a virtual machine to emulate the capabilities of a HDD or SSD. They have bounded size. 
* Block storage would be used for any file systems attached to a virtual machine. 
* Object storage allows simple pushing and pulling of individual file objects on demand. They can be unbounded in size.
* Object storage would be used for the persistence of individual (possibly very large) files such as videos 

### What is the purpose of Terraform? (Medium)
* Terraform is "infrastructure as code" and allows repeatable provisioning of cloud infrastructure such as networks/VPCs, compute instances, load balancers etc. 

### What is an API gateway? (Medium)
* An API gateway is a gateway, it allows clients to access a single endpoint for API calls and distributes each of those API calls to the relevant microservice 

### What is "serverless"? (Easy)
* A serverless service is one that does not run continuously but rather is managed by the platform and triggered only when it has data to ingest or is called by a client. 

### What are the advantages compared to standard services/microservices? What are the disadvantages?  (Medium)
* Advantages
    * Cost saving (since the service is only running when needed)
    * Forcing people to think in a scalable/functional way. 
* Disadvantages
    * Latency since a service may not respond for a number of seconds. For this reason, heavyweight container technology (e.g. Spring Boot) that has a lot of startup overhead is not recommended for such systems. 

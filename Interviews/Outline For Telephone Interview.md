## Outline For Telephone Interview

Tell the candidate the interview will take the following structure:

* Overview of the Company (10 mins)
* Talk through their CV and background (10 mins)
* Ask some technical questions (20 mins)
* Time for them to ask questions (5 mins)

Total time = 45 mins
## Questions - Core Strengths

### Do you have team leadership experience?   

### Do you participate in the wider software engineering community? Have you read the engineering blog?  
* Open source contribution (commits/bug reports), blog posts, conferences (esp. presenting), meetups, hackathons  

### What are your long-term career aspirations? Team leadership? Architecture?   

### What do you most enjoy on a project and why?   

### What is your favourite programming language?   

### What sized teams have you worked in?   

### What skills would you like to improve or what new skills would you like to learn?
* Good opportunity to tell them about training budgets. 

### What was the last training you did? Why did you do this? Do you think it was effective? Why (or not)?   

### What's your favourite role on a project and why?
* Try and get an understanding of what they want to do 

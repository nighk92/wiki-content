## Overview
A common issue with an LED screen after it had been dropped or damaged is that the backlight/inverter becomes dislodged.

## Symptoms
The screen will freeze and eventually turn white, when pressing on the bottom of the screen the picture comes back.

## Fixes
The best fix is to replace the screen. However it is also possible to get something to press on the bottom of the screen permanently to solve the issue e.g. a clothes peg
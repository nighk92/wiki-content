## Overview
The Screen is a core part of the computer. It is the main output for a personal computer, and greatly improves the usability of the computer. There are various types of screen including:

* Cathode Ray Tube (CRT) - an outdated screen technology using vacuum tubes containing electron guns to produce an image 
* Liquid Crystal Display (LCD) - a screenn technology using the light modulating properties of liquid crystals 
* Light Emitting Diode (LED) - A screen technology using lots of small lights (as pixels) to create an image 

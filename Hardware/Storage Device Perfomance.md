## Overview
The following table shows the performance of various storage devices. The performance benchmarks are produced using CrystalDiskMark. 

* All figures are in MB/s

| Storage Device | Seq (Q32T1) | 4KiB (Q8T8) | 4KiB (Q32T1) | 4KiB (Q1T1) |
| --- | --- | --- | --- | --- |
| Apple MacBook Pro "Core i7" 2.5 15" Mid-2015 (DG) | Read: 2173.5 Write 1586.1 | Read: 665.5 Write: 394.1 | Read: 416.4 Write: 332.2 | Read: 26.7 Write: 111.1 |
| Sandisk Ultra SDXC Micro SD Class 10 (64GB) | Read: 78.04 Write: 36.95 | Read: 6.65 Write: 2.60 | Read: 7.81 Write: 2.71 | Read: 5.41 Write: 2.54 | 
| Transcend Premium Micro SDHC Class 10 UHSI 300x (32GB) | Read: 70.25 Write: 19.01 | Read: 6.51 Write: 0.79 | Read: 6.65 Write: 0.59 | Read: 5.23 Write: 0.67 |
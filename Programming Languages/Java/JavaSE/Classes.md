[TOC]

## Overview
A class is a blueprint from which objects can be created. A class must have the same name as the file it is in. It is possible to have subclasses within a class, but you can only have one class within a file. Classes don’t have to contain a “main” method. 

The [Objects page](Objects.md) explains how to use a class.

## Create a Class
The `class` keyword is used to make a class. The code of a class is surrounded by a pair of curly brackets. The same syntax is used to create a subclass, which are classes within a class. You can have many subclasses within a class.
```java
public class FILE_NAME
{ 
    CODE...

    public class SUBCLASS_NAME
    {
	    CODE...
    }
}
```

## Constructors
Every class must have a constructor, so objects can be created from it. Constructors must have the same name as the class, and have no return type not even void! The body of a constructor is like a method, and can contain any valid Java code.

The simplest type is a no-argument constructor. This is added to the class by default if you don’t write your own constructor:
```java
public class FILE_NAME
{ 
    public FILE_NAME()
    {
    	CODE...
    }
}
```

Constructors can also take in arguments/parameters which can be used to set variables or change the configuration of the object:
```java
public class FILE_NAME
{ 
    public FILE_NAME(int int1In, int int2In)
    {
        int sum = int1 + int2;
    }
}
```
You can have more than one constructor as long as they take in different parameters. All of them have to have the same name as the Class.

## Adding Variables & Methods
A class can have variables and methods, every object made from this class will have these properties. 

Instance variables are specific to each object, so each object can have different values and store different data within the class blueprint. They are declared at the top of the class, before the constructor and any methods. When declaring a variable you must specify an [Access Modifier](Access Modifiers.md), if you don’t then it will default to `package` access. Instance variables can be made using any Java type/object and can even be another class that you have made!

[Methods](Methods.md) can be added to access the classes variables and do object specific tasks.
```java
public class FILE_NAME
{
	// Instance variables
	public String testString;
	private int testInt;
	
	public FILE_NAME(String testStringIn, int testIntIn)
	{
		// Set variables in constructor
		testString = testStringIn;
		testInt = TestIntIn;
	}

	// Methods
    public int getTestString()
	{
		return testString;
	}

	public void setTestString(int testStringIn)
	{
		testString = testStringIn;
	}

	public int getTestInt()
	{
		return testInt;
	}

	public void setTestInt(int testIntIn)
	{
		testInt = testIntIn;
	}
}
```

## Initialising Global/Static Variables
Setting a static variable means there is only one of that variable that is used by every instance of that class. To make the variable global it must also be made public, so it can be accessed outside of the class. If the value is set at the top of the class then it is only assigned when the program is first compiled, not every time a new object is made.
```java
public class FILE_NAME
{ 
	public final static String test = "hello";
}
```

Setting the value in the constructor means that it would be set every time a new object is made, beware this would affect all current objects as they all use the same value.
```java
public class FILE_NAME
{ 
	public static String test;

	public FILE_NAME()
	{
		test = "hello";
	}
}
```
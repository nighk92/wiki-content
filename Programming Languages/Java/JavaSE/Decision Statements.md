[TOC]

## Overview
All programs need to make decisions based on variable values. There are several statements which can be used for this including if-else and switch.

## IF-ELSE Statements
IF statements take in a Boolean condition, and the code inside is only executed if that condition is True.
```java
if(1 == 1)
{
	CODE
}
```

IF statements can be made using multiple Boolean conditions, and using different Boolean operators
```java
if(a == b && b == c || a == c)
{
	CODE
}
```

IF-ELSE statements can be used together. If the first IF statement is True then the other statements are skipped, the second ELSE-IF statement is only checked if the first IF statement is false, and the final ELSE statement is only executed if the first two statements are False. IF-ELSE statements can include as many ELSE-IF statements as you like, or none at all. Both the ELSE-IF and ELSE statements are optional.
```java
if(a == b)
{
	CODE
}
else if(a == c)
{
	CODE
}
else
{
	CODE
}
```
 
## Switch
The SWITCH statement is like an IF-ELSE statement. It can be used with all variables including Strings, but not objects.

In the example below if the number is 1, then the code in case 1 is executed, the rest of the statement isn’t run. If the number is 2 then the code in case 2 is executed. If the number is something else the `default` case is executed (like an ELSE statement). The `break` keyword is required at the end of each case ensures the below instructions are skipped if a case is true. The default case doesn’t need a `break`.
```java
int number = Scanner.nextInt();
switch(number)
{
	case 1:
		CODE
		break;
	case 2:
		CODE
		break;
	default:
		CODE
}
```

The `break` statements can be removed if you don’t wish to skip instructions. In the example if the code for case 1 & 2 was the same, then the `break` statement can be removed from case 1 to stop replication of code. So here if `number` is 1 or 2 then "case 2" will be printed. 
```java
int number = Scanner.nextInt();
switch(number)
{
	case 1:
	case 2:
		System.out.println("case 2");
		break;
	default:
		System.out.println("default");
}
```

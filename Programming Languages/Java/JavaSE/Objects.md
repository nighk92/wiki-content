[TOC]

## Overview
Objects are made (instantiated) from a Class. The methods created in a Class can be executed on an Object made from it.

## Creating objects
Objects are created using the new keyword.
```java
CLASS_NAME OBJECT_NAME = new CLASS_NAME(PARAMETERS);
```

The following creates an object from the Robot class, passing it a name.
```java
Robot myRobot = new Robot("My Robot");
```

## Using Objects
Once an object is created the variables/methods from the Class can be used. Use a **full stop** after the object name to access the variables/methods.

The following calls the **getName** method from the Robot class on the newly created object:
```java
myRobot.getName();
```

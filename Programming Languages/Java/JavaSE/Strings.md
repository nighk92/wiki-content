[TOC]

## Overview
A `String` is an object that can contain a sequence of characters. A `String` in Java is immutable meaning that it can;t be changed once it is created, instead to modify it a new `String` must be created with the updated value.

## Useful functions
Use the equals method on strings instead of == to compare for equality. As Strings are technically objects made from the String class, and not an inbuilt data type. What a String object actually contains is a pointer to the location of the String in memory, not the String itself. Using the == method compares memory locations not values. The Strings equals method compares the actual value of the String.
```
firstString.equals(secondString);
```

The toString method converts any object to a String. The default version of this just prints out the type of the object and it’s memory location. It can be overridden in a class to provide more useful information. When you call System.out.print(object) the toString method is automatically called on the object to print it out to the console.
```java
variable.toString();
```

The Integer class contains a parseInt method which can convert any String to an int.
```java
Integer.parseInt(stringVariable)
```

## String Escape Characters
Put a backslash before any text in a string to use an escape character:

* \t – prints a tab
* \n – prints a new line
* \\\\ – prints a normal backslash
* \" – prints a speech mark 

[TOC]

## Overview
Variables are used to store data, there are different variables to store different types of data.

## Primitive Data Types
| Type | Use | Range of values | Size |
| --- | --- | --- | --- |
| byte | Very small integers | -128 to 127 | 8 bits |
| short | Small Integers | -32,768 to 32,767 | 16 bits |
| int | Big Integers | -2,147,483,648 to 2,147,483,647 | 32 bits |
| long | Very big integers | -263 to 263-1 | 64 bits |
| float | Decimal numbers | +- 1.4x10-45 to 3.4x1038 | 32 bits |
| double | Big Decimal numbers | +- 4.9x10-324 to 1.8x10308 | 64 bits |
| char | Characters, which are surrounded by single quotes like ‘a’ | Unicode character set | 16 bits |
| String | Words & Sentences, which are surrounded by double quotes like “my string”. String is actually a class so starts with a capital S | Unicode character set | 16 bits per character |

## Declaring Variables
A variable name must be declared before it can be used. Variables are declared as `DataType NAME` e.g.
```java
int score; // Makes an integer variable called score
int one, two, three; // Makes 3 integers called one, two and three
```

## Using Variables
```java
int score;
score = 5; // Assigns the value of 5 to score
String sentence = “this is a sentence”; // This creates a String called sentence, and assigns the String of text to it all in one line
```
 
## Making a 
The `final` keyword can be used to make a variable a constant, which means it’s value can never be changed. The name of a constant is written in capitals with underscores to separate words.
```java
final char MY_LETTER = 'a';
```

## Variable Names Not Allowed
* Java Keywords e.g. int
* Literals e.g. true, false, null
* Reserved words e.g. break, return
* Names must begin with a letter, underscore or dollar sign

## Type Casting
Java variables can be converted to a different types throughout a program. e.g.

The data type is put in brackets before the value. This forces the value to be converted to that data type, although some data may be lost.
```java
variable = (dataType) value
```

A double can’t normally be assigned to an int. but with type casting it will be. The value assigned to the int isn’t rounded, the decimal part is just cut off. So 3.72 would become just 3. Alternatively in this case `math.round()` can be used to round numbers correctly.
```java
int n = (int) 3.72;
```

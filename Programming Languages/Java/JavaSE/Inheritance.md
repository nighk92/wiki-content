[TOC]

## Overview
Inheritance is a core part of Object-Oriented programming. Classes can inherit variables and functions from other classes, so allowing for efficient reuse of code and functions.

## Extends
A **child** class that `extends` a **parent** class inherits all of the parent’s variables and functions, unless they are private. This is done by adding the `extends keyword` to the class heading E.g.
```java
public class Parent
{
	public int parentInt;

	public void parentMethod()
	{
		System.out.println(“Parent”);
	}
}

public class Child extends Parent
{
	public child()
	{
		parentInt = 5;
	}
}

Child childObject = new Child();
System.out.println(childObject.parentInt); // prints “5”
childObject.parentMethod(); // prints “Parent”
```

## Overriding Methods
A child class doesn’t just have to inherit all the default functionality of a parent class. It can override methods to provide more specific functionality. To override a method in the child class write a method with the same name, return type and parameters.

E.g. Change the child class above to the following:
```java
public class Child extends Parent
{
	public void parentMethod()
	{
		System.out.println(“Child”);
	}
}

Child childObject = new Child();
childObject.parentMethod(); // now prints “Child”
```

## Overriding Method Rules
The following table summarizes what happens when you define a method with the same signature as a method in a superclass:

| | **Superclass Instance Method** | **Superclass Static Method** |
| --- | --- | --- |
| **Subclass Instance Method** | Overrides | Generates a compile-time error |
| **Subclass Static Method** | Generates a compile-time error | Hides |

## Overriding (hiding) Variables
A child class can also hide variables in the parent class, however this is generally discouraged as it can lead to confusing code. However if there is a need to do this then create a variable of any type with the same name as the one in the parent class. E.g.
```java
public class Child
{
	public String parentInt;
	
	public child()
	{
		parentInt = “Hello”;
	}
}

Child childObject = new Child();
System.out.println(childObject.parentInt); // now prints “hello”
```

## Super
The super keyword can be used to access the parent class constructor. It can also be used to access variables/methods in the super class which have been overridden by the child class.

To access the parent constructor from within the child classes constructor just use:
```java
super(<arguments>) // This must be the first line in the child constructor.
```

To access non-private variables/methods in the parent class, from anywhere within the child class use:
```java
super.parentMethod(<arguments>)

super.parentVariable
```
[TOC]

## Overview
Modifiers can be used in Java to change the way a class/method/variable can be used.

## Access Modifiers
Are used to restrict the use of your class/method/variable outside of the class they are made in.

| Modifier | Applies to | Description |
| --- | --- | --- |
| Public | Class, Inner class, Method, Class Variable | The item can be seen by anything |
| Protected | Class, Inner class, Method, Class Variable | The item can be seen within the current package, and by any subclasses outside of the package |
| Package (Default) | Class, Inner class, Method, Class Variable | The item can only be seen within the current package |
| Private | Class, Inner class, Method, Class Variable | The item can only be seen within the current class |

## Additional Modifiers
Are used to add specific constraints to how a class/method/variable can be used.

| Modifier | Applies to | Description |
| --- | --- | --- |
| Static | Inner class, Method, Class Variable | This means there is only ever one version. For an **Inner class/method** it means they can be called without first making an object of the parent class. E.g. Integer.parseInt() can be called without creating an Integer object first. For a **variable** it means there is one value for all object in a class, so if the value is changed on one object it also changes for all other objects of that class. |
| Final | Class, Inner class, Method, Class Variable, Method Parameter | Final **classes** can’t be inherited from. Final **methods** can’t be overridden in subclasses. Final **variables** can’t have their values changed after initialisation, so they are constants. |
| Abstract | Class, Method | Abstract classes contain unimplemented methods, can can’t be instantiated. Abstract methods have no body, and must be overridden in a subclass to provide functionality. |
 

## Overview
Two or more processes that run at the same time are called concurrent processes. This can be done in Java by making Threads. Each separate task performed by a program is a Thread.

## Thread State Transitions
A thread can be in many different states:

* **New** – not yet started
* **Runnable** – executing code
* **Blocked** – waiting for a monitor lock
* **Waiting** – waiting for another thread to finish and notify the current thread
* **Time-Waiting (Sleeping)** - waiting for a specific period of time
* **Terminated** – finished executing

### This is how a thread transitions between states:
![Thread Lifecycle](.images/Thread_Lifecycle.png)
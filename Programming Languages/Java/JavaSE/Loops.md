[TOC]

## Overview
Looping allows the same section of code to be executed many times over.

## For Loop
Loops over a section of code For a specified number of times. 

This is the general syntax for a For loop. The loop has a counter, which is incremented/decremented on every loop, the counter is compared against the condition on every loop, and the loop continues until the condition is False.
```java
for(initiate counter; condition; increment counter)
```

This loop prints out hello 10 times.
```java
for(int i=0; i<10; i++)
{
	System.out.println(“hello”);
}
```

The condition of a loop can also be a variable, one of the most common uses of loops is to process arrays. The above loop uses the array.length function in the condition, so it will loop over all elements of the array. It then prints them out using the counter “i" to get the next element of the array on each loop.
```java
int[5] intArray = new int[5];
for(int i=0; i<intArray.length; i++)
{
	System.out.println(intArray[i]);
}
```

## While Loop
Loops over a section of code While a condition is true. So unlike the For loop you don’t specify how many times a While loop will run.

This is the general syntax for a While loop. All it needs is a condition to test on every loop.
```java
while(condition)
```

The below loop will continue until myInt1 is more than myInt2.
```java
int myInt1 = 0;
int myInt2 = 5;
while(myInt1 <= myInt2)
{
	myInt1 ++;
	myInt2 --;
}
```

Using `true` as the condition for a While loop will make an infinite loop, this can be useful but be careful as it can also cause a program to crash!

```java
while(true)
```

## Do… While Loop
Like the While loop, but the code is guaranteed to execute at least once.

The general syntax is made of two sections Do & While. The code in the Do section is executed first then the condition is checked second. So on the first loop the code is executed before the condition is checked, hence why the code is guaranteed to run at least once.
```java
do
{
	// CODE
}
while(condition);
```

The Do-While loop is useful for taking in user input, and running some validation. The user will always be asked for input at least once, and if the input isn’t valid then keep looping until the user enters in something valid.
```java
int mark;
do
{
	mark = IO.readInt("Enter a mark between 1 and 100");
}
while(mark < 1 && mark > 100);
```

## Break
This keyword can be used to exit the loop being executed.

The below loop only runs until i equals 5, then it “breaks” out and execution continues onto the code after the loop. `break` is also used in a Switch statement, to exit the Switch after a True condition is found.
```java
for(int i=0; i<10; i++)
{
	if(i == 5)
	{
		break;
	}
	System.out.println(i);
}
```

## Continue
This keyword skips the current iteration of a loop, and continues onto the next iteration.

This loop only prints out odd numbers, if a number is even then the `continue` statement skips the current iteration.
```java
for(int i=0; i<10; i++)
{
	if(i%2 == 0)
	{
		continue;
	}
	System.out.println(i);
}
```

## Code Sections
You can name a section of code which you can reference in a `break` statement to break out of multiple nested loops. In this case the break in the inner loop will actually break out of the outer loop as well.
```java
mysection:
for
{
	for
	{
		break mysection;
	{
}
```

For eaxmple the following code prints out:
```
I = 0
J = 0
J = 1
J = 2
J = 3
done
```

```java
public class Main 
{
	public static void main(String[] args)
	{
		mysection:
		for(int i=0; i<5; i++)
		{
			System.out.println("I = " + i);
			
			for(int j=0; j<5; j++)
			{
				System.out.println("J = " + j);
				if(j == 3) {
					break mysection;
				}
			}
			
			System.out.println("This never gets printed :-(");
		}
	
		System.out.println("done");
	}
}
```
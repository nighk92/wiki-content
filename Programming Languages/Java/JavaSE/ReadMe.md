## Overview
JavaSE (Java Standard Edition) provides a platform for the development and deployment of programs to personal computers, servers or mobile devices. It consists of all the standard Java features, API's and its core functionality such as the:

* Java Language Specification
* Java Class Library
* Java Virtual Machine (JVM)
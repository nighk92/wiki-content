[TOC]

## Output
Prints out a String text to the command line console
```java
System.out.print("Text to be printed");
```

Prints out text to the command line followed by a new line character
```java
System.out.println("print text then a new line");
```

## Input
Creates a new input object using the scanner class. This takes key input form the command line. The user is prompted to enter an integer which is stored in the variable `number`.
```java
Scanner input = new Scanner(System.in);
System.out.print("Enter a number: ");
int number = scanner.nextInt();
```

## Math class (java.lang.Math)
Returns the first number raised to the power of the second number
```java
Math.pow(double1, double2)
Math.pow(10.0, 2.0) = 100.0
```

Rounds double/float numbers to whole numbers
```java
Math.round(doubleIn);
```

This will round a number to two decimal places, useful for currencies.
```java
Math.round(number*100)/100
```

[TOC]

## Normal Comments
**//** - Double forward slashes are single line comments

**/* … */** - Forward slash and star are multi line comments, and must be closed by another star and forward slash.

## Javadoc Comments
Javadoc can be made automatically for your code, providing you have commented it using the Javadoc notation. Comments should be put at the beginning of a code file, before each class, and before each method.

**/\*\* … */** - Javadoc comments are made using a forward slash and two stars, and must be closed by a star and forward slash

To create Javadoc using Eclipse go to Project -> Generate Javadoc…

## Javadoc Keywords
Additional keywords can be used to give meaningful information.

| Keyword | Meaning | Usage |
| --- | --- | --- |
| @author | States the author of the document | At the beginning of a code file |
| @version | The version number of the program | At the beginning of a code file |
| @since | Date the file was last updated | At the beginning of a code file |
| @exception | Describes an exception that may be thrown | Before a class/method |
| @param | Describes a method parameter | Before a method |
| @return | Describes the return value of a method | Before a method |
| @deprecated | Describes an outdated method | Before a method |
| {@inheritDoc} | Copies the description from an overridden method | Before a method |
| @see | Provides a link to another element of documentation | Anywhere |
| {@link URL} | Link to an external web page | Anywhere |
| HTML tags | All HTML tags can be used to format output. E.g. <b>Bold Text</b> | Anywhere |

## Javadoc examples
This may be put at the beginning of a code file:
```java
/**
 * This file contains the convertToStringClass, which can convert variables to Strings
 *
 * @author Paul Monk
 * @version 1.0.0
 * @since 01/01/2016
*/
```

This may be put before a class:
```java
/**
 * This class converts variables to Strings
 *
 * @exception OMG it throws an exception!
 */
public class convertToStringClass 
{...
```

This may be put before a method:
```java
/**
 * Converts an integer to a String
 *
 * @param it takes in an integer to convert to a string
 * @return it returns a String containing the integer input
 */
public String convertInt(int numberIn) 
{...
```
[TOC]

## Basic Arrays
Arrays are a way to store many values of the same type of variable. Each element in an array is referenced by its position (starting from 0). An Array is a pointer to variables in memory, so be careful when passing arrays into other methods and modifying them as the original array also gets modified.

The following is the syntax to create a 1 dimensional array with a specified size
```java
datatype[] name = new datatype[size_of_array];
```

Elements of an array can be accessed by their position id number (starting from 0). E.g.
```java
int[] myArray = {1,2,3,4,5}; // Creates a 1 dimensional array, with a length of 5, populated with values from the curly braces
System.out.println(myArray[0]); // prints 1
System.out.println(myArray[4]); // prints 5
System.out.println(myArray[5]); // throws ArrayOutOfBoundsException
```

`.length` returns the number of elements in the array
```java
myArray.length // returns 5
```

## Two Dimensional Arrays
These arrays act like a table of data, with rows and columns. These can be processed using nested for loops.

The following is the syntax for creating a 2D array
```java
datatype[] name = new datatype[size1][size2]
```

It is possible to access data from the 2D array using two position id numbers
```java
myArray[0][1]
```

## Processing Arrays
Arrays can be processed using loops. For example the below code This prints all elements in an array. An enhanced for loop can also be used.
```java
for(int i=0; i<= myArray.length; i++)
{
	System.out.println(myArray[i]);
}
```

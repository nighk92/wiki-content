[TOC]

## Overview
The **Error** class in java is a subclass of **Throwable**, and indicates a serious problem has occurred. Applications should not try to catch these errors.

The **Exception** class is also a subclass of **Throwable**, and indicates a problem that an application might and to catch and deal with.

## Types of Error
* **Compile Time Errors** - e.g. a Syntax Error
    * Program code isn’t generated
    * Read error messages to debug
* **Syntax Errors** – e.g. undeclared variables, semi-colon missed
    * Program code isn’t generated
    * Read error messages to debug
* **Run Time Errors** – e.g. null pointer exception
    * Program will crash unless error is caught
    * Read error messages to debug
    * Add print statements into code
    * Use the eclipse debugger
* **Semantic/Logic Errors** – e.g. program does something unexpected
    * Errors in the meaning of the code
    * These errors can be present without the programmer knowing
    * Test to find these errors
    * Add print statements into code
    * Use eclipse debugger

## Checked/Unchecked Exceptions
* `Error` and `RuntimeException` are unchecked
    * Unchecked exceptions are not normally dealt with by the code, if one occurs it often points to a bug in the application.
* All other exceptions are checked
    * Checked exceptions in Java are sometimes expected and must be explicitly dealt with in the code. This can be done by using a `try` `catch` block or using the `throws` keyword.

### Java Exception Hierarchy:
 ![Exception Hierarchy](.images/Exception_Hierarchy.png)
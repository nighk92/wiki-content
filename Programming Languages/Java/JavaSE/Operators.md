[TOC]

## Overview
Operators in Java are used to perform actions on data.

## Arithmetic Operators
These are used to perform actions on 2 or more numbers.

**Assignment (=)**
```java
float decimal = 1;
```

**Addition (+)**
```java
1 + 1 = 2

// Can also be used to concatenate Strings:
"Hi " + "Paul" = "Hi Paul"
```

**Subtraction (-)**
```java
2 - 1 = 1
```

**Multiplication (*)**
```java
2 * 2 = 4
```

**Division (/)**
```java
4 / 2 = 2
```

**Remainder / Modulus (%)**
```java
3 % 2 = 1
```

## Unary Operators
These are used to perform actions on a single number or data type.

**Plus (+)**

Indicates positive value (numbers are positive without this, however)
```java
int result = +1;
// result is now 1
```

**Minus (-)**

Negates a number or expression
```java
int result = 1;
result = -result;
// result is now -1
```

**Logical compliment (!)**

Inverts the value of a boolean
```java
!true = false
```

**Increment (++)**

Increments a value by 1
```java
int result = 1;
result++;
// result is now 2
```

**Decrement (--)**

Decrements a value by 1
```java
int result = 1;
result--;
// result is now 0
```

The increment/decrement operators can be applied before (prefix) or after (postfix) the operand. The code `result++;` and `++result;` will both end in result being incremented by one. The only difference is that the prefix version (`++result`) evaluates to the incremented value, whereas the postfix version (`result++`) evaluates to the original value. For example:
```java
int result = 1;
		
int newResult = result++;
System.out.println(newResult); // prints out 1

result = 1;
newResult = ++result;
System.out.println(newResult); // prints out 2

result = 1;
newResult = result--;
System.out.println(newResult); // prints out 1

result = 1;
newResult = --result;
System.out.println(newResult); // prints out 0
```

## Equality and Relational Operators
All these operations return a boolean (true or false).

**Equal to (==)**
```java
1 == 1 = true
1 == 2 = false
```

**Not equal to (!=)**
```java
1 != 2 = true
```

**Greater than (>)**
```java
1 > 2 = false
```

**Greater than or equal to (>=)**
```java
2 >= 2 = true
```

**Less than (<)**
```java
1 < 2 = true
```

**Less than or equal to (<=)**
```java
1 <= 2 = true
```

## Conditional Operators
All these operators return a boolean (true or false) based on a condition.

**And (&&)**
```java
true && true = true
true && false = false
```

**Or (||)**
```java
true || true = true
true || false = true
```

**Ternary - shorthand if-then-else statement (?:)**
```java
// <condition> ? <if true value> : <if false value>
1 > 2 ?: 1 : 2 = 2
```

## Type Comparison Operator
Used to compare the type of objects.

**Compare (instanceof)**
```java
"Hello" instanceof String = true
```

## Bitwise and Bit shift Operators
These operators affect the binary values of a variable.

**And - returns 1 if both bits are 1 (&)**
```java
3 & 5 = 1
// Binary = 0011 & 0101 = 0001
```

**Or - returns 1 if either bit is 1 (|)**
```java
3 | 7 = 7
// Binary: 0011 | 0101 = 0111
```

**Exclusive Or - returns 1 if both bits are different (^)**
```java
3 ^ 5 = 6
// Binary: 0011 & 0101 = 0110
```

**Not - inverts the bits (~)**
```java
~3 = -4
// Binary: 0011 = 1100 (-4 due to signed integer)
```

**Left Shift  - moves all the bits of n left p positions (n << p)**
```java
3 << 2 = 12
// Binary: 0011 = 1100
```

**Right Shift - moves all bits of n right p positions (n >> p)**
```java
11 >> 2 = 2
// Binary: 1001 = 0010
```
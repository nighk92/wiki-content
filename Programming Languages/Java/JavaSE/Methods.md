[TOC]

## Overview
A method is a set of code. It can be executed at any point in a program by using its name. Methods can take in values as parameters, and return values as output. As with anything in Java methods can only be created and run from within a class.

## Creating a Method
Methods are given a value to return (or void for no return value), a name and any optional parameters separated by commas. Code is contained within “curly” brackets.
```java
public RETURN_VALUE NAME(PARAMETER_1, PARAMETER_2)
{
	CODE
}
```

## Calling a Method
A method can be called by name to execute it. It can be called from anywhere that its access modifier allows. E.g. a public method can be called from inside a class, or outside from any instance of that class. E.g.
```java
public void printHello() 
{
    System.out.println("Hello");
}

printHello(); // runs the method and prints "hello"
```

## Var Args
It is possible to create a method that takes a variable number of arguments of a specific type. These can be passed to the method separately and become an array of values inside the method. E.g.
```java
public int addNumbers(int... numbers) 
{
    int total = 0;

    for(int number: numbers)
    {
        total += number;
    }

    return total;
}
```

This can be called with any number of integers e.g.
```java
addNumbers(1); // returns 1
addNumbers(1,2); // returns 3
addNumbers(1,2, 3); // returns 6
```

## Return
This can be used to exit a method, or to return a value from a method.

The following method returns true or false depending on the value of markIn. As soon as the return statement is executed, the method exits and execution returns to the calling method.
```java
public boolean passMark(int markIn)
{
	if(markIn < 40)
	{
		return false;
	}
	else
	{
		return true;
	}
}
```

For a method that doesn't need to return a value the `return;` keyword can be used without any value after it. 

## The “main” Method
The `main` method is the starting point for execution of a Java program. This can then create objects and call other methods to create a useful program. It is static and takes in an array of Strings which are optional parameters that can be provided via the command line to the program.
```java
public class MainClass
{
    public static void main(String[] args)
    {
        System.out.println("This will actually run!");
    }
}
```
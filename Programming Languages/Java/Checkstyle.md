## Overview
Checkstyle is a static analysis tool for Java. It checks your code ensuring various formatting and quality controls are met. It is highly configurable so projects can have their own set of rules for code quality.

### Configuration File
Checkstyle is configured using an xml file. For an example configuration file see [checkstyle-config.xml](./.files/checkstyle-config.xml)

### Usage with Maven
The following should be included in your `pom.xml`:
```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-checkstyle-plugin</artifactId>
            <version>3.1.0</version>
            <configuration>
                <!-- Set appropriate config file location -->
                <configLocation>config/checkstyle-config.xml</configLocation>
            </configuration>
            <executions>
                <execution>
                    <goals>
                        <goal>check</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

### Usage with Eclipse
The Eclipse Checkstyle plugin can be installed through the Eclipse Marketplace or downloaded from their website: https://checkstyle.org/eclipse-cs

### Common Issues
#### Cannot initialize module TreeWalker - TreeWalker is not allowed as a parent of LineLength Please review
This is caused by a breaking change to the Eclipse Checkstyle Plugin, from version 8.26 onwards. The fix is to either manually edit the config file and move the `LineLength` module to be a direct descendent of the `Checker` module, or (using the GUI) to remove the check and re-add it.

[TOC]

### Mock a Final Static field value
This method can be used to set the value of a public/private final static field:
```java
/**
 * Method to set the value of a final static field
 *
 * @param field The field to update
 * @param newValue The new value to set
 * @throws Exception Any exceptions
 */
public static void setFinalStatic(final Field field, final Object newValue) throws Exception {
    field.setAccessible(true);

    // remove final modifier from field
    Lookup lookup = MethodHandles.privateLookupIn(Field.class, MethodHandles.lookup());
    VarHandle modifiers = lookup.findVarHandle(Field.class, "modifiers", int.class);
    modifiers.set(field, field.getModifiers() & ~Modifier.FINAL);

    // Set field value
    field.set(null, newValue);
}
```

Usage:
```java
setFinalStatic(PrivateStaticFinal.class.getDeclaredField("variable"), Integer.valueOf(100));
```

### Mock a Final field value
This method can be used to set the value of a public/private final field:
```java
/**
 * Method to set the value of a final field
 *
 * @param field The field to update
 * @param instance The object containing the field to be set
 * @param newValue The new value to set
 * @throws Exception Any exceptions
 */
public static void setFinal(final Field field, final Object instance, final Object newValue) throws Exception {
    field.setAccessible(true);

    // remove final modifier from field
    Lookup lookup = MethodHandles.privateLookupIn(Field.class, MethodHandles.lookup());
    VarHandle modifiers = lookup.findVarHandle(Field.class, "modifiers", int.class);
    modifiers.set(field, field.getModifiers() & ~Modifier.FINAL);

    // Set field value
    field.set(instance, newValue);
}
```

Usage:
```java
PrivateFinal instance = new PrivateFinal();
setFinal(PrivateFinal.class.getDeclaredField("variable"), instance, Integer.valueOf(100));
```

### Get the value of a private field
```java
/**
 * Method to get the value of a private field
 *
 * @param field The field to update
 * @param instance The object containing the value to get
 * @return The value of the field
 *
 * @throws Exception Any exceptions
 */
public static Object getPrivate(final Field field, final Object instance) throws Exception {
    field.setAccessible(true);
    return field.get(instance);
}
```

Usage:
```java
PrivateClass instance = new PrivateClass();
Object value = getPrivate(PrivateClass.class.getDeclaredField("variable"), instance);
```

### Run a private method
This can be used to unit test a private method within a class

```java
/**
 * Invokes a private method on the given object.
 *
 * @param clazz The class containing the method
 * @param methodName The name of the method
 * @param object The object instance to run the method on
 * @param args Arguments to pass to the method
 * @throws Exception Any exceptions that occur when attempting to run the method
 */
public static void invokePrivateMethod(
        final Class<?> clazz,
        final String methodName,
        final Object object,
        final Object...args) throws Exception {

    List<Class<?>> paramTypes = new ArrayList<>();

    // Work out the correct param types for the method
    Arrays.stream(args).forEach(arg -> {
        Class<?>[] interfaces = arg.getClass().getInterfaces();

        boolean isMock = Arrays.stream(interfaces).anyMatch(
            interfaceClass -> interfaceClass.equals(org.easymock.cglib.proxy.Factory.class));

        Class superclass = arg.getClass().getSuperclass();

        if(isMock) {
            // This is a mock object, so the superclass will be the correct class to use
            paramTypes.add(superclass);
        } else {
            // Check for superclass of List, Set or Map interfaces
            if(AbstractList.class.equals(superclass)
                || AbstractSet.class.equals(superclass)
                || AbstractMap.class.equals(superclass)) {

                paramTypes.add(superclass.getInterfaces()[0]);
            } else {
                // Not a mock or common interface, so use class of object directly
                paramTypes.add(arg.getClass());
            }
        }
    });

    Method method = clazz.getDeclaredMethod(methodName, paramTypes.toArray(new Class[0]));
    method.setAccessible(true);
    method.invoke(object, args);
}
```

Usage:
```java
TestObj testObj = new TestObj();
TestHelper.invokePrivateMethod(TestObj.class, "methodName", testObj, "Arg 1", "Arg 2");
```
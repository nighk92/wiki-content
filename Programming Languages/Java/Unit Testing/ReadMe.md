## Overview
Unit testing is an important part of developing code. The code is tested in isolation to ensure it does what it should. The aim is to test every possibility to find any bugs. All calls to external methods should be mocked out.

JUnit is used to run unit tests in Java. Mocking libraries include: EasyMock, PowerMock and Mockito.

## Testing Steps
Unit test each method individually:

* Check each parameter has the desired effect
* Test boundaries between partitions e.g. test IF statements work correctly
* Test using normal, extreme and erroneous data

## Overview
JAR (Java Archive - .jar file) is a package file format used to bring together many Java class files and associates libraries into one executable file. This can be run on any computer with the JRE installed.

## Creating a JAR in Eclipse
To create a JAR file through Eclipse:
* File > Export
* Select Java > Runnable JAR File
* Under “Launch Configuration” select your “Main” Method File
* Choose an Export Destination
* Under Library handing
    * Select “Package required libraries into generate JAR”
* Click “Finish”
Note: If your Java program requires additional files/folders (like pictures) they should be put in the same directory as your JAR file.

## Converting a JAR to an EXE
To make an EXE file from a JAR file that can install your program on any windows machine carry out the following steps:

* Install Launch4j (http://launch4j.sourceforge.net) and InnoSetup Compiler (http://www.jrsoftware.org/isinfo.php)
* Package your JAR file into an EXE using Launch4j - This ensures the user has Java installed before running your EXE, otherwise it just won’t run and nobody will know why!
    * Open the program, specify an output EXE file, and you input JAR file
    * Open the “JRE” tab and specify a Min JRE version e.g. for Java 6 put 1.6.0
    * Press the “Build” button (looks like a settings cog)
    * Specify a name to save the Launch4j config file, in case you want to remake your EXE
    * You EXE file has been made!
* Use InnoSetup Compiler to add an Installer and package any additional files your program needs - this will create another EXE file which can be used to install your program on any Windows machine
    * Open the program, select “Create a new empty script file” and press OK
    * Open File > New - This opens the wizard to make your program installer EXE
    * Click Next
    * Enter in your application details & click next
    * Select the install directory location & click next
    * Add your EXE file that you made with Launch4j, and add any other files/folders your application needs to run & click next
    * Adjust other details for your application installer & click next
    * Add optional licence files & click next
    * Specify any additional installer languages & click next
    * Add a setup icon file if you have one & click next
    * Click next again, then click finish
    * On the popup box elect “Yes” to compile your script (alternatively to re-compile open Build > Compile)
    * From the top toolbar open Build > Open Output Folder
    * Your installer EXE file has been created here
    * Run it to test!
## Overview
Java was developed by Sun Microsystems in 1995 and is one of the most popular programming languages. It is now owned by Oracle.

Java is a general-purpose, object-oriented, strongly typed, compiled programming language. It is intended to be write once, run anywhere – meaning that compiled Java code can run on all platforms that have the JRE (Java Runtime Environment) installed. Java applications are typically compiled into bytecode that can run on any JVM (Java Virtual Machine).

The JRE is a software package that contains what is required to run a Java program. It includes a Java Virtual Machine implementation together with an implementation of the Java Class Library.

The JDK (Java Development Kit) is a superset of a JRE and contains tools for Java programmers, e.g. a javac compiler.

JavaSE is the standard edition of Java. JavaEE adds lot of additional functionality and frameworks for building larger enterprise applications.

## Pros/Cons
| Pros | Cons |
| --- | --- |
| Standard and extensive syntax | Isn't optimised for any other programming styles other than object-oriented |
| Many native and 3rd party libraries providing extensive functionality | Syntax can be verbose, so it is slower to develop small programs |
| Easy to write reusable code, with inheritance and interfaces | Tends to use a lot of RAM |
| Very portable due to the JRE, and well supported on mobile devices | |
| Modern Java versions are fast to run and comparable to C++ for speed | |

## Program Lifecycle
Java code goes through several different stages before it can run, the diagram below shows the code lifecycle from writing the code to running it:

![Program Lifecycle](.images/Program_Lifecycle.png)
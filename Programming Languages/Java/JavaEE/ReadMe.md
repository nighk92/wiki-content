## Overview
JavaEE is the Java Enterprise Edition. It includes many features over JavaSE (Standard Edition) which aid the development of large applications with many users. JavaEE applications have to be run on a server that supports all it's features, typically this is a [Jboss / Wildfly](/Web Servers/WildFly \(Jboss\)) server.

## Features
* Servlets
* WebSocket API
* Java Server Faces JSF
* Unified Expression Language
* REST API
* JSON API
* XML API
* JPA (Java Persistence API)
* JTA (Java Transaction API)
* JMS (Java Messaging Service)
* EJB (Enterprise Java Beans)
* Managed Beans
* Data validation e.g. Bean Validation API
* Interceptors
* Context and Dependency Injection
* Batch Applications / Timers
* JavaEE Connector Architecture
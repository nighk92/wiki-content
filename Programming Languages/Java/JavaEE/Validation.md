[TOC]

## Overview
JavaEE provides a comprehensive framework for validating fields. The framework consists of field level annotations, and validation methods. This validation is normally applied to EJB's but it can be applied to any standard Java class as well.

## Simple Validation
**1. Add the following maven dependencies to your project**
```xml
<dependencies>
    <!-- Main validation interface -->
    <dependency>
        <groupId>javax.validation</groupId>
        <artifactId>validation-api</artifactId>
        <version>2.0.0.Final</version>
    </dependency>

    <!-- Hibernate implementation of the validation interface, these do the validating -->
    <dependency>
        <groupId>org.hibernate.validator</groupId>
        <artifactId>hibernate-validator</artifactId>
        <version>6.0.2.Final</version>
    </dependency>
    <dependency>
        <groupId>org.hibernate.validator</groupId>
        <artifactId>hibernate-validator-annotation-processor</artifactId>
        <version>6.0.2.Final</version>
    </dependency>

    <!-- Expression Language API for variable interpolation inside violation messages -->
    <dependency>
        <groupId>javax.el</groupId>
        <artifactId>javax.el-api</artifactId>
        <version>3.0.0</version>
    </dependency>

    <!-- Implementation of the Expression Language API -->
    <dependency>
        <groupId>org.glassfish.web</groupId>
        <artifactId>javax.el</artifactId>
        <version>2.2.6</version>
    </dependency>
</dependencies>
```

**2. Create a class specifying rules using the validation annotations**
```java
import javax.validation.constraints.NotNull;

public static class User
{
    @NotNull(message = "Name can't be null")
    private String name;

    public void getName()
    {
        return name;
    }

    public String setName(String nameIn)
    {
        this.name = nameIn;
    }
}
```

**3. Validate instances of the class using a validator**
```java
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

...

Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

User user = new User();
Set<ConstraintViolation<User>> violations = validator.validate(user);

for (ConstraintViolation<User> violation : violations) {
    /* Prints out any validation errors */
    System.err.println(violation.getMessage());
}
```

## Validation Annotations
All annotations have a message property that can be used to set a custom error message.

| Annotation | Type | Check (field is the annotated variable) |
| --- | --- | --- |
| @NotNull | Any | field != nulll |
| @Max(value = 5) | Any number | field <= value |
| @Min(value = 1) |Any number | field >= value |
| @Negative | Any number | field < 0 |
| @NegativeOrZero | Any number | field <= 0 |
| @Possitive | Any number | field > 0 |
| @PossitiveOrZero | Any number | field >= 0 |
| @AssertFalse | Boolean, boolean | field == false |
| @AssertTrue | Boolean, boolean | field == true |
| @Future | Date, Calendar | field > Date.now() |
| @FutureOrPresent | Date, Calendar | field >= Date.now() |
| @Past | Date, Calendar | field < Date.now() |
| @PastOrPresent | Date, Calendar | field <= Date.now() |
| @Email | String | Checks a String is an email address |
| @Pattern(regexp = "d{5}") | String | Checks a String matches the given regular expression |
| @Size(min = 1, max = 5) | String, Collection, Map, Array | min < field.size() < max |
| @NotEmpty | String, Collection, Map, Array | field.isEmpty() != true |
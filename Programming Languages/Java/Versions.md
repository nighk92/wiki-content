[TOC]

## Versions
#### JavaSE 6 (2006)
* Includes all the core JavaSE syntax
* Lots of performance improvements

#### JavaEE 5 (2006)
* Includes the core JavaEE syntax
* Entity beans replaced with independent JPA (Java Persistence API)
* APIs for XML web services like SOAP
* More lightweight and efficient

#### JavaEE 6 (2009)
* SOAP with attachments API
* APIs for REST web services
* Dependency injection
* Bean validation
* Managed beans
* Interceptors

#### JavaSE 7 (2011)
* Allows Strings in a switch statement
* Type inference with the diamond operator (<>)
* Multi-catch blocks for use with try statements
* A new File IO library (java.nio)
* Timsort is now used to sort arrays instead of merge sort

#### JavaEE 7 (2013)
* WebSockets API
* Support for JSON processing
* Servlet async non-blocking new IO
* Support for Batch Processing

#### JavaSE 8 (2014)
* LTS release, supported until at least 2026
* Support for inline lambda expressions
* Support for streams
* Unsigned Integer support
* New LocalDate, LocalTime, LocalDateTime, Period and Time Zones APIs
* Allows annotations to be repeated more than once on the same object
* Ability to launch JavaFX applications through the main method, so they can be deployed as JARs

#### JavaSE 9 (2017)
* A new Java command line interface
* Ahead of time compiler which may improve the performance of some applications
* Improvements to the streams API e.g.
    * Addition of `Matcher.replaceAll(Function<MatchResult,String> replacer);` method
* Addition of reactive streams for asynchronous stream and event processing
* Java Linker which can link modules and their dependencies to create an efficient run time image

#### JavaEE 8 (2017)
* Support for the HTTP 2.0 standard
* Support for server-sent events
* API for JSON binding
* Caching support with JCache
* Additional support for cloud applications

#### JavaSE 10 (2018) 
* Ability to declare untyped local variables with the "var" keyword
* Class data sharing enabling classes to be placed in a shred archive reducing the startup time and memory footprint of an application
* Improved ability to execute callbacks from a thread, making it easier to shut down individual threads

#### JavaSE 11 (2018)
* LTS release, supported until at least 2024
* Removes the Java EE and CORBA deprecated in Java 9
* `HttpClient` class, first introduced in Java 9, is now a standard and supported feature
* Added support for `var` keyword in Lambda parameters, meaning annotations can be added e.g.
```java
import org.jetbrains.annotations.NotNull;

  List<String> list = Arrays.asList("a", "b", "c", null);
  String result = list.stream()
          .map((@NotNull var x) -> x.toUpperCase())
          .collect(Collectors.joining(","));
  System.out.println(result3);
```
* Added support for Unicode 10 characters in Strings e.g. `String crazyFaceInUnicode = "U+1F92A"`
* Added support for Transport Layer Security (TLS) 1.3
* Nashorn Javascript engine is now deprecated

## Migration
### Migrating from Java 8 to Java 11 (for Maven projects)
* Ensure you have the Java 11 JDK installed, if not this can be done on Centos 8 with the following:
```bash
sudo dnf install java-11-openjdk-devel -y
```

* Ensure Java 11 is the default Java version, on Centos 8 this can be done with the following:
```bash
update-alternatives --config java
# Select the number corresponding to Java 11
```

* Set your JAVA_HOME to point to Java 11, otherwise maven may use a different version of Java
```bash
vi ~/.bashrc

# Add the following line at the bottom, then save the file
export JAVA_HOME=/usr/lib/jvm/java-11

source ~/.bashrc
```

* Set the Java version in the pom.xml
```xml
<!-- Set these properties -->
<properties>
    <maven.compiler.target>11</maven.compiler.target>
    <maven.compiler.source>11</maven.compiler.source>
</properties>

<!-- Or direct configutation via the plugin -->
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-compiler-plugin</artifactId>
    <version>3.8.1</version>
    <configuration>
        <source>11</source>
        <target>11</target>
    </configuration>
</plugin>
```

* Replace `findbugs-maven-plugin` with `spotbugs-maven-plugin`, use version 3.X which is fully compatible with all findbugs configuration options e.g.
```xml
<plugin>
    <groupId>com.github.spotbugs</groupId>
    <artifactId>spotbugs-maven-plugin</artifactId>
    <version>3.1.12.2</version>
    <executions>
        <execution>
            <phase>verify</phase>
            <goals>
                <goal>check</goal>
            </goals>
        </execution>
    </executions>
    <configuration>
        <xmlOutput>true</xmlOutput>
        <effort>max</effort>
        <failOnError>true</failOnError>
        <onlyAnalyze>my.package.*</onlyAnalyze>
    </configuration>
</plugin>
```

* Upgrade the following packages in the pom.xml to the specified version or higher:
    * EasyMock to **4.2**
    * PowerMock to **2.0.9**
    * Mockito to **2.23.0**
    * maven-surefire-plugin to **2.22.0** (may not be necessary?)

* Add the following configuration for powermock under `src/test/resources/org/powermock/extenions/configuration.properties`
```
powermock.global-ignore=javax.xml.parsers.*,org.apache.xerces.jaxp.*,com.sun.org.apache.xerces.*,org.xml.*
```

* In order to fix powermock tests add the following annotation to every test that uses the `PowerMockRunner`
```java
@PowerMockIgnore("org.w3c.dom.*")
```

* Update any instances of Easymock's `new Capture<>()` to `Capture.newInstance()`

* Replace the `cobertura-maven-plugin` with the `jacoco-maven-plugin` as below, this will run automatically as part of a `mvn clean install` or `mvn test`, so the `mvn cobertura:cobertura` command is no longer valid.
```xml
<plugin>
    <groupId>org.jacoco</groupId>
    <artifactId>jacoco-maven-plugin</artifactId>
    <version>0.8.6</version>
    <configuration>
        <excludes>
            <exclude>my/package/ClassToIgnore.class</exclude>
            <exclude>my/package2/**/*.class</exclude>
        </excludes>
    </configuration>
    <executions>
        <execution>
            <goals>
                <goal>prepare-agent</goal>
            </goals>
        </execution>
        <!-- attached to Maven test phase -->
        <execution>
            <id>report</id>
            <phase>test</phase>
            <goals>
                <goal>report</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```
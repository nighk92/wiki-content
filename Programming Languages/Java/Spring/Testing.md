[TOC]

## Overview
Spring and Spring Boot provide an extensive framework for unit testing and integration testing. It starts a lightweight version of the application with certain features mocked in preparation for testing.

## Unit Testing
It is possible to mock the Spring MVC and check the responses from web endpoints return the expected output under controlled circumstances. This is done with several annotations as shown below.

```java
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * Unit tests for the HelloController
 * This starts a lightweight, partially mocked, version of the application and performs tests against it
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class HelloControllerTest
{
    @Autowired
    private MockMvc mvc;

    /**
     * Test the "/" web endpoint returns the expected message
     */
    @Test
    public void testGetHello()
    {
        mvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().string(
                Matchers.equalTo("Greetings from Spring Boot")));
    }
}
```

## Integration Testing
Spring also has extensive support for integration testing applications. As an application is standalone it can be integration tested through your IDE. Integration testing a Spring application is much like unit testing it except that the Spring MVC isn't mocked. A full version of the application is started on a random port and the integration tests are directly run against it.

```java
import java.net.URL;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Integration tests for the HelloController
 * This starts a full version of the application on a random port and performs tests against it
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HelloControllerIntegrationTest
{
    /* Get the port the application was started on */
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate template;

    private URL base;

    /**
     * Steps run before each test
     */
    @Before
    public void setUp()
    {
        this.base = new URL("http://localhost:" + port + "/");
    }

    /**
     * Test the "/" web endpoint returns the expected message
     */
    @Test
    public void testGetHello()
    {
        ResponseEntity<String> response = template.getForEntity(
            base.toString(), String.class);

        Assert.assertThat(response.getBody(), Matchers.equalTo("Greetings from Spring Boot"));
    }
}
```
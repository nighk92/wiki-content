## Overview
There are several different components that make up a Spring application. Spring Boot will create a lot of the boilerplate code for you and provide sensible default functionality. The general components are:

* [A main class](Main Class.md) - Runs the application
* [Beans](Beans.md) - Provides server side functionality
* [Controllers](Controllers.md) - A web component
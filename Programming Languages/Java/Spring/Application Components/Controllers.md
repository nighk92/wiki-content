## Usage
A controller is a web component for a Spring application. It exposes functionality so it can be accessed via a web address. Controllers can provide any sort of output including web pages, JSON or XML. They can call beans using the applications context to gain access to lower level functionality.

```java
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Provides web endpoints for the application
 */
@RestController
public class HelloController
{
    @Autowired
    private ApplicationContext context;

    /**
     * Provides a HTML web page when a user accesses the "/" endpoint
     *
     * @return The HTML provided by a bean
     */
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String index()
    {
        return context.getBean(MyClass.class).getHtml();
    }
}
```
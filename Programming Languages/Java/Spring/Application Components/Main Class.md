## Usage
As Spring applications are standalone they run from the Main method. The static SpringApplication.run(Object source, String... args) method is used to start an application. By default Spring Boot will run using an embedded version of the Apache Tomcat server.

The @SpringBootApplication annotation is a convenience annotation that adds all of the following:

* @Configuration - Tags the class as a source of bean definitions for the application context
* @EnableAutoConfiguration - Tells Spring Boot to add beans based on the classpath settings, other beans, and various property settings
* @EnableWebMvc - If Spring Boot sees the spring-webmvc package on the classpath it flags the application as a web application and activates key behaviours such as setting up a DispatcherServlet
* @ComponentScan - Tells Spring to look for other components, configurations, and services in the current package, allowing it to automatically find controller classes

```java
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The main class, that is the entrypoint for the Spring application
 */
@SpringBootApplication
public class Application
{
    /**
     * Runs the Spring application
     *
     * @param args Command line arguments passed to the application
     */
    public static void main(String[] args)
    {
        SpringApplication.run(Application.class, args);
    }
}
```
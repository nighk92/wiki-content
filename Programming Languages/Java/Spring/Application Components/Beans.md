[TOC]

## Usage
Beans are objects that are created and managed by the Spring container. They form the backbone of the application, executing server-side logic. A Bean can be configured by Java code or XML. Spring Boot provides many default beans out of the box.

Classes containing beans must be annotated with @Configuration. The beans configurations themselves are methods annotated with @Bean.

The return value of an @Bean method is registered as a bean within the `BeanFactory`. By default the name of the bean is the same as the method name.

```java
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Provides bean configuration for the application
 */
@Configuration
public class ApplicationConfiguration
{
    /**
     * Registers the MyClass bean in the applications context
     *
     * @return A new instance of MyClass
     */
    @Bean
    public MyClass myClassBean()
    {
        return new MyClass();
    }
}
```

The beans can be accessed anywhere in the application through the applications context. See the example on the [controllers](Controllers.md) page.

## Scope
The default scope for a bean is a "singleton", the following scopes are available:

* **singleton** - A only single instance is ever used, the same instance is returned everytime the bean is referenced
* **prototype** - A new instance is created each time the bean is referenced
* **session** - A new instance is created once per user session (web environment only)
* **request** - A new instance is created once per request (web environment only)

The scope can be adjusted by adding the @Scope annotation e.g.
```java
@Bean
@Scope("prototype")
public MyClass myClassBean() {
   return new MyClass();
}
```
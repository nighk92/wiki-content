[TOC]

## Overview
Spring and Spring Boot use a lot of annotations to provide functionality. This means a lot of spring code just consists of POJO's and functionality is added to them using the various annotations provided.

## List of Spring Annotations
### Standard
| Annotation | Included Annotations | Description |
| --- | --- | --- |
| @Aspect | | Part of the AOP (Aspect-Oriented Programming) support Spring offers, this registers a class as an Aspect. The methods can then be annotated separately to enable them to be run under the specified conditions. |
| @Autowired | | Similar to JavaEE's @Inject. Provides dependency injection, injecting the specified bean into the current class. It can be used on Constructors, Methods or Class Variables. |
| @Bean | | Defines a method as a bean provider, which is then managed by the Spring container. |
| @Cacheable | | Marks a methods return value as cacheable. Spring will manage the cache automatically and only call the annotated method if the value hasn't been previously cached. |
| @Component | | A generic stereotype to specify a bean as a Spring managed component, specialisations of this annotation include: @Controller, @Repository and @Service |
| @ComponentScan | | Tells Spring to look for other components, configurations, and services in the current (or specified) package, allowing it to automatically find controller classes. |
| @Configuration | | Tags the class as a source of bean definitions for the application context. |
| @Controller | | A specialisation of @Component, used to annotate classes for Spring MVC. It allows these classes to be autodetected through the classpath. |
| @Description | | Provides an additional description for a Bean. |
| @EnableAspectJAutoProxy | | Part of Springs support for AspectJ, this turns on scanning for Aspects and enables any that are found. |
| @EnableJpaRepositories | | Scans for any CrudRepository interfaces, and automatically creates implementations for them so they can be used to run actions using the JPA against an SQL database. |
| @EnableMongoRepositories | | Scans for any interfaces, and automatically creates implementations for them so they can be used to run actions using the JPA against a MongoDB database. |
| @EnableWebMvc | | If the spring-webmvc package is on the classpath it flags the application as a web application and activates key behaviours such as setting up a DispatcherServlet. |
| @Import | | Used to import beans from another @Configuration file, so multiple configuration files can be combined into a single Application Context providing a single starting point for the application. |
| @ImportResource | | Used to import Spring configuration from an XML file. |
| @Lazy | | Specified a bean as lazy, so instead of being initialised on application startup it is created when it is first used. |
| @Qualifier | | Used to specify the name of a bean to inject when using @Autowired |
| @PathVariable | | Used on a controller method to indicate a method parameter value should be set to the named path parameter. |
| @PostConstruct | | JavaEE annotation, used on a bean class to specify the method run at startup after the constructor and dependency injection. |
| @PreDestroy | | JavaEE annotation, used on a bean class to specify the method run prior to destroying the bean instance, this is not called for "prototype" scoped beans |
| @Profile | | Specify a profile for a @Configuration class or a @Bean method, e.g. to specify if beans should just be scoped to a development environment and not included in production. |
| @PropertySource | | Contributes additional property sources, e.g. can be used to specify an additional properties file to load onto the classpath. |
| @Repository | | A specialisation of @Component, used to annotate classes at the persistence layer. It catches persistence specific exceptions and re-throws them as one of Spring's unified unchecked exceptions. |
| @RequestBody | | Used on a controller method to Indicate a parameters value should be de-serialised from the body of the web request. |
| @RequestMapping | | Maps a web endpoint to a method. Specialisations of this include: @GetMapping, @PostMapping, @PutMapping, @PatchMapping, @DeleteMapping |
| @RequestParam | | Used on a controller method to indicate a method parameter value should be set to the named request parameter. |
| @Resource | | JEE annotation supported bu Spring, used for dependency injection by name not type as @Autowired does. This supports setter and class variable injection only. |
| @ResponseBody | | Used on a controller method to specify that the object returned from a method will constitute the body of the HTTP response, this will automatically serialise the return value of the method into JSON or XML as appropriate. |
| @RestController | @Controller, @ResponseBody | Flags a class to the Spring MVC, so it can be used to handle web requests. |
| @Service | | A specialisation of @Component, used to annotate implementation classes at the service layer. It allows for implementation classes to be autodetected through classpath scanning. |
| @Transactional | | Used on a class/method to mark operations that require a transaction e.g. creating a record in a database should be done transactionally so no other operations take place while it happens, and if it fails nothing is committed to the database. |
| @Value | | Used to inject a value from the environment properties into a variable. It can be used on class variables and method arguments. Before this can be used a PropertySourcesPlaceholderConfigurer bean must be declared. The SpEL can also be used with this annotation. |

### Testing
| Annotation | Included Annotations | Description |
| --- | --- | --- |
| @AfterTransaction | | Mark a method in the test to run after the transaction has been completed for tests marked with @Transactional, this is an alternative to using @After which will be run within the transaction. |
| @BeforeTransaction | | Mark a method in the test to run before the transaction has been started for tests marked with @Transactional, this is an alternative to using @Before which will be run within the transaction. |

## List of Spring Boot Annotations
### Standard
| Annotation | Included Annotations | Description |
| --- | --- | --- |
| @EnableAutoConfiguration | | Tells Spring Boot to add beans based on the classpath settings, other beans, and various property settings. |
| @EntityScan | | Enables scanning for JPA entities (data models) and will automatically create the required database tables. |
| @SpringBootApplication | @ComponentScan, @SpringBootConfiguration (just extends @Configuration), @EnableAspectJAutoProxy, @EnableAutoConfiguration, @EnableJpaRepositories (if spring-jpa is on the classpath), @EnableWebMvc (if spring-webmvc is on the classpath) | Convenience annotation that configures the main class for a Spring application. |

### Testing
| Annotation | Included Annotations | Description |
| --- | --- | --- |
| @AutoConfigureMockMvc | | Auto configures a mock Spring MVC for use when unit testing web endpoints. |
| @LocalServerPort | | Injects the port the Spring Test server has been started on into a class level variable. |
| @SpringBootTest | | Sets up a Spring Boot test server and environment for unit/integration testing. |

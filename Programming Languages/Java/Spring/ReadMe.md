## Overview
Spring is a framework that provides alternative implementations of many JavaEE features. Spring doesn't require a JavaEE server to run so it can be more lightweight than a JavaEE application. The majority of Springs functionality is provided by annotations.

Spring Boot is part of the Spring application framework. It aims to make the creation of Web Applications faster and reduce the amount of boiler plate code and configuration needed.

## Versions
#### 2.0  (2006) 
* XML Simplification
* Async JMS (Java Messaging Service)
* JPA (Java Persistence API)
* AspectJ Support

#### 2.5 (2007)
* Requires Java 4+
* Supports JUnit 4
* Annotation Dependency Injection
* @MVC Controllers
* XML Namespaces

#### 3.X (2009)
* Environment & Profiles
* @Cacheable
* @EnableXXX
* Requires Java 5+ and JUnit 4.7+
* REST Support
* JavaConfig
* Spring Expression Language 
* More annotations

#### 4.X (2013)	
* Support for Java 8
* @Conditional
* Web-sockets

#### 5.X (2017) 
* Requires Java 8+
* Support for functional programming within the Spring framework using Kotlin
* Reactive programming focus with support for Reactive Streams, and the new non-blocking WebClient which is an alternative to the RestTemplate

## Overview
In order to connect to a MongoDB using Spring, it is necessary to use the `MongoRepository` interface provided by the Spring Boot `starter-data-mongodb` library. This provides many out of the box methods including `save`, `delete`, `findAll` and `count`. It is possible to add additional methods by implementing the interface, however there is normally no need to add content to your own methods as Spring will add the implementation for them at runtime providing you follow the standard naming convention.

## Example
This shows a standard Customer model, and how to make it persistent with Spring Boot:
```java
package customer.models;

import org.springframework.data.annotation.Id;

/**
 * Stores information on a customer
 */
public class Customer
{
    @Id
    public String id;

    public String firstName;
    public String lastName;

    /**
     * Creates a new blank customer, necessary for the find methods in the CustomerController to work
     */
    public Customer() {}

    /**
     * Creates a customer with the given information
     *
     * @param firstNameIn The customers first name
     * @param lastNameIn The customers last name
     */
    public Customer(String firstNameIn, String lastNameIn)
    {
        firstName = firstNameIn;
        lastName = lastNameIn;
    }
}
```

Customer specific extension of the `MongoRepository` interface:
```java
package customer.mongo;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import customer.models.Customer;

/**
 * Connects to mongo to save customer data
 */
public interface CustomerRepository extends MongoRepository<Customer, String>
{
    /**
     * Finds a single customer matching the first name given
     *
     * @param firstName The first name to search for
     * @return The customer matching the given first name
     */
    public Customer findByFirstName(String firstName);

    /**
     * Finds all the customers matching the given last name
     *
     * @param lastName The last name to search for
     * @return All the customers matching the given last name
     */
    public List<Customer> findByLastName(String lastName);
}
```

Using the `CustomerRepository` methods:
```java
package customer.beans;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import customer.models.Customer;
import customer.mongo.CustomerRepository;

/**
 * A Bean providing functionality for Customers
 */
public class CustomerBean
{
    @Autowired
    private CustomerRepository repository;

    /**
     * Creates a customer storing their information in the backing database
     *
     * @param customer The customer to save
     * @return The new customer information
     */
    public Customer createCustomer(Customer customer)
    {
        return repository.save(customer);
    }

   /**
     * Finds a customer by their first name
     *
     * @param firstName The first name to search for
     * @return The customer found
     */
    public Customer findByFirstName(String firstName)
    {
        return repository.findByFirstName(firstName);
    }

    /**
     * Retrieves all the customers stored in the backing database
     *
     * @return All the customers stored
     */
    public List<Customer> findAll()
    {
        return repository.findAll();
    }
}
```
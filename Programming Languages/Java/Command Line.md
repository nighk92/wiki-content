## Compile
Runs the Java compiler to create a .class file from .java source code. This can be run on the “Main” method file of an application to compile all files for that application.
```bash
javac FILENAME
```

## Run a Class
Executes a .class file to run a Java application. Do this on the “Main” method file.
```bash
java FILENAME
```

## Run a Jar
Executes a .jar file to run a Java application.
```bash
java <options> -jar FILENAME.jar

# Options:
# -Xmx1024m = sets maximum ram allowance to 1024 MB i.e. 1 GB
```

## Keytool
Keytool is a Java command line tool used for manipulating keystores. The default keystore location in linux is /usr/java/lates/bin

List a keystores info
```bash
keytool -list -v  -keystore path_to_cert
```

Check a stand-alone certificate
```bash
keytool -printcert -v -file mydomain.crt
```

Check a particular keystore entry using an alias
```bash
keytool -list -v -keystore keystore.jks -alias mydomain
```

Generate a Java keystore and key pair
```bash
keytool -genkey -alias mydomain -keyalg RSA -keystore keystore.jks -keysize 2048
```

Generate a certificate signing request (CSR) for an existing Java keystore
```bash
keytool -certreq -alias mydomain -keystore keystore.jks -file mydomain.csr
```

Import a root or intermediate CA certificate to an existing Java keystore
```bash
keytool -import -trustcacerts -alias root -file Thawte.crt -keystore keystore.jks
```

Import a signed primary certificate to an existing Java keystore
```bash
keytool -import -trustcacerts -alias mydomain -file mydomain.crt -keystore keystore.jks
```

Generate a keystore and self-signed certificate
```bash
keytool -genkey -keyalg RSA -alias selfsigned -keystore keystore.jks -storepass password -validity 360 -keysize 2048
```

Delete a certificate from a Java Keytool keystore
```bash
keytool -delete -alias mydomain -keystore keystore.jks
```

Change a Java keystore password
```bash
keytool -storepasswd -new new_storepass -keystore keystore.jks
```

Export a certificate from a keystore
```bash
keytool -export -alias mydomain -file mydomain.crt -keystore keystore.jks
```

List Trusted CA Certs
```bash
keytool -list -v -keystore $JAVA_HOME/jre/lib/security/cacerts
```

Import New CA into Trusted Certs
```bash
keytool -import -trustcacerts -file /path/to/ca/ca.pem -alias CA_ALIAS -keystore $JAVA_HOME/jre/lib/security/cacerts
```

List info about a PKCS12 (.pfx or .p12)
```bash
keytool -list -storetype pkcs12 -keystore source.p12
```

Convert a PKCS12 (.pfx or .p12) to a keystore, to find the srcalias use the "list info about PKCS12" command above. This can be used to generate a new keystore or add certificates to an existing keystore under another alias.
```bash
keytool -importkeystore -srckeystore source.p12 -srcstoretype pkcs12 -destkeystore dest.jks -deststoretype jks -deststorepass password

# Or with aliases
keytool -importkeystore -srckeystore source.p12 -srcstoretype pkcs12 -srcalias alias -destkeystore dest.jks -deststoretype jks -deststorepass password -destalias alias
```

Convert a keystore to a PKCS12 (.pfx or .p12) which can then be converted to other types like a PEM using openssl on [[[linux]]]
```bash
keytool -importkeystore -srckeystore source.jks -destkeystore dest.p12 -srcstoretype jks -deststoretype pkcs12
```
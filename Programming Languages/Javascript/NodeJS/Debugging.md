## Debugging

To turn on full debug logging from all node modules set the following environment variable:
```bash
NODE_DEBUG="*"
```

To turn on debug logging for specific modules (e.g. to debug ssl connections) set:
```bash
NODE_DEBUG='tls,https' 
```
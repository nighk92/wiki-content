## Overview
NPM is the Node Package Manager. It provides a framework to install Javascript libraries. Each library is versioned. Specific packages can be specified for an application by listing them in a `package.json` file. NPM comes pre-installed with NodeJS.

NPM also provides a global registry for packages, this is where versions of libraries are uploaded so other people can use them.
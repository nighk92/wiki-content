## Common Issues
#### npm ERR! async write is not a function
This is caused by the npm installation becoming corrupted, the solution is to reinstall it e.g.
```bash
curl -0 -L https://npmjs.com/install.sh | sudo sh
```

#### No space left on device
This can mean a few things:

* You are out of disk space
    * Run `df -h` to check
	* The solution is to increase the disk space or delete stuff
* You are out of file watchers
    * The solution is to increase the number of file watchers by running `echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p`
* You are out of inodes
    * Run `df -i` to check
	* The solution is to increase the disk space, delete stuff (lots of small files as each file uses an inode), or reformat the drive in a different disk format that gives more inodes

[TOC]

## General Notes
Concept of Container components that have all the function and Redux logic, and Presentation components which are dumb components that just contain the JSX/html layout inside a render method,

ESLint can be used to check the style/quality of javascript code

Initialise all inputs with values, e.g. use an empty string instead of null. This will prevent errors from react controlled components.

Can automatically namespace CSS so that individual component styles won't affect the rest of the application, this is done by importing the stylesheet as an object and referring to the class names through that CSS object. Node modules you need to configure the package.json file to look at the processed CSS file, see: https://jaketrent.com/post/package-json-style-attribute/. Or instead use JSS: http://cssinjs.org/react-jss?v=v8.3.5
```javascript
import style from "./style.css";

...

<div className={style.myCssClass}>
    ...
</div>
```

Can display pure html inside a JSX object 
```javascript
let html = <strong>bold text</strong>;

<p dangrouslySetInnerHtml={{__html: html}}/>
```

Using this.methodName.bind(this) will enable the use of the this keyword inside the method even when it is called from other classes or listeners. It ensures that this always refers to the class the function was declared in and isn't overridden when calling this method from different locations.

Arrow functions at class level automatically bind to the component, no need to call this.method.bind(name), however this will have a performance penalty compared to binding.
```javascript
myFunction() +> {
    // function code
}
```

Can have components described via a function instead of a class
```javascript
const DivElement = function() {
    return (
        <div>some text</div>
    );
}
```

Spread operator (...), makes all values of an object available as individual props in a react component
```javascript
const myObject = {name="Paul", description="An awesome person"}

const myComponent = function() {
    return (
        <div>
            <div>{this.props.name}</div>
            <div>{this.props.description}</div>
        </div>
    );
}

// Use spread operator to make name/description availible as props inside my component
ReactDOM.render(<MyComponent {...myObject} />, baseNode);
```

## Performance Optimisations
Extend React.PureComponent instead of React.Component as the PureComponent will compare the existing state/props (in the componentWillUpdate method) for changes before it updates itself. This means if the parent component is re-rendered the child PureComponents may not, whereas normal Components will always update when the parent updates. It is possible to put a console.log in the componentWillUpdate(nextProps, nextState) method to check when your component is updating. PureComponents only shallowly compare objects for changes, so if you mutate a property on an object the component won't update, it is possible to use the Immutable JS library to efficiently force new objects to be made when a value is updated.

Don't create functions/objects in the render method as they are re-created every time the component is re-rendered.
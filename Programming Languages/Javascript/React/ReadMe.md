## Overview
React is a Javascript library for building front end web applications originally written by Facebook. 

Unlike other UI frameworks/libraries it encourages HTML to be included within the Javascript files, normally in the format of JSX code. It focuses on building small reusable components that make up a larger web page. Each component has a state where its variables are stored, the HTML then updates to match the state, so the state should be the single point of truth for the component.

There are various libraries that enable the application to have a global store/state where variables can be stored and used throughout the application. The most popular global stores are Flux and Redux.
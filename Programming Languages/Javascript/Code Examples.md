[TOC]

### Timeout
Set timeout in milliseconds. This method is asynchronous so it can also be used to make new threads.
```javascript
setTimeout(function()
{
   //code to run
}, 100);
```

### Show/Hide Element
Show/Hide a html element, sliding it in/out of view. The time the animation takes can be set in milliseconds.
```html
<script type="text/javascript">
    //Requires Jquery
    $("#element_id").toggle(animationTime);
    $("#element_id").toggle(500);
</script>

<!-- This link will run the above Javascript -->
<a onclick="$('#element_id').toggle(500);">Show/Hide</a>
```

### Global Replace
Replace all occurrences of a string.

This method uses regex, so won't work with special characters like * or $
```javascript
var currentString = "this original string is the original";
var newString = currentString.replace(new RegExp("original", 'g'), "replacement");

// newString = "this replacement string is the replacement"
```

This method is slower but will work with special characters, regex characters are just treated as normal characters
```javascript
var currentString = "this original string is the original";
var newString = currentString.split("original").join("replacement");

// newString = "this replacement string is the replacement"
```
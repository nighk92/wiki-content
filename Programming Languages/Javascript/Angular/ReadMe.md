## Overview
Angular is a Javascript based front end web application framework originally written by Google.

Angular supplements standard HTML with it's own custom tag attributes. The HTML and accompanying Javascript is interpreted to form a dynamic web application. Unlike many traditional web application frameworks all this processing is done on the client side, helping reduce strain on the server.

**AngularJS** 1 is the original version, however it had a lot of performance issues. It has been completely re-written as Angular 2 which is a lot faster but isn't backwards compatible with AngularJS 1. One of the major differences is that Angular 2 has no concept of "scope".
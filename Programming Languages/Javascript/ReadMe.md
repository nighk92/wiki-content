## Overview
Javascript is a dynamically typed scripting language, mainly used in webpages to add additional functionality over static HTML and CSS. It is interpreted, not compiled, which makes it platform independent. There are several frameworks that are built upon Javascript including AngularJS and React.

## Pros/Cons
| Pros | Cons |
| --- | --- |
| Quick to write small scripts | Insecure as it executes on the client side |
| Lightweight and fast to run | Relies on the web browsers interpretation of the script, new features are often not supported in all browsers |
| Very flexible, dynamically typed and can instantiate variables automatically | It's flexibility can cause bugs which can be difficult to track down, e.g. having global variables, or making changes to variables that have knock on effects |
| Reduces the load on servers as it mostly runs client side through a web browser | Has a single execution thread |
| Fast to learn the basic functions | Javascript is interperated so is much slower to run than compiled languages |
| Lots of 3rd party libraries, and frameworks build on top of Javascript | |

## Versions
ES stands for EMCAScript.

#### ES 5.1 (2011)
* Includes all the standard syntax
* Is widely supported by web browsers

#### ES 6 (2015)
* Adds classes and modules
* Supported in modern browsers, but tends to be compiled down to 5.1 syntax

#### ES 7 (2016)
* Added exponential operator (**)
* Added Array.prototype.includes method
* Not currently supported in browsers, has to be compiled down to 5.1 syntax
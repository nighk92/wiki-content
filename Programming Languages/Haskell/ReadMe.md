## Overview
Haskell is a compiled, strongly typed, functional programming language. Programs consist of functions which take in an input and provide an output, multiple functions can then be chained together and pass data to each other. It doesn't store objects, as all data is contained within a function and then output once it has completed. There are no loops either, however loops can be made by a function calling itself recursively.

## Pros/Cons
| Pros | Cons |
| --- | --- |
| Code is concise | Difficult to learn |
| Fast to run | Difficult to read |
| Very good for concurrency e.g. for backend web server functions | Not ideal for programming GUI's |
| | Libraries are often overcomplicated and poorly documented |

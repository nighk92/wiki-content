## Overview
Python is a multi-purpose, dynamically typed, interpreted programming language. It can be used for object-oriented, scripting, or functional programming. It's main focus is on readability, with a syntax made largely of natural language, and using white space to delimit code blocks instead of curly braces.

## Pros/Cons
| Pros | Cons |
| --- | --- |
| Syntax is easy to read | Can be slow to run as it is interpreted not compiled |
| Fast and easy to learn | Not well supported on mobile devices |
| Lots of support for writing asynchronous code | Flexibility can lead to bugs e.g with dynamic types |
| Flexible enough to be used with any programming style including object-oriented, scripting or functional | |
| Lots of 3rd party libraries offering additional functionality | |

## Versions
#### 2.7 (2010)
* Supports all the main features of python including scripting and object-oriented capabilities
* Has a lot of 3rd party libraries, some of which aren't compatible with Python 3

#### 3.0 (2008)
* The print statement has been replaced with the print() function
* The input function has been replaced with the raw_input function
* The string and unicode types have been merged
* Changes to integer division functionality. Instead of `5 / 2 = 2`, in Python 3 it equals `2.5`
* Removal of backwards compatible features such as old style classes, string exceptions, and implicit relative imports

#### 3.1 (2009)
* Ordered dictionaries
* The `format()` and `str.format()` functions can format a number with a thousands separator e.g. `format(1234567, ',d') = 1,234,567`
* Zip archives with a `__main__.py` file can be directly executed by the python interpretor

#### 3.2 (2011)
* Helps make extensions more compatible with newer versions of python by defining a `Py_LIMITED_API`
* Adds the concurrent.futures module to manage threads and processes
* Modifications to the .pyc bytecode files, the are now kept under a `__pycache__` directory within a package

#### 3.3 (2012)
* New `yield from` expression allowing a generator to delegate part of it's operations to another generator
* The unicode syntax is accepted again for str objects
* New ipaddess object which represents ip addresses and masks
* New unittest.mock module which can mock objects for unit testing

#### 3.4 (2014)
* Pip is now included with the standard python installation
* New enum module means Python now supports enumerations
* New asynchronous IO module

#### 3.5 (2015)
* Improved support for asynchronous programming, including new `async` functions and new `await` keyword
* New matrix multiplication operator `@` - can be used to multiply the values in one array by the values of another
* Improvements for the array unpacking operator `*` and the dictionary unpacking operator `**`
* A new installer for Windows

#### 3.6 (2016)
* New formatted string literals prefixed with **f**
* Supports underscores in numbers to improve readability e.g. `1_000_000`
* Introduces type hints for function parameters
* `await` and `yield` can now be used within the same function
* New asynchronous for loop e.g. `async for i in array`
* New secrets module for generating secure random values

## Overview
A programming language is something which can be used to provide instructions a computer can execute. These range from very low level machine code which can be executed by the processor directly, to higher level languages which have to be compiled to machine language before they are run. Mostly higher level, human readable, languages are used. There are many different styles of programming language including: Object-Oriented, Scripting, and Functional.

The more common languages include:
* C
* C++
* C#
* Java
* Javascript
* PHP
* Python
* Ruby
[TOC]

## Link to Page Headings

To link to a specific heading on a page do the following

**1.** Select some text or position your cursor where you want to insert the link

**2.** Choose **Link** on the toolbar or use the keyboard shortcut **Ctrl+k**

**3.** Click the **Advanced** tab

**4.** In the **link** box enter: Page Name#Heading Name

**5.** Click **Save**
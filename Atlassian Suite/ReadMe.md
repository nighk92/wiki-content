## Overview

The Atlassian Suite is a group of applications made by the [Atlassian](https://www.atlassian.com)�company. They can be used as standalone applications, but they can also collaborate with each other when used together as a group. 

The applications include:

* Bamboo
* Bitbucket
* Confluence
* Jira
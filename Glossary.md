## Glossary

**.NET** - (pronounced "dot net") - A software framework for Windows that provides many functions and libraries for applications

**Abstract** – A class or method that can’t be instantiated

**Android** - A mobile operating system developed by Google

**AngularJS** - A front end web application framework based on Javascript and HTML 

**AOP** - Aspect-Oriented Programming - A type of programming language that aims to increase modularity by allowing separation of concerns, by using stand-alone modules called aspects 

**Apache** - An open-source web server 

**Apache Tomcat** - A version of Apache that supports Java, and can run .war files 

**API** - Application Programming Interface - A framework providing functionality or aiding communication between software components 

**AspectJ** - An aspect-oriented programming (AOP) extension for Java 

**AZ** - Availability Zone - An AWS concept where data centres in separate locations provide increased resilience and availability

**Bash** - A Unix shell and command language 

**BGP** - Border Gateway Protocol - is the protocol underlying the global routing system of the internet. It manages how packets get routed from network to network through the exchange of routing and reachability information between routers.

**Block** – A section of code, surrounded by `{ }` in many programming languages

**Block Storage** - Like traditional computer storage, works at the byte/block level

**BYOL** - Bring Your Own Licence

**C#** - A general purpose, strongly typed, programming language, built upon the .NET framework for Windows 

**Child Class** – Class that inherits from another. Same as Subclass.

**Class** - An extensible program-code template for creating an object 

**Compiled** - A compiled programming language is directly translated to machine code before it is executed

**Compiler** - Turns programs into machine code that a computer executes

**Constant** – A value stored in memory which can’t be changed

**Continuous Integration** - The process of automatically building and testing code every time a change is made 

**CRT** - Cathode Ray Tube - an outdated screen technology using vacuum tubes containing electron guns to produce an image 

**CRUD** - Create, Read, Update, Delete - are the 4 core functions of persistent storage 

**CSS** - Cascading Style Sheets - A language used to describe the presentation of a document written in markup language 

**DAO** - Data Access Object - An API that allows for easy access to the backing data store e.g. a database 

**Database** - An application that organises and stores data 

**DDOS** - Distributed denial of service attack

**Dependency Injection** A technique to supply dependencies to a dependent object, the main aim is to decouple application code from its dependencies by 
delegating the providing of them to external code

**Docker** - Virtualisation software that automates deployment of applications using software containers 

**DTO** - Data Transfer Object - An API for storing data, and transferring it between different access layers of an application e.g. between a database and UI 

**Dynamically Types** - It is not necessary to declare the type of a variable, the type is dynamically verified at runtime 

**Eclipse** - An IDE used for developing Java applications, but also supporting many other programming languages 

**EJB** - Enterprise Java Beans - A server-side Java class that executes the business logic for an application 

**EJS** - Embeddable Javascript - A framework for creating dynamic web pages using Javascript 

**EXE** - An executable file (.exe files) - These files can be run on any Windows operating system to execute a program or command 

**Flux** - A global data store and design architecture for Javascript applications 

**Functional** - A Functional programming language can be used to build computer programs consisting purely of functions, where data is simply passed from one 
function to the next 

**Git** - A command line version control system for tracking changes in computer files 

**Haskell** - A strongly typed, functional programming language 

**HDD** - Hard Disk Drive - The most common type of permanent storage in a computer 

**High Cohesion** - Cohesion is the degree to which the parts of a module belong together, related code should be kept together to make the code easier to 
find and reduce the amount of coupling between modules 

**HTML** - Hypertext Markup Language - a standard language for making web pages 

**HTTP** - Hypertext Transfer Protocol - the core communication protocol for the World Wide Web 

**HTTPS** - Secure Hypertext Transfer Protocol - a version of HTTP where all communication is encrypted 

**IaaS** - Infrastructure as a Service - a service providing virtual machines for general purpose use, masking the underlying physical hardware

**IDE** - Integrated Development Environment - An application that aids in the writing of code 

**IDS/IPS** - Intrusion Detection & Prevention Systems

**Inheritance** - When a Class is based on another, the "child class" can have it's own specific functionality as well as using all of the functionality of 
the "parent class" 

**Instantiate** – To create with values e.g. instantiate a Class by making an Object

**Integration Testing** - Testing an application as a whole, ensuring all the code links together and works as expected 

**Interface** - A class which provides a standard template that other classes can adhere to, using interfaces encourages the creation of generic, flexible and 
reusable code 

**Interpreted** - An interpreted language is executed directly and freely, without being compiled into machine code 

**IO** - Input/Output - Communication between the processor and other devices such as the file system or screen 

**IOPS** - Input/Output operations per second

**ISO** - An archive file which is traditionally written to an optical disk, but more recently can be mounted as a virtual drive to install programs or 
written directly to a storage device to make a bootable drive 

**JAAS** - Java Authentication and Authorisation Service - An API to provide user authentication, security and role protection for Java applications 

**Java** - A general purpose, strongly typed, programming language 

**JavaEE** - Java Enterprise Edition - A Java framework that adds support for enterprise applications 

**JavaFX** - A Java framework for creating Desktop and Web applications, it replaces Swing as the standard GUI library for Java 

**Javascript** - A dynamically typed, interpreted, scripting language commonly used within web pages 

**JavaSE** - Java Standard Edition - The basic Java framework for development and deployment of portable code 

**JCache** - Java Temporary Caching API 

**JDK** - Java Development Kit - Contains additional libraries over the JVM to aid in developing Java programs 

**Jenkins** - Automation server used for deploying/testing of applications and continuous integration. 

**JMS** - Java Messaging Service - An API enabling efficient asynchronous communication between Java components 

**JPA** - Java Persistence API - An API for storing data within a database 

**JRE** - Java Runtime Environment - Contains the necessary libraries to run Java programs 

**JSF** - Java Server Faces - A technology used to build user interfaces for web applications 

**JSON** - Javascript Object Notation - A human and machine readable data format 

**JSP** - Java Server Pages - A framework for creating dynamic web pages using Java 

**JSX** - A Javascript extension language used within React to create HTML 

**JTA** - Java Transaction API - An API for performing distributed transactions to update data on two or more networked computer resources 

**JVM** - Java Virtual Machine - The framework that runs Java byte code 

**Kotlin** - A statically typed programming language the runs on the Java Virtual Machine, and can also be compiled to Javascript 

**LCD** - Liquid Crystal Display - a screen technology using the light modulating properties of liquid crystals 

**LED** - Light Emitting Diode - A screen technology using lots of small lights (as pixels) to create an image 

**Linux** - A family of open-source operating systems built upon the Linux kernel 

**Low Coupling** - Coupling is the degree of dependency between software modules, loosely coupled code is easier to maintain as changes to one module 
shouldn't affect the others 

**Machine Code** - A low level programming language consisting of binary or hexadecimal instructions which can be directly executed by a processor 

**Material UI** - A UI design standard developed by Google, it is available in many formats for web applications including CSS, AngularJS and React versions 

**Maven** - A build automation and dependency management tool 

**Method** – Runnable block of code

**MFA** - Multi-factor authentication - Where multiple steps are required to authenticate a user like a password and a one time passcode

**MongoDB** - A NO-SQL database storing data as JSON documents 

**MS-DOS** - Microsoft Disk Operating System - The first operating system by Microsoft released in 1981, and was the predecessor to Windows 

**NoSQL** - A class of databases that don't use SQL and don't follow all the rules of a traditional relational database 

**Object** – A group of variables and methods, made from a class

**Object-oriented** - Object-oriented programming (OOP) - Is a programming architecture that is based on the creation and manipulation of objects 

**Oracle** - A computer technology organisation that develops Java and several database systems 

**Overriding** – Replacing a method in the parent class

**PaaS** - Platform as a Service - provides a platform to run applications, masking the underlying hardware

**Parent Class** – Class that is being inherited from

**PhantomJS** - A headless browser used for automated testing of web applications 

**PL/SQL** - Procedural Language Structured Query Language - is a procedural programming language designed to be used with SQL 

**Pointer** – A variable that points to the memory address of another object

**Polymorphism** – Using something in two or more ways e.g. the + operator

**POJO** - Plain Old Java Object - A Java class without any special restrictions or class path dependencies, basically just variables with getters/setters 

**Pseudocode** - A notation that resembles a high level programming language, this can be used to design the logic of complex sections of code 

**Python** - A general purpose, dynamically typed, interpreted, programming language, with a syntax designed for readability 

**RAID** - Redundant Array of Inexpensive Disks

**React** - A library for building front end web applications 

**Reactive Programming** - An asynchronous and non-blocking programming paradigm the focuses on data streams and the propagation of changes 

**Recursion** – A function calling itself, which is an alternative to looping

**Redux** - A global data store and design architecture for Javascript applications 

**REST** - Representational State Transfer - An architectural style for developing stateless web services, and producing API's for communication between 
computer systems on the internet 

**Ruby** - A general purpose, dynamically typed, scripting language 

**SaaS** - Software as a Service - centrally hosted software services that are licenced on a subscription basis

**Scala** - A general purpose, dynamically typed, programming language designed with a concise syntax 

**Scalar Value** - A variable that holds just one value at a time, e.g. any primitive variable type or a String 

**Scripting** - A scripting language is used to create short computer programs that execute tasks in the specified order 

**Selenium** - A framework for automating the testing of web applications 

**Shell** - A Unix command line interpreter that can execute bash commands 

**SOAP** - Simple Object Access Protocol - An XML based architectural style for developing web services, and producing API's for communication between 
computer systems on the internet 

**SPARQL** - Pronounced "sparkle" - is an RDF query language

**SpEL** - Spring Expression Language - Enables environment variables and beans to be accessed and calculations to be carried out within a String using the 
@Value Spring annotation 

**Spring** - A Java application framework used for Enterprise and Web applications, and is an alternative to JavaEE 

**Spring Boot** - A framework designed to simplify the configuration and development of a new Spring application 

**Spring MVC** - Spring Model View Controller - The core Spring sever that links the different components of a Spring application together 

**SQL** - Structured Query Language - A standard language for storing and retrieving data in databases 

**SSD** - Solid State Drive - a fast and reliable storage rive

**Statement** – A single line of code

**Structure** – A collection of variables/objects

**Strongly Typed** - All variables must be assigned a type when they are first declared, and this type is verified at compile time 

**Subclass** – Class that inherits from another. Same as Child class.

**UI** - User Interface - the user friendly interface to a computer application 

**UML** - Unified Modelling Language - A diagrammatic language used to design the high level class structure of an application 

**Unit Testing** - Testing each individual section of code in isolation to verify its behaviour 

**Unix** - A family of computer operating systems that derive from the original AT+T Unix originally developed in the 1970's 

**USB** - Universal Serial Bus - Is a universal standard used to define computer connectors, but is most commonly used to refer to a USB flash drive 

**Use Case Diagram** - A UML diagram describing a set of actions that an application should be able to perform and which users have access to perform those 
actions, e.g. Create, Update, Delete 

**Variable** – A value stored in memory which can be changed at any time

**VPC** - Virtual Private Cloud - A logically isolated section of the cloud, consisting of a private subnet and virtual communication protocol like VLAN 

**VPN** - Virtual Private Network - A private network that is extended over a public network

**XML** - Extensible Markup Language - A human and machine readable data format 